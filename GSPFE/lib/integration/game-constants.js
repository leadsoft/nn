var WGS_GAME_CONFIG = {
    currencies: {
        USD: {
            symbol: "$",
            default: true
        },
        AUD: {
            symbol: "$"
        },
        EUR: {
            symbol: "€"
        }
    }
}
