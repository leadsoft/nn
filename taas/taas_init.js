(function () {

    let urlSearchParams = new URLSearchParams(window.location.search);
    let widgetParams = urlSearchParams.get("widgetParams");
    if (!widgetParams) {
        return;
    }
    let widgetJSON = JSON.parse(widgetParams);
    let enabled = widgetJSON.enable;
    if (!enabled) {
        return;
    }

    // let isLocal = urlSearchParams.get("widgetLocal");
    // isLocal = isLocal && isLocal.toLowerCase() === "true";
    let taasPublicDomain = "http://taaswidget.net:8080/taas";
    if (WGS_GAME_CONFIG && WGS_GAME_CONFIG.taas && WGS_GAME_CONFIG.taas.domain) {
        taasPublicDomain = WGS_GAME_CONFIG.taas.domain;
    }
    let isLocal = false;
    if (isLocal) {
        window.rankWidgetDomain = './taas';
    } else {
        window.rankWidgetDomain = taasPublicDomain;
    }
    require.config({
        paths: {
            'rank-widget-core': window.rankWidgetDomain + '/rank-widget-core',
            'taas/sockjs': window.rankWidgetDomain + '/sockjs',
            'taas/stompjs': window.rankWidgetDomain + '/stompjs'
        }
    });
    require(['rank-widget-core'], function (taas) {
        if (!enabled) {
            return;
        }

        let stompjsEndpoint = "http://taaswidget.net:2053/ws-stomp-endpoint";

        // global reference, for s2 call back after preloader finishes
        let taasWidget = {};
        window.taasWidget = taasWidget;

        // add "taas-widget-anchor" class to main canvas element, for widget positioning
        let canvas = document.getElementById("canvas");
        canvas.classList.add("taas-widget-anchor");


        // call back after game has finished preloading
        taasWidget.onInit = () => {

            let widgetScript = document.createElement("script");
            widgetScript.src = window.rankWidgetDomain + "/rank-widget-core.js";
            widgetScript.onload = connectWidget.bind(this);
            document.head.appendChild(widgetScript);
        }

        function getWidgetHeaders() {
            return widgetJSON;
        }

        function getWidgetConfig() {
            return {
                display: true,
                height: { desktop: 32, mobile: 30 },
                coordinates: { x: 0, y: 50 },
                infoWidget: { width: 250 }
            }
        }

        function connectWidget() {
            if (!enabled) {
                return;
            }

            let widgetData = {};
            widgetData.headers = getWidgetHeaders();
            widgetData.config = getWidgetConfig();
            taas.connectTaasSocket(stompjsEndpoint, widgetData, true);

            window.taas = taas;
            return false;
        }

    });

})();
