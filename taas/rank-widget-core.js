/* Taas Widget v{taas-widget.version}-{taas-widget.build} */
// wrapping taas-widget in Universal Module Declaration pattern
// https://github.com/umdjs/umd

// UMD boilerplate
!function (factory) {
	// common js module loading
	if ("object" == typeof exports && "undefined" != typeof module)
		module.exports = e();
	// AMD (require js) loading
	else if ("function" == typeof define && define.amd)
		define(["taas/sockjs", "taas/stompjs"], factory);
	else {
		("undefined" != typeof window ? window
			: "undefined" != typeof global ? global
				: "undefined" != typeof self ? self
					: this).taas = factory();
	}
}(function (_SockJS, _StompJs) {
	if (_SockJS)
		SockJS = _SockJS;

	if (_StompJs)
		StompJs = _StompJs;

	var stompTaasClient = "", rebuyResp = {}, widgetTaasData = {}, tourDetail = false, updateCaptured = false, processUpdate = false, openInfoWindow = false;
	var rankWidgetDomain = ("string" != typeof window.rankWidgetDomain ? "" : window.rankWidgetDomain);

	function connectTaasSocket(e, t, n) {
		if (n === undefined) { n = false; }
		if (e != "" && e != undefined) {
			widgetTaasData = t;
			addWidget();
			stompTaasClient = new StompJs.Client({
				webSocketFactory: function () { return new SockJS(e); },
				connectHeaders: t.headers,
				debug: function (e) {
					if (n == true) { console.log(e); }
				},
				onConnect: function (e) {
					var n = getSessionIdTaas(e),
						i = n.split("/")[n.split("/").length - 1];
					stompTaasClient.subscribe("/user/" + i + "/queue", function (e) {
						showTaasUpdate(JSON.parse(e.body));
					});
					stompTaasClient.subscribe("/user/" + t.headers.playerId + "/topic", function (e) {
						showTaasUpdate(JSON.parse(e.body));
					});
				},
				onStompError: function (e) {
					stompTaasClient.debug("Error: " + e);
				},
				onWebSocketClose: function (e) {
					stompTaasClient.debug("Disconnection: " + JSON.stringify(e));
				},
				reconnectDelay: 5e3
			});
			stompTaasClient.activate();
		}
	}
	function disconnectTaasStomp() {
		let widgetElm = document.getElementById('ranking-widget-container');
		let infoElm = document.getElementById('ranking-info-container');
		if (widgetElm) widgetElm.innerHTML = "";
		if (infoElm) infoElm.innerHTML = "";
		widgetTaasData = {};
		tourDetail = false;
		updateCaptured = false;
		openInfoWindow = false;
		processUpdate = false
		if (stompTaasClient != "" && stompTaasClient != undefined) {
			if(stompTaasClient.webSocketObj){
				stompTaasClient.webSocketObj.close();
			}
			stompTaasClient.deactivate();
		}
	}
	function getSessionIdTaas(e) {
		var t = stompTaasClient.webSocketObj._transport.url,
			n = e;
		return (t = (t = t.replace("ws://" + n + "/", "")).replace("/websocket", "")).replace(/^[0-9]+\//, "");
	}
	function getTaasTournamentDetail(param) {
		openInfoWindow = (param && param == 1);
		stompTaasClient.publish({ destination: '/wsdf/getTournamentDetail', body: JSON.stringify({}) });
	}
	function rebuyTaasConfirm() {
		stompTaasClient.publish({ destination: '/wsdf/rebuyConfirm', body: JSON.stringify({}) });
	}
	function rebuyTaasComplete() {
		stompTaasClient.publish({ destination: '/wsdf/rebuyComplete', body: JSON.stringify({}) });
	}
	function loadRankCss(e) {
		let widgetCSS = document.createElement('link')
		widgetCSS.href = e + "/rank-widget-style.css?t=" + Math.random();
		widgetCSS.rel = 'stylesheet'
		widgetCSS.media = 'screen'
		document.head.appendChild(widgetCSS)
	}
	function showTaasUpdate(e) {
		stompTaasClient.debug("Message: " + e);
		//var t = { content: e.content, updatetime: new Date(), type: e.type };
		if ((console.log(e), null != e.type)) {
			let widgetPanel = document.getElementById('ranking-widget');
			let infoPanel = document.getElementById('rank-info');
			let infoContent = document.getElementById('rank-info-content');
			let rankNo = document.getElementById('rank-no');
			let balance = document.getElementById('balance');
			let rebuyBtn = document.getElementById('rebuy-btn');
			let widgetContent = document.getElementById('widget-content');
			let rebuyMsgPanel = document.getElementById('rebuy-msg-box');
			let rebuyConfirmMsg = document.getElementById('rebuyConfirmMsg');
			let rebuyConfirmBtn = document.getElementById('rebuy-confirm');
			let rebuyCloseBtn = document.getElementById('rebuy-close');

			if (infoPanel) infoPanel.style.display = "none";
			if (infoContent) infoContent.innerHTML = "";
			if (rebuyMsgPanel) rebuyMsgPanel.style.display = "none";
			if (rebuyConfirmMsg) rebuyConfirmMsg.innerHTML = "";
			if (rebuyConfirmBtn) rebuyConfirmBtn.style.display = "block";
			if (rebuyCloseBtn) rebuyCloseBtn.innerHTML = "Cancel";

			if (e.type == 1) {
				if(typeof tourDetail !== 'object') tourDetail = {game:e};
				else{
					if(!e.preventCapture && (updateCaptured === false || !processUpdate)){
						updateCaptured = e;
						return false;
					}else if(processUpdate){
						tourDetail = {game:updateCaptured};
						updateCaptured = false;
						processUpdate = false;
					}
				}
				if (e.error) {
					if (widgetPanel) widgetPanel.style.display = 'none';
					if (rebuyMsgPanel) rebuyMsgPanel.style.display = "block";
					if (rebuyConfirmBtn) rebuyConfirmBtn.style.display = "none";
					if (rebuyCloseBtn) rebuyCloseBtn.innerHTML = "OK";
					if (rebuyConfirmMsg) {
						rebuyConfirmMsg.innerHTML = e.error;
					}
				} else {
					if (widgetPanel && widgetPanel.classList.contains('active') && !tourDetail.detail) {
						getTaasTournamentDetail(0);
					} else {
						if (widgetPanel) widgetPanel.style.display = 'inline-flex';
						let currSymbol = (tourDetail.game.currencySymbol ? tourDetail.game.currencySymbol : (tourDetail.detail && tourDetail.detail.currencySymbol ? tourDetail.detail.currencySymbol : '$'));
						let isQualified = !(typeof tourDetail.game.betsToQualify != 'undefined' && +tourDetail.game.betsToQualify > 0);

						if (rankNo) rankNo.innerHTML = isQualified ? "Rank: " + tourDetail.game.rank : "Bets to qualify: "+tourDetail.game.betsToQualify;
						if (balance) balance.innerHTML = isQualified ? (tourDetail.game.rank && typeof tourDetail.game.rankBalance != 'undefined' ? "Ranking Balance: " + currSymbol + tourDetail.game.rankBalance.toFixed(2) : "Balance: " + currSymbol + tourDetail.game.stake.toFixed(2)) : "Bets made: " + tourDetail.game.bets;
						if (rebuyBtn) rebuyBtn.style.display = tourDetail.game.rebuy ? "block" : "none";
						if (widgetContent && (tourDetail.detail || (widgetPanel && widgetPanel.classList.contains('active')))) {
							widgetContent.innerHTML = (!isQualified && typeof tourDetail.game.stake != 'undefined' ? '<div class="info-data">Balance: ' + currSymbol + tourDetail.game.stake.toFixed(2)+'</div>' : '') +
								(tourDetail.detail && tourDetail.detail.endsInMillis && (!tourDetail.detail.endType || tourDetail.detail.endType == 1) ? '<div class="info-data">Time Left: ' + getRemainTime(tourDetail.detail.endsInMillis) + '</div>' : '');
						}
					}
				}
			} else if (e.type == 2) {
				if(!openInfoWindow && typeof tourDetail == 'object' && !tourDetail.detail){
					tourDetail.detail = e;
					showTaasUpdate({type:1, preventCapture: true});
				}else{
					if(openInfoWindow){
						tourDetail.detail = e;
						openInfoWindow = false;
						showTaasUpdate({type:1, preventCapture: true});
					}

					Object.keys(e).forEach((ind) => {
						tourDetail.detail[ind] = e[ind]
					});
					if (infoPanel) infoPanel.style.display = "block";
					if (infoContent) {
						let currSymbol = (tourDetail.game.currencySymbol ? tourDetail.game.currencySymbol : (tourDetail.detail && tourDetail.detail.currencySymbol ? tourDetail.detail.currencySymbol : '$'));

						infoContent.innerHTML = (typeof tourDetail.detail.endType != 'undefined' ? '<li><label>End Type:</label>' +
								'<span>' + (tourDetail.detail.endType == 1 ? 'Scheduled' : 'Knockout') + '</span></li>' : '') +
							(typeof tourDetail.game.bets != 'undefined' ? '<li><label>Bets Made:</label>' +
								'<span>' + tourDetail.game.bets + '</span></li>' : '') +
							(typeof tourDetail.detail.participants != 'undefined' ? '<li><label>Participants:</label>' +
								'<span>' + tourDetail.detail.participants + '</span></li>' : '') +
							(typeof tourDetail.game.rank != 'undefined' && typeof tourDetail.detail.totalPrizeCount != 'undefined' ?
									'<li><label>Prize Position:</label> <span>' + tourDetail.game.rank + ' of ' + tourDetail.detail.totalPrizeCount + '</span></li>' :
									(typeof tourDetail.detail.totalPrizeCount != 'undefined' ? '<li><label>Prize Positions:</label>' +
										'<span>' + tourDetail.detail.totalPrizeCount + '</span></li>' : '')
							) +
							('<li class="prizes"><label>Your current prize allocation</label>' +
								(tourDetail.game.rank && (typeof tourDetail.game.cashPrize != 'undefined' || typeof tourDetail.game.bonusPrize != 'undefined' || typeof tourDetail.game.rewardsPrize != 'undefined') ?
									('<ul>'+
										(typeof tourDetail.game.cashPrize != 'undefined' ? '<li><label>Cash: </label><span>'+currSymbol+formatNumber(tourDetail.game.cashPrize,2)+'</span></li>' : '')+
										(typeof tourDetail.game.bonusPrize != 'undefined' ? '<li><label>Bonus: </label><span>'+currSymbol+formatNumber(tourDetail.game.bonusPrize,2)+'</span></li>' : '')+
										(typeof tourDetail.game.rewardsPrize != 'undefined' ? '<li><label>Reward Points: </label><span>'+formatNumber(tourDetail.game.rewardsPrize,0)+'</span></li>' : '')+
										'</ul>') :
									('<p>Currently not in a prize position. Keep trying.</p>'))
								+ '</li>');
					}
				}
			} else if (e.type == 3) {
				rebuyResp = e;
				if (rebuyMsgPanel) rebuyMsgPanel.style.display = "block";
				if (rebuyConfirmBtn) rebuyConfirmBtn.style.display = "block";
				if (rebuyCloseBtn) rebuyCloseBtn.innerHTML = "Cancel";
				if (rebuyConfirmMsg) {
					if(typeof e.fromCurrencySymbol != 'undefined' && typeof e.rate != 'undefined'){
						rebuyConfirmMsg.innerHTML = 'Would you like buy-in to tournament ' + '#' + e.tournamentId + ' - ' + e.description + '? Your balance will be debited ' + e.fromCurrencySymbol + (+e.fee * +e.rate).toFixed(2) + ' (' + e.toCurrencySymbol + e.fee.toFixed(2) + ' ' + e.toCurrencyCode + ' @ ' + e.rate.toFixed(3) + ').<span class="goodluck"> Good Luck!</span>';
					} else {
						rebuyConfirmMsg.innerHTML = 'Would you like to Rebuy back into tournament #' + e.tournamentId + ' - ' + e.description + '? Your balance will be debited ' + e.toCurrencySymbol + e.fee.toFixed(2) + '.<span class="goodluck"> Good Luck!</span>';
					}
				}
			} else if (e.type == 4) {
				if (rebuyMsgPanel) rebuyMsgPanel.style.display = "block";
				if (rebuyConfirmBtn) rebuyConfirmBtn.style.display = "none";
				if (rebuyCloseBtn) rebuyCloseBtn.innerHTML = "OK";
				if (rebuyConfirmMsg) {
					if (e.success) {
						if(typeof rebuyResp.fromCurrencySymbol != 'undefined' && typeof rebuyResp.rate != 'undefined'){
							rebuyConfirmMsg.innerHTML = 'Your balance has been debited ' + (rebuyResp.fromCurrencySymbol ? rebuyResp.fromCurrencySymbol : '') + (rebuyResp.fee && rebuyResp.rate ? (+rebuyResp.fee * +rebuyResp.rate).toFixed(2) + ' (' + (rebuyResp.toCurrencySymbol ? rebuyResp.toCurrencySymbol : '') + rebuyResp.fee.toFixed(2) + ' ' + (rebuyResp.toCurrencyCode ? rebuyResp.toCurrencyCode : '') + ' @ ' + rebuyResp.rate.toFixed(3) + ')' : '') + ' from your account and added to your tournament balance.<span class="goodluck"> Good Luck!</span>';
						} else {
							rebuyConfirmMsg.innerHTML = 'Your balance has been debited ' + (rebuyResp.toCurrencySymbol ? rebuyResp.toCurrencySymbol : '') + (rebuyResp.fee ? rebuyResp.fee.toFixed(2) : '') + ' from your account and added to your tournament balance.<span class="goodluck"> Good Luck!</span>';
						}
						tourDetail.game.rebuy = false;
						if(typeof e.stake != 'undefined') tourDetail.game.stake = +e.stake;
						if(typeof e.betsToQualify != 'undefined') tourDetail.game.betsToQualify = +e.betsToQualify;
						if(typeof e.rank != 'undefined') tourDetail.game.rank = +e.rank;

						let currSymbol = (tourDetail.game.currencySymbol ? tourDetail.game.currencySymbol : (tourDetail.detail && tourDetail.detail.currencySymbol ? tourDetail.detail.currencySymbol : '$'));
						let isQualified = !(typeof tourDetail.game.betsToQualify != 'undefined' && +tourDetail.game.betsToQualify > 0);

						if (rankNo) rankNo.innerHTML = isQualified ? "Rank: " + tourDetail.game.rank : "Bets to qualify: "+tourDetail.game.betsToQualify;
						if (balance) balance.innerHTML = isQualified ? (tourDetail.game.rank && typeof tourDetail.game.rankBalance != 'undefined' ? "Ranking Balance: " + currSymbol + tourDetail.game.rankBalance.toFixed(2) : "Balance: " + currSymbol + tourDetail.game.stake.toFixed(2)) : "Bets made: " + tourDetail.game.bets;
						if (rebuyBtn) rebuyBtn.style.display = tourDetail.game.rebuy ? "block" : "none";
						if (widgetContent && (tourDetail.detail || (widgetPanel && widgetPanel.classList.contains('active')))) {
							widgetContent.innerHTML = (!isQualified && typeof tourDetail.game.stake != 'undefined' ? '<div class="info-data">Balance: ' + currSymbol + tourDetail.game.stake.toFixed(2)+'</div>' : '') +
								(tourDetail.detail && tourDetail.detail.endsInMillis && (!tourDetail.detail.endType || tourDetail.detail.endType == 1) ? '<div class="info-data">Time Left: ' + getRemainTime(tourDetail.detail.endsInMillis) + '</div>' : '');
						}
						let widgetBox = document.getElementById("ranking-widget-container");
						if(widgetBox){
							let event = new CustomEvent("TournamentRebuySuccess", { bubbles: true, detail: { } });
							widgetBox.dispatchEvent(event);
						}
					} else {
						rebuyConfirmMsg.innerHTML = e.errorMessage;
					}
				}
			} else {
				stompTaasClient.debug("Unknown Update (" + e.type + "): " + JSON.stringify(e));
			}
		}
	}
	function getRemainTime(milis) {
		let t = milis - new Date().getTime();
		let days = Math.floor(t / (1000 * 60 * 60 * 24));
		let hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		let minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
		let seconds = Math.floor(((t % (1000 * 60)) / 1000));
		if (days > 0) {
			return days + 'd ' + hours + 'h';
		} else if (hours > 0) {
			return hours + 'h ' + minutes + 'm';
		} else if (minutes > 0) {
			return minutes + 'm ' + seconds + 's';
		} else if (seconds > 0) {
			return seconds + 's';
		}
		return '0s';
	}
	function hideRankInfo() {
		let infoPanel = document.getElementById('rank-info');
		if (infoPanel) infoPanel.style.display = "none";
		let infoContent = document.getElementById('rank-info-content');
		if (infoContent) infoContent.innerHTML = "";
	}
	function toggleRankWidget() {
		let widget = document.getElementById('ranking-widget');
		if (widget) {
			if (widget.classList.contains('active')) {
				widget.classList.remove('active');
			} else {
				widget.classList.add('active');
				if (!tourDetail.detail){
					getTaasTournamentDetail(0);
				}
			}
		}
	}
	function rebuySubmit() {
		rebuyTaasComplete();
		let rebuyMsgPanel = document.getElementById('rebuy-msg-box');
		if (rebuyMsgPanel) rebuyMsgPanel.style.display = "none";
	}
	function rebuyClose() {
		rebuyResp = {};
		let rebuyMsgPanel = document.getElementById('rebuy-msg-box');
		if (rebuyMsgPanel) rebuyMsgPanel.style.display = "none";
		let rebuyConfirmMsg = document.getElementById('rebuyConfirmMsg');
		if (rebuyConfirmMsg) rebuyConfirmMsg.innerHTML = "";
		if (tourDetail.error) disconnectTaasStomp();
	}
	function addGameListeners(){
		document.body.addEventListener("GameBalanceDisplayed", (ev)=>{
			console.log('GameBalanceDisplayed', ev);
			if(updateCaptured && typeof updateCaptured == 'object' && !processUpdate){
				if(ev.detail && typeof ev.detail.balance != 'undefined'){
					updateCaptured.stake = ev.detail.balance;
				}
				processUpdate = true;
				showTaasUpdate({type:1});
			}else if(ev.detail && typeof ev.detail.balance != 'undefined' && typeof tourDetail === 'object' && tourDetail.game){
				tourDetail.game.stake = ev.detail.balance;

				let isQualified = !(typeof tourDetail.game.betsToQualify != 'undefined' && +tourDetail.game.betsToQualify > 0);
				let currSymbol = (tourDetail.game.currencySymbol ? tourDetail.game.currencySymbol : (tourDetail.detail && tourDetail.detail.currencySymbol ? tourDetail.detail.currencySymbol : '$'));
				if(!(typeof tourDetail.game.betsToQualify != 'undefined' && +tourDetail.game.betsToQualify > 0)){
					let balance = document.getElementById('balance');
					if (balance) balance.innerHTML = isQualified ? (tourDetail.game.rank && typeof tourDetail.game.rankBalance != 'undefined' ? "Ranking Balance: " + currSymbol + tourDetail.game.rankBalance.toFixed(2) : "Balance: " + currSymbol + tourDetail.game.stake.toFixed(2)) : "Bets made: " + tourDetail.game.bets;
				}else{
					let widgetPanel = document.getElementById('ranking-widget');
					let widgetContent = document.getElementById('widget-content');
					if (widgetContent && (tourDetail.detail || (widgetPanel && widgetPanel.classList.contains('active')))) {
						widgetContent.innerHTML = '<div class="info-data">Balance: ' + currSymbol + tourDetail.game.stake.toFixed(2)+'</div>' +
							(tourDetail.detail && tourDetail.detail.endsInMillis && (!tourDetail.detail.endType || tourDetail.detail.endType == 1) ? '<div class="info-data">Time Left: ' + getRemainTime(tourDetail.detail.endsInMillis) + '</div>' : '');
					}
				}
			}
		});
		document.body.addEventListener("click", (event) => {
			let elm = document.querySelector('#rank-info .ranking-modal');
			if(document.getElementById('rank-info').style.display != "none" && event.target != elm && !childOf(event.target, elm)){
				hideRankInfo();
			}
		});
		window.addEventListener('resize', ()=>{
			let widgetPanel = document.getElementById('ranking-widget');
			if(widgetPanel){
				const cords = getWidgetCoordinates();
				widgetPanel.style.left = cords[0]+'px';
				widgetPanel.style.top = cords[1]+'px';
			}
		});
	}
	function childOf(c, p) {
		while((c=c.parentNode)&&c!==p); return !!c;
	}
	function formatNumber(t,d){
		return (t=(t=(+t).toFixed(d)).toString().split("."))[0].replace(/\B(?=(\d{3})+(?!\d))/g,","),d?t.join("."):t[0];
	}
	function getWidgetCoordinates(){
		let xPos = (widgetTaasData.config.coordinates && typeof widgetTaasData.config.coordinates.x != 'undefined' ? +widgetTaasData.config.coordinates.x : 0);
		let yPos = (widgetTaasData.config.coordinates && typeof widgetTaasData.config.coordinates.y != 'undefined' ? +widgetTaasData.config.coordinates.y : 0);
		let canvas = document.getElementsByClassName("taas-widget-anchor");
		if(canvas.length){
			let marginLeft = canvas[0].offsetLeft;
			let marginTop = canvas[0].offsetTop;
			if(!isNaN(+marginLeft) && +marginLeft > 0){ xPos += +marginLeft; }
			if(!isNaN(+marginTop) && +marginTop > 0){ yPos += +marginTop; }
		}
		return [xPos, yPos];
	}
	function addWidget() {
		const cords = getWidgetCoordinates();

		let rankWidgetHtml = '<div class="ranking-widget" id="ranking-widget" style="display:none;' + (widgetTaasData.config.height && typeof widgetTaasData.config.height[(widgetTaasData.headers.deviceCode == 'D' ? 'desktop' : 'mobile')] != 'undefined' ? 'height:' + widgetTaasData.config.height[(widgetTaasData.headers.deviceCode == 'D' ? 'desktop' : 'mobile')] + 'px;' : '') + 'left:'+cords[0]+'px;' + 'top:'+cords[1]+'px;' + '"><div class="rank-no" id="rank-no"></div><div class="rank-no" id="balance"></div><div class="rebuy-btn" id="rebuy-btn" style="display:none;"><a href="javascript:void(0)" onclick="taas.rebuyTaasConfirm()">RE-BUY NOW</a></div><div class="collapase-info" id="widget-content"></div><div class="info-collapse"><a href="javascript:void(0)" onclick="taas.getTaasTournamentDetail(1)" class="info-btn-rank"><img src="' + rankWidgetDomain + '/info-icon.png"></a><a href="javascript:void(0)" onclick="taas.toggleRankWidget()" class="arrow-rank"><img src="' + rankWidgetDomain + '/right-arrow.png"></a></div></div><div class="modal-ui-block-outer" id="rebuy-msg-box" style="display:none;"><div class="modal-ui-block popup-ui-block"><div class="inner-content-modal"><p id="rebuyConfirmMsg"></p><div class="btn-block tournamanent-btnblock"><a href="javascript:void(0)" id="rebuy-close" onclick="taas.rebuyClose()">Cancel</a><a href="javascript:void(0)" id="rebuy-confirm" onclick="taas.rebuySubmit()">Yes</a></div></div></div></div>';

		let rankInfoHtml = '<div id="rank-info" class="modal-ranking-outer" style="display:none;"><div class="ranking-modal" style="' + (widgetTaasData.config.infoWidget && typeof widgetTaasData.config.infoWidget.width != 'undefined' ? 'width:' + widgetTaasData.config.infoWidget.width + 'px;' : '') + '"><button onclick="taas.hideRankInfo()" class="dismiss-modal">&times;</button><ul id="rank-info-content"></ul></div></div>';

		let widgetBox = document.getElementById("ranking-widget-container");
		let infoBox = document.getElementById("ranking-info-container");
		if(widgetBox){ widgetBox.innerHTML = rankWidgetHtml; }
		else{
			var t = document.createElement("div");
			t.setAttribute("id","ranking-widget-container");
			t.innerHTML = rankWidgetHtml;
			document.body.appendChild(t);
		}
		if(infoBox){ infoBox.innerHTML = rankInfoHtml; }
		else{
			var t = document.createElement("div");
			t.setAttribute("id","ranking-info-container");
			t.innerHTML = rankInfoHtml;
			document.body.appendChild(t);
		}
		addGameListeners();
	}
	loadRankCss(rankWidgetDomain);
	// external interface for taas-widget-core, functions not used externally are excluded
	return {
		connectTaasSocket: connectTaasSocket,
		disconnectTaasStomp: disconnectTaasStomp,
		// getSessionIdTaas: getSessionIdTaas,
		getTaasTournamentDetail: getTaasTournamentDetail,
		rebuyTaasConfirm: rebuyTaasConfirm,
		rebuyTaasComplete: rebuyTaasComplete,
		// showTaasUpdate: showTaasUpdate,
		// getRemainTime: getRemainTime,
		hideRankInfo: hideRankInfo,
		toggleRankWidget: toggleRankWidget,
		rebuySubmit: rebuySubmit,
		rebuyClose: rebuyClose
		// addWidget: addWidget,
	};
});