window.onload = function()
{
    WebFont.load(
        {
            // this event is triggered when the fonts have been rendered, see https://github.com/typekit/webfontloader
            active : function()
            {
                start();
            },

            // multiple fonts can be passed here
            google :
                {
                    families: [ fontName ]
                }
        });
};
