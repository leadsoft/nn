var mandatoryParams = [/*'authToken', *//*'cid', 'real',*/ 'game', /*'tournament', *//*'clientType'*/];
let app = [];
let oAL = [];
let game = [];
let freeS = [];
let fsv = [];
let spinH = [];
let footer = [];
let gameCompleted = [];
let user = [];
let infoModal = [];
let rebuy = [];
let modalDialog = [];
let gameContainer = [];
let footerContainer = [];
let titleContainer = [];
let actionContainer = [];
let preloaderContainer = [];
let curtainsContainer = [];
let chairsContainer = [];

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function changeGameSize(enableSleep = false) {
    return new Promise(async (resolve) => {
        if(enableSleep)
            await sleep(200);
        if (isMobile())
            if (isLandscape()) {
                console.log('changeGameSize - isLandscape')
                GAME_WIDTH = GAME_WIDTH_LANDSCAPE;
                GAME_HEIGHT = GAME_HEIGHT_LANDSCAPE;
            } else {
                console.log('changeGameSize - isPortrait')
                GAME_WIDTH = GAME_WIDTH_PORTRAIT;
                GAME_HEIGHT = GAME_HEIGHT_PORTRAIT;
            }
        await sleep(100);
        resolve(1)
    })
}

loadJSON(function (response) {
    let data = JSON.parse(response);
    if (!data.soundStatus || new URLSearchParams(window.location.search).has('mute'))
        PIXI.sound.toggleMuteAll();
});
$(document).ready(async function () {
    var rendererOptions = {
        antialiasing: false,
        transparent: false,
        resolution: window.devicePixelRatio,
        autoResize: true,
    };

    renderer = PIXI.autoDetectRenderer(GAME_WIDTH, GAME_HEIGHT,
        rendererOptions);

// Put the renderer on screen in the corner
    renderer.view.style.position = "absolute";
    renderer.view.style.top = "0px";
    renderer.view.style.left = "0px";
    renderer.view.style.transform = 'translate3d( 50%, 50%, 0% )';

    const canvas = document.getElementById('mycanvas');

    preloaderContainer = new PIXI.Container();
    preloaderContainer.name = 'preloader-container';
    preloaderContainer.zIndex = 10;
    /*preloaderContainer.visible = true;*/
    preloaderContainer.sortableChildren = true;

    /*preloaderContainer.visible = false;*/
    preloaderContainer.x = 0;

    app = new PIXI.Application({
        antialias: true,
        // backgroundColor: 0x5b0600,
        view: canvas,
        width: GAME_WIDTH,
        height: GAME_HEIGHT,
        resolution: window.devicePixelRatio,
        autoDensity: true,
    });

    app.stage.sortableChildren = true;



    gameContainer = new PIXI.Container();
    gameContainer.name = 'game-container';
    gameContainer.width = GAME_WIDTH;
    gameContainer.height = GAME_HEIGHT;
    gameContainer.visible = false;
    gameContainer.sortableChildren = true;
    gameContainer.zIndex = 1;
    /*var bg = new PIXI.Sprite(PIXI.Texture.WHITE);
    bg.tint = 0xfff000;
    bg.width = gameContainer.width;
    bg.height = gameContainer.height;
    bg.alpha = 0.4
    gameContainer.addChild(bg);*/
    footerContainer = new PIXI.Container();
    footerContainer.name = 'footer-container';
    footerContainer.visible = false;
    footerContainer.zIndex = 2;

    actionContainer = new PIXI.Container();
    actionContainer.name = 'action-container';
    actionContainer.visible = true;
    actionContainer.zIndex = 9;
    actionContainer.sortableChildren = true;
    actionContainer.zIndex = 3

    titleContainer = new PIXI.Container();
    titleContainer.name = 'title-container';
    titleContainer.visible = false;
    titleContainer.sortableChildren = true;
    titleContainer.zIndex = 1

    curtainsContainer = new PIXI.Container();
    curtainsContainer.name = 'curtains-container';
    /*curtainsContainer.visible = false;*/
    curtainsContainer.sortableChildren = true;
    curtainsContainer.zIndex = isMobile() ? 2 : 10
    curtainsContainer.width = GAME_WIDTH;
    curtainsContainer.height = GAME_HEIGHT;

    chairsContainer = new PIXI.Container();
    chairsContainer.name = 'chairs-container';
    chairsContainer.visible = true;
    chairsContainer.sortableChildren = true;
    chairsContainer.zIndex = 1;

    oAL = new onAssetsLoad(app, gameContainer);

    game = new Game(app, gameContainer);
    spinH = new spinHelper();
    footer = new Footer(app, gameContainer);

    gameCompleted = new gameComplete();
    user = new User();
    infoModal = new Info(app);
    rebuy = new Rebuy(gameContainer);
    freeS = new freeSpin();

    app.stage.addChild(gameContainer, titleContainer, actionContainer, footerContainer, preloaderContainer, curtainsContainer, chairsContainer);
    modalDialog = new ModalDialog(gameContainer);
    fsv = new freeSpinVoucher();
    /*await sleep(4000);*/
    await changeGameSize();
    resizeApp();
    window.addEventListener("resize", async () => {
        footer.paylineWin.visible = false;
        footer.paylineWin.x = 0;
        footer.paylineWin.y = 0;
        await changeGameSize(true);
        resizeApp(false)
    });
});

function resizeOld(app, initialCall = true) {
    return function () {
        const vpw = window.innerWidth;  // Width of the viewport
        const vph = window.innerHeight; // Height of the viewport
        let nvw; // New game width
        let nvh; // New game height

        if (vph / vpw < GAME_HEIGHT / GAME_WIDTH) {
            nvh = vph;
            nvw = (nvh * GAME_WIDTH) / GAME_HEIGHT;
        } else {
            // In the else case, the opposite is happening.
            nvw = vpw;
            nvh = (nvw * GAME_HEIGHT) / GAME_WIDTH;
        }
        setTimeout(function () {
            /*if (settings.DEVICE === 'D') {*/
            if (DESKTOP_INITIALS.includes(settings.DEVICE)) {
                app.renderer.resize(nvw, nvh);

                app.stage.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
            } else {
                /* if (isLandscape()) {
                     GAME_HEIGHT = 600
                     GAME_WIDTH = 1066
                 } else {
                     GAME_HEIGHT = 1066
                     GAME_WIDTH = 600

                 }*/
                app.renderer.resize(vpw, vph);

                gameContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                curtainsContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                footerContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                titleContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                actionContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                //preloaderContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                curtainsContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                chairsContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
                infoModal.mainContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);


                if (!initialCall) {
                    menuActions.openCloseInfoBar(true);
                    coinMenuBtnActions.openCloseSliderBar(true);
                    setActionContainerTextures();
                    setFooterContainerTextures();
                    setActionContainerPosition();
                    setFooterContainerPosition();
                    footer.adjustFooter(!settings.DEVICE.includes('D') ? 'mobile' : 'desktop');
                    switchTitleTexture();
                    prepareLayoutForDevice();
                    textAdjustment(!settings.DEVICE.includes('D'))
                }
                resetPosition();

            }

            if (!settings.GAME_START)
                initPreloadContainer(true)//resize = true

        }, 100)
    };
}

function resizeApp(initialCall = true) {
    /*let gameWidth = GAME_WIDTH;
    let gameHeight = GAME_HEIGHT;
    if(!isLandscape()){
        gameWidth = GAME_HEIGHT;
        gameHeight = GAME_WIDTH;
    }*/

    const vpw = document.documentElement.clientWidth;  // Width of the viewport
    const vph = window.innerHeight; // Height of the viewport
    let nvw; // New game width
    let nvh; // New game height

    if (vph / vpw < GAME_HEIGHT / GAME_WIDTH) {
        nvh = vph;
        nvw = (nvh * GAME_WIDTH) / GAME_HEIGHT;
    } else {
        // In the else case, the opposite is happening.
        nvw = vpw;
        nvh = (nvw * GAME_HEIGHT) / GAME_WIDTH;
    }

    /*if (settings.DEVICE === 'D') {*/
    if (DESKTOP_INITIALS.includes(settings.DEVICE)) {
        app.renderer.resize(nvw, nvh);

        app.stage.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
    } else {
        app.renderer.resize(vpw, vph);
        /*gameContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);*/
        if (!initialCall && isMobile() && !settings.GAME_START){
            rotateDevice()
            return;
        }

        if (!isLandscape()) {
             nvh = vph;
             nvw = (nvh * GAME_WIDTH_PORTRAIT) / GAME_HEIGHT_PORTRAIT;
            /*gameContainer.scale.set(nvw / ((SYMBOL_CONTAINER_WIDTH) * 5 + 50), nvh / (GAME_HEIGHT + 50));*/
            curtainsContainer.scale.set(nvw / 1066)
            // gameContainer.scale.set(nvw / 1066); /*SHOULD BE 1066*/
            gameContainer.scale.set((window.innerWidth/GAME_HEIGHT_PORTRAIT ).toFixed(2) , nvw / GAME_HEIGHT_PORTRAIT);

            footerContainer.scale.set(nvh / (nvw * 3) , nvh / window.innerHeight/2);

            chairsContainer.scale.set(nvw / GAME_WIDTH_PORTRAIT, nvh / GAME_HEIGHT_PORTRAIT);
            infoModal.mainContainer.scale.set(nvw / GAME_WIDTH_PORTRAIT, nvh / GAME_HEIGHT_PORTRAIT);
            //curtainsContainer.scale.set(nvw / 1066, nvh / 600);
        } else {
            footerContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
            infoModal.mainContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
            gameContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
            chairsContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
            curtainsContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
        }
        //debugger;

        // footerContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
        titleContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
        actionContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
        //preloaderContainer.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);


        if (!initialCall) {
            menuActions.openCloseInfoBar(true);
            coinMenuBtnActions.openCloseSliderBar(true);
            infoModal.hideInfo();
            setActionContainerTextures();
            setFooterContainerTextures();
            setActionContainerPosition();
            setFooterContainerPosition();
            resizeActionContainerText();
            infoOnResize();
            footer.adjustFooter(!settings.DEVICE.includes('D') ? 'mobile' : 'desktop');
            switchTitleTexture();
            prepareLayoutForDevice();
            textAdjustment(!settings.DEVICE.includes('D'))
            if(settings.FSV_STATUS)
            {
                fsv.repositionFreeCouponContainer()
            }
        }
        resetPosition();
        addGraphic();
        if (isMobile())
            showHideContainers();
    }

    /*if (!settings.GAME_START)
        initPreloadContainer(true)*/
}

async function resetPosition() {
    if (typeof oAL.gameBackgroundImg !== 'undefined')
        oAL.gameBackgroundImg.scale.set(1, 1);

    /*if (settings.DEVICE !== 'D') {*/

    if (isMobile()  /*!== 'D'*/) {

        curtainsContainer.x = window.innerWidth / 2 - gameContainer.width / 2;

        chairsContainer.y = window.innerHeight - chairsContainer.height;
        chairsContainer.x = window.innerWidth / 2 - chairsContainer.width / 2;
        if(isLandscape()) {
            titleContainer.x = window.innerWidth / 2 - titleContainer.width / 2;
        } else {
            titleContainer.x = window.innerWidth / 2 - titleContainer.width / 2;
        }

        if (window.innerWidth > window.innerHeight) {
            /*gameContainer.x = window.innerWidth / 2 - gameContainer.width / 2;*/
            setTimeout(function () {
                footerContainer.x = window.innerWidth / 2 - footerContainer.width / 2;

                footerContainer.y = window.innerHeight - footerContainer.height - 12;
                actionContainer.x = window.innerWidth - actionContainer.width;
                actionContainer.y = 0;
            }, 200)


        } else {
            footerContainer.x = 0;
            footerContainer.y = (SYMBOL_SIZE * 3) / gameContainer.scale.y;
            //footerContainer.y = window.innerHeight / 2//- ((window.innerHeight - gameContainer.height * gameContainer.scale.y) / 2 - actionContainer.height);
            actionContainer.y = window.innerHeight - 150;// TODO: need to calculate exact height of actionContainer
            //actionContainer.y = gameContainer.y + gameContainer.height + 30;
            //actionContainer.y = footerContainer.y + footerContainer.height
            actionContainer.x = 0;
            //console.log(window.innerWidth + window.innerWidth * preloaderContainer.scale.x, 'window.innerWidth + window.innerWidth * preloaderContainer.scale.x')
            /*preloaderContainer.width = window.innerWidth + window.innerWidth * preloaderContainer.scale.x
            preloaderContainer.height = window.innerHeight*/
        }

        if(isMobile && !isLandscape()) {
            footerContainer.x = window.innerWidth/2 - footerContainer.width/3 +5;
            actionContainer.x = window.innerWidth/2 - actionContainer.width/1.5 + 3;

            oAL.highlightedTitleImg && oAL.highlightedTitleImg.scale.set(0.5, 0.5);
            // gameCompleted.containerLineIndicators.y = -window.innerHeight/4;
            gameCompleted.containerLineIndicators.y = -GAME_HEIGHT_PORTRAIT/7;
            // gameCompleted.containerLineIndicators.x = -window.innerWidth/2.7;
            gameCompleted.containerLineIndicators.x = -GAME_WIDTH_PORTRAIT/4.5;

            if(gameContainer.getChildByName("game-background-image") && gameContainer.getChildByName("game-background-image").visible === true) {
                gameContainer.getChildByName("game-background-image").visible = false
            }

        } else if(isMobile && isLandscape()) {
            oAL.highlightedTitleImg && oAL.highlightedTitleImg.scale.set(1, 1);
            gameCompleted.containerLineIndicators.y = 0;
            gameCompleted.containerLineIndicators.x = 10;


            if(gameContainer.getChildByName("game-background-image") && gameContainer.getChildByName("game-background-image").visible === false) {
                gameContainer.getChildByName("game-background-image").visible = true
            }

        }

        await repositionGameContainer();
        await repositionFooterContainer();
        await repositionRespinTitle();
        repositionActionContainer();

    } else if (!isMobile()) {
        actionContainer.y = GAME_HEIGHT - actionContainer.height - 12;
        // titleContainer.x = window.innerWidth / 4 - titleContainer.width ;
        titleContainer.x =GAME_WIDTH/4 ;
    }

    resetSideIndicators()
}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
        //console.log(e)
    }
};

function isMobile() {
    /*return settings.DEVICE !== 'D';*/
    return !DESKTOP_INITIALS.includes(settings.DEVICE)

}

function isLandscape() {
    return window.innerWidth > window.innerHeight
}

function tickerStartBalance() {
    let tickStart = tickerStart.play();
    tickStart.on('end', function () {
        tickerMidBalance()
    });
}

function tickerMidBalance() {
    if (settings.STOP_PAYLINE_ANIMATION || settings.GAME_RUNNING)
        return;
    let tickMid = tickerMid.play();
    tickMid.on('end', function () {
        //console.log(settings.TICK_STATUS + ' => ' + settings.GAME_RUNNING +' settings.TICK_STATUS')
        if (!settings.TICK_STATUS || settings.GAME_RUNNING) {
            if (!settings.STOP_PAYLINE_ANIMATION || !settings.GAME_RUNNING) {
                let tEnd = tickerEnd.play();
                tEnd.on('end', function () {
                    setSettings.TICKER_SOUND_STATUS = false;
                })
            }
        } else if (!settings.STOP_PAYLINE_ANIMATION)
            tickerMidBalance();
    })
}

function checkTickerStatus() {
    return new Promise((resolve) => {
        var waitForElement = async function () {
            if (!settings.TICKER_SOUND_STATUS) {
                resolve(1);
            } else {
                window.requestAnimationFrame(waitForElement);
            }
        };
        waitForElement();
    })
}

async function waitUntilIsTrue(property) {
    return new Promise((resolve) => {
        var wait = async function () {

            if (settings[property]) {
                return resolve(1);
            } else {
                await sleep(100)
                wait();
            }
        };
        wait();
    })
}

async function waitUntilIsDone(property) {
    return new Promise((resolve) => {
        var wait = async function () {

            if (!settings[property]) {
                return resolve(1);
            } else {
                await sleep(100)
                wait();
            }
        };
        wait();
    })
}

function getUrlParameters() {
    var params = window.location.search.split('&');
    var paramsArr = {};
    $.each(params, function (index, val) {
        paramsArr[val.split('=')[0].replace('?', '')] = val.split('=')[1];
    });
    return paramsArr;
}

function validateGameLaunch(real) {
    var params = getUrlParameters();
    var validParams = true;

    if (real['real'] === 'demo') {
        return true
    } else {
        $.each(mandatoryParams, function (index, val) {
            if (!(val in params)) {
                validParams = false;
                return;
            }
        });
        return validParams;
    }
}

function plus() {

    if (footer.disableFooter) {
        return;
    }
    setSettings.STOP_PAYLINE_ANIMATION = true;
    setSettings.ACTIVITY = 8;

    if (sound)
        button.play()

    if (settings.MIN_BET >= 0.01 && settings.MIN_BET < 0.05) {
        footer.mobileCoinMinus.texture = oAL.panel['coins_minus_n'];
        footer.mobileCoinMinus.interactive = true;
        setSettings.MIN_BET = settings.MIN_BET + 0.01;
    } else if (settings.MIN_BET > 0.04 && settings.MIN_BET < 0.25) {
        setSettings.MIN_BET = settings.MIN_BET + 0.05;
    } else if (settings.MIN_BET >= 0.25 && settings.MIN_BET < 1) {
        setSettings.MIN_BET = settings.MIN_BET + 0.25;
    } else if (settings.MIN_BET >= 1 && settings.MIN_BET < 10) {
        setSettings.MIN_BET = settings.MIN_BET + 1;
    }
    if (settings.MIN_BET === 10) {
        footer.mobileCoinPlus.texture = oAL.panel['coins_plus_d'];
        footer.mobileCoinPlus.interactive = false;
        button.stop()
    }
    //footer.betAmount.y = footer.betImage.height - footer.betAmount.height;
}

function minus() {

    if (footer.disableFooter) {
        return;
    }
    setSettings.STOP_PAYLINE_ANIMATION = true;
    setSettings.ACTIVITY = 8;

    if (sound)
        button.play()


    if (settings.MIN_BET < 0.02) {
        setSettings.MIN_BET = 0.01;

    } else if (settings.MIN_BET < 0.06) {
        setSettings.MIN_BET = settings.MIN_BET - 0.01;
    } else if (settings.MIN_BET > 0.06 && settings.MIN_BET <= 0.25) {
        setSettings.MIN_BET = settings.MIN_BET - 0.05;
    } else if (settings.MIN_BET >= 0.25 && settings.MIN_BET <= 1) {
        setSettings.MIN_BET = settings.MIN_BET - 0.25;
    } else if (settings.MIN_BET <= 10 && settings.MIN_BET > 1) {
        setSettings.MIN_BET = settings.MIN_BET - 1;
        footer.mobileCoinPlus.texture = oAL.panel['coins_plus_n'];
        footer.mobileCoinPlus.interactive = true;
    }

    if (settings.MIN_BET === 0.01) {
        footer.mobileCoinMinus.texture = oAL.panel['coins_minus_d'];
        footer.mobileCoinMinus.interactive = false;
        button.stop()
    }
    //footer.betAmount.y = footer.betImage.height - footer.betAmount.height;
}

enableFullscreen = () => {

    let userInputEventNames = ['click', 'contextmenu', 'auxclick', 'dblclick', 'mousedown', 'mouseup', 'pointerup',

        'touchend', 'keydown', 'keyup'];

    for (var event of userInputEventNames) {

        document.addEventListener(event, this.requestFullscreen);

    }

};

requestFullscreen = () => {

    if (this.requesting)

        return;

    this.requesting = true;

    this.removeListeners();


    if (screenfull.isEnabled)

        screenfull.request();

};

removeListeners = () => {

    let userInputEventNames = ['click', 'contextmenu', 'auxclick', 'dblclick', 'mousedown', 'mouseup', 'pointerup',

        'touchend', 'keydown', 'keyup'];

    for (var event of userInputEventNames) {

        document.removeEventListener(event, this.requestFullscreen);
    }
}

function reScaleContainer(container) {

    const vpw = window.innerWidth;  // Width of the viewport
    const vph = window.innerHeight; // Height of the viewport
    let nvw; // New game width
    let nvh; // New game height

    if (vph / vpw < GAME_HEIGHT / GAME_WIDTH) {
        nvh = vph;
        nvw = (nvh * GAME_WIDTH) / GAME_HEIGHT;
    } else {
        // In the else case, the opposite is happening.

        nvw = vpw;
        nvh = (nvw * GAME_HEIGHT) / GAME_WIDTH;
    }

    container.scale.set(nvw / GAME_WIDTH, nvh / GAME_HEIGHT);
}

function showHideContainers() {
    //curtainsContainer.visible = !(isMobile() && !isLandscape());

    /*if (typeof oAL.sideIndicatorsImg !== 'undefined')
        oAL.sideIndicatorsImg.visible = !(isMobile() && !isLandscape());*/

    //footer.betLinesContainer.visible = !(isMobile() && !isLandscape() && !!!settings.FS_STATUS)

    if(isMobile() && !isLandscape()){
        footer.betLinesContainer.visible = settings.FS_STATUS
        if(settings.FS_STATUS){
            footer.linesBetImage.texture = oAL.panel['lines_mask']['mobile']['portrait']['freeGame'];
            footer.betLinesContainer.y = footer.balanceContainer.y + footer.balanceContainer.height + 10
            /*footer.betLinesContainer.x = footerContainer.width / 2 - footer.betLinesContainer.width / 2;*/
            footer.betLinesContainer.x = GAME_WIDTH / 2 - footer.linesBetImage.width / 2
            footer.linesBetAmount.y = footer.linesBetAmount.y - 10;
        } else {
            if(!!oAL.panel['lines_mask'])
                footer.linesBetImage.texture = oAL.panel['lines_mask']['mobile']['portrait']['baseGame'];
        }
    }
}

function fullScreenLayer(){
    let graphics = new PIXI.Graphics();
    graphics.width = window.innerWidth;
    graphics.name = 'fs-graphic';
    graphics.height = window.innerHeight;

    graphics.beginFill(0x000000);

    graphics.lineStyle(1, 0xFFFFFF);


    graphics.drawRoundedRect(0, 0, window.innerWidth, window.innerHeight, 5);

    graphics.buttonMode = true;
    graphics.interactive = true;
    graphics.alpha = 0;
    graphics.zIndex = 99999;

    graphics.on('pointerdown', () => {
        console.log('go to full screen');
        graphics.visible = false;
        goToFullScreen()
    });
    app.stage.addChild(graphics);
}

function onBalanceDisplayed(balanceValue){
    console.log('test123');
    // check for taas widget, then update it
    let eventName = "GameBalanceDisplayed";
    let widgetClass = "ranking-widget-container";
    let widgetEl = document.getElementById(widgetClass);
    let event = new CustomEvent(eventName,
        { bubbles: true, detail: {balance: balanceValue} });
    if (widgetEl) {
        widgetEl.dispatchEvent(event);
    } else {
       /* if (sc.debug) {
        console.log(sc.utils.currentTime + "[Services] missing ranking-widget-container element"); }*/
        console.log('oops')
    }
}