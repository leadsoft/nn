class freeSpin {

    constructor() {
        this.status = false;
        this.name = 'free-spin';
        this.spritesheet = [];
        this.runnerAnimation = [];
        this.dialog = [];
        this.curtainsStatus = false;
        this.slotTextures = [];
        this.bigSlotTextures = [];
        this.reels = [];
        this.tweening = [];
        this.slotPosition = [];
        this.reelContainer = new PIXI.Container();
        this.reelBackground = new PIXI.Sprite();
        this.reelOverlay = new PIXI.Sprite(PIXI.Texture.WHITE);
        this.reelOverlay.tint = 0x000000;
        this.symbols = [];
        this.reelContainer.name = 'free-s-reel-container';
        this.anticipationStatus = false;
        this.freeSpinPanel = {};
        this.totalWin = '';

        this.totalWin = new PIXI.Text('TOTAL WIN', {
            font: "10px Arial",
            fill: "0xffffff",
            fontWeight: 'bold',
            fontSize: 11
        });
        this.totalWin.name = 'total-win-text';
        /*this.totalWin.x = 693;
        this.totalWin.y = 525;*/
        this.totalWin.zIndex = 2;
        this.totalWin.visible = false;

        this.totalWinAmount = new PIXI.Text('', {
            font: "10px Arial",
            fill: isMobile() ? "0xffffff" : "0x000000",
            fontSize: 18
        });
        this.totalWinAmount.name = 'total-win-amount';
        this.totalWinAmount.zIndex = 2;
        /*this.totalWinAmount.visible = false;*/
        this.panelBackup = {};
        this.randomElement = {0: true, 1: true, 2: true};

        //gameContainer.addChild(this.totalWin);
        //gameContainer.addChild(this.totalWinAmount);
        /*footer.totalBalanceImage.addChild(this.totalWinAmount);*/
        footer.totalBalanceContainer.addChild(this.totalWinAmount);

        app.ticker.add((delta) => {
            let now = new Date();

            const remove = [];
            for (let i = 0; i < this.tweening.length; i++) {

                const t = this.tweening[i];

                const phase = Math.min(1, (now - t.start) / t.time);
                t.propertyBeginValue = 0; //reset value
                if (i === this.tweening.length - 1) {
                    t.target = settings.TARGET_PROPERTY /*+ 5*/; //reset value
                } else {
                    t.target = settings.TARGET_PROPERTY; //reset value*/
                }

                t.object[t.property] = freeSpin.lerp(t.propertyBeginValue, t.target, t.easing(phase));

                if (t.change)
                    t.change(t);

                if (phase > 0.3)
                    this.randomElement[parseInt(t.object.reelPosition)] = false;

                if (phase === 1) {
                    t.object[t.property] = t.target;

                    if (parseInt(t.object.reelPosition) === 0 && spinH.firstReel.includes(settings.FS_FEATURES_RES)) {

                        this.anticipationStatus = true;
                        game.anticipation.forEach(function (val, key) {
                            if (key !== 1) {
                                val.visible = true;
                                val.gotoAndPlay(0)
                            }
                        });
                        setSettings.ANTICIPATION = true;
                    }
                    if (parseInt(t.object.reelPosition) === 2)
                        setSettings.ANTICIPATION = false;
                    reelStop.play();
                    if (t.complete) t.complete(t);
                    remove.push(t);
                } else if (settings.STOP_REEL) {
                    for (let l = 0; l < Object.keys(game.randomElement).length; l++) {
                        this.randomElement[l] = false;
                    }
                    setSettings.ANTICIPATION = false;
                    reelStop.play();
                    footer.winAmount.text = '';
                    t.object.scatterSymbol = false;
                    t.object[t.property] = t.target;
                    if (t.complete) t.complete(t);
                    remove.push(t);
                }

                if (this.anticipationStatus && parseInt(t.object.reelPosition) === 2) {
                    t.time += 2000;
                    t.start = Date.now(Date.now());
                    t.target = 60; //reset value
                    this.anticipationStatus = false;
                }
            }

            for (let i = 0; i < remove.length; i++)
                this.tweening.splice(this.tweening.indexOf(remove[i]), 1);

        });

        app.ticker.add((delta) => {
            // Update the slots.
            /*if (this.spinStatus)*/
            for (let i = 0; i < this.reels.length; i++) {
                const r = this.reels[i];
                // Update blur filter y amount based on speed.
                // This would be better if calculated with time in mind also. Now blur depends on frame rate.
                r.blur.blurY = (r.position - r.previousPosition) * 40;
                r.previousPosition = r.position;

                // Update symbol positions on reel.
                for (let j = 0; j < r.symbols.length; j++) {
                    const s = r.symbols[j];
                    const prevy = s.y;
                    if (i === 1)
                        s.y = ((r.position + j) % r.symbols.length) * s.height - s.height + (j * SYMBOL_MARGIN_TOP);
                    else
                        s.y = ((r.position + j) % r.symbols.length) * SYMBOL_CONTAINER_SIZE - SYMBOL_CONTAINER_SIZE /*+ (j * SYMBOL_MARGIN_TOP)*/;

                    if ((s.y < 0 && prevy > SYMBOL_SIZE * 3 && i === 1) || (s.y < 0 && prevy > SYMBOL_SIZE) || Object.keys(spinH.slotPosition).length > 0) {

                        if (typeof spinH.freeSlotPosition[i] !== "undefined") {
                            r.reelPosition = i;
                            let symbolSource = i !== 1 ? this.slotTextures : this.bigSlotTextures;
                            //console.log(spinH.freeSlotPosition, 'spinH.freeSlotPosition');
                            s.texture = symbolSource[spinH.freeSlotPosition[i][j]];
                            /*let tezt = new PIXI.Text(j, {font: "Arial", fill: "0xffe000"})
                            tezt.style.fontSize = 18;
                            s.addChild(tezt);*/
                            //s.texture = !this.randomElement[i] ? symbolSource[spinH.freeSlotPosition[i][j]] : symbolSource[Math.floor(Math.random() * symbolSource.length)];
                            s.visible = true;
                            s.symbolElement = spinH.freeSlotPosition[i][j];
                            //if(!this.randomElement[i]) {
                            /*delete spinH.freeSlotPosition[i][j];
                            if (Object.keys(spinH.freeSlotPosition[i]).length === 0)
                                delete spinH.freeSlotPosition[i];*/
                            //}
                        }
                    }
                }
            }
        });
    }

    changeReelSymbolsValue() {
        for (let i = 0; i < this.reels.length; i++) {
            const r = this.reels[i];
            for (let j = 0; j < r.symbols.length; j++) {
                const s = r.symbols[j];
                let symbolSource = i !== 1 ? this.slotTextures : this.bigSlotTextures;
                spinH.freeSlotPosition[i][j] = i !== 1 ? spinH.freeSlotPosition[i][j] : 9;
                s.texture = symbolSource[spinH.freeSlotPosition[i][j]];
                s.visible = true;
                s.symbolElement = spinH.freeSlotPosition[i][j];
            }
        }
    }

    init() {
        this.dialogContainer = new PIXI.Container();
        this.dialogContainer.zIndex = 1;
        this.dialogContainer.zIndex = 12;
        this.dialogContainer.name = 'dialog-container';
        this.dialogContainer.visible = false;
        this.dialogBoxStageLight = new PIXI.Sprite(this.dialog.textures['themedTemplates/stage_light.png']);
        this.dialogBoxStageLight.alpha = 0;
        this.dialogBoxImage = new PIXI.Sprite(this.dialog.textures['themedTemplates/Dialogue-box.png']);

        this.lightFlashing = new PIXI.AnimatedSprite(this.dialog.animations['themedTemplates/dialogBoxLights']);
        /** DONE: Set text #1 */
        this.dialogBoxText = new PIXI.Container()
        // this.dialogBoxText = new PIXI.Text("\nIT’S SHOW TIME.  \nFREE GAMES AWARDED", {
        //     font: "Arial",
        //     fill: "0x000000",
        //     fontSize: 32,
        //     fontWeight: 700,
        //     align: "center"
        // });
        /** */

        this.dialogContainer.addChild(this.dialogBoxStageLight, this.dialogBoxImage, this.lightFlashing, this.dialogBoxText);
        this.dialogContainer.y = 0;
        this.dialogBoxText.x = this.dialogContainer.width / 2 - this.dialogBoxText.width / 2;
        this.dialogBoxText.y = 210;
        this.lightFlashing.loop = true;
        this.lightFlashing.animationSpeed = 0.100;
        //this.lightFlashing.play();

        this.freeSpinContainer = new PIXI.Container();
        this.freeSpinContainer.name = 'free-spin-container';
        var graphics = new PIXI.Graphics();
        graphics.width = 300;
        graphics.height = 75;

        graphics.beginFill(0x000000);

        graphics.lineStyle(1, 0xFFFFFF);
        graphics.drawRect(0, 0, 300, 80);
        graphics.name = 'graphic';
        graphics.visible = false;
        graphics.alpha = 0.6;
        this.awardedText = new PIXI.Text("", {
            font: "Arial",
            fill: "0xffffff",
            fontSize: 15,
            align: "center"
        });

        this.freeSpinContainer.addChild(graphics);
        this.freeSpinContainer.addChild(this.awardedText);
        this.awardedText.x = (this.freeSpinContainer.width / 2) - this.awardedText.width / 2;
        this.awardedText.y = 5;

        this.freeSpinContainer.zIndex = 9;

        gameContainer.addChild(this.freeSpinContainer);
        gameContainer.addChild(this.dialogContainer);
        this.freeSpinContainer.x = 50;
        this.freeSpinContainer.y = -this.freeSpinContainer.height;

        this.animations = {
            /*"curtains_transition": this.spritesheet.transitions.animations['curtain close-open/curtain_close'],*/
            "frame_symbol_flashing": this.spritesheet.fsframes.animations['fsframe'],
            "background_frame_symbol": this.spritesheet.fsframes.animations['fsframe'],
            /*"frame_big_symbol_flashing": this.spritesheet.fsframes.animations['bigSymbols/fgBigSymHighlight'],*/
            "frame_big_symbol_flashing": this.spritesheet.fsframes.animations['bigSymbols/frame_hilight_lg'],
            "big_symbol_flashing": this.spritesheet.fsframes.animations['bigSymbols/lg_flashing'],
            "big_background_frame_symbol": this.spritesheet.fsframes.animations['fsframe'],
        };

        this.curtainsAnimation();
        this.symbols = this.spritesheet;

        /*this.courtainsAnimationOpen = new PIXI.AnimatedSprite(freeS.animations['curtains_transition']);
        this.courtainsAnimationOpen.loop = false;
        this.courtainsAnimationOpen.zIndex = 7;
        this.courtainsAnimationOpen.animationSpeed = 0.450;
        app.stage.addChild(this.courtainsAnimationOpen);*/

        /*this.slotTexturesHighlighted = {
            'symbol0_h': this.spritesheet.static.textures["standard size/fssym_wild_n.png"],
            'symbol1_h': this.spritesheet.static.textures["standard size/fssym_ms1_n.png"],
            'symbol2_h': this.spritesheet.static.textures["standard size/fssym_ms2_n.png"],
            'symbol3_h': this.spritesheet.static.textures["standard size/fssym_ms3_.png"],
            'symbol4_h': this.spritesheet.static.textures["standard size/fssym_ms4_n.png"],
            'symbol5_h': this.spritesheet.static.textures["standard size/fssym_A_n.png"],
            'symbol6_h': this.spritesheet.static.textures["standard size/fssym_K_n.png"],
            'symbol7_h': this.spritesheet.static.textures["standard size/fssym_Q_n.png"],
            'symbol8_h': this.spritesheet.static.textures["standard size/fssym_J_n.png"],
            'symbol9_h': this.spritesheet.static.textures["standard size/fssym_10_n.png"],
            'symbol10_h': this.spritesheet.static.textures["standard size/fssym_9_n.png"],
            'symbol11_h': this.spritesheet.static.textures["standard size/fssym_scatter_n.png"]
        };*/
    }

    curtainsAnimation() {
        /* const multiPack = freeS.spritesheet.transitions[0].data.meta.related_multi_packs;

         let multiPackTextureMap = {...freeS.spritesheet.transitions[0].textures};


         if (multiPack && multiPack.length > 0) {
             multiPack.forEach((mpack) => {
                 const textureName = mpack.replace(".json", "");
                 const _mpackSheet = app.loader.resources[textureName];
                 if (_mpackSheet.textures) {

                     multiPackTextureMap = Object.assign(
                         multiPackTextureMap,
                         _mpackSheet.textures
                     );
                 }
             });
         }
         let multiPackTextureMapArray = Object.values(multiPackTextureMap)
         multiPackTextureMapArray.reverse();*/
        /*this.runnerAnimation = new PIXI.AnimatedSprite(multiPackTextureMapArray);*/
        this.runnerAnimation = new PIXI.AnimatedSprite(freeS.spritesheet.transitions.animations['curtainClose']);
        this.runnerAnimation.name = 'curtains';

        this.runnerAnimation.animationSpeed = 0.275;
        this.runnerAnimation.loop = false;
        this.runnerAnimation.visible = true
        this.runnerAnimation.zIndex = 3;

        //curtainsContainer.addChild(this.runnerAnimation);
    }

    async startFreeSpin() {
        await waitUntilIsTrue('GAME_START');
        this.changeReelSymbolsValue();
        disableEnableButtons(false);
        footer.showHideDarkSpinButton(true, 'startFreeSpin');
        await this.enable();
        oAL.sound.background.pause();

        spinH.activeSpinButton();
    }

    async stopFreeSpin() {
        console.log('STOP-FREE-SPIN');
        /*if(game.paylines.length > 0 && settings.FS_STOP)
            await waitUntilIsDone('WINNING_ROTATION_STATUS')*/
        await waitUntilIsDone("GAME_RUNNING")
        footer.showHideDarkSpinButton(true, 'stopFreeSpin');
        disableEnableButtons(false);
        setSettings.REQUEST_END = false;
        await waitUntilIsDone('REQUEST_END');
        /** DONE: Set text after "reward" */
        await this.showDialogBox('reward')
        // await this.showDialogBox('reward', "WELL DONE\n FREE GAMES ENDED \n" + user.currencySymbol + parseFloat(settings.FS_TOTAL_WIN).toFixed(2) + '\nHAS BEEN ADDED \nTO YOUR BALANCE.');
        /** */
        //modalDialog.show('We hope that you have enjoyed your complementary game play!<br>You have won <span style="color: #FFFF00">' + user.currencySymbol + parseFloat(settings.FS_TOTAL_WIN).toFixed(2) + '</span> which has been added to your balance.', 'reward')
        //resetWinContainerStyling()
        await sleep(1000);

        /*setSettings.PROMO_CHIPS += parseFloat(settings.FREE_SPIN_TOTAL_WIN).toFixed(2)*/
        setSettings.CAN_BET = true;

        /*footer.spinButtonSprite.visible = true;*/
        await waitUntilIsDone("MODAL_DIALOG_STATUS")
        /*let intID = setInterval(() => {
            if(freeSpinBackground.volume > 0) {
                freeSpinBackground.volume -= 0.15
            } else {
                clearInterval(intID)
            }
        }, 100)*/

        await this.disabled();

        oAL.sound.background.play();
    }

    async enable() {
        footer.totalBalanceContainer.x = 635//footerContainer.width - footer.totalBalanceContainer.width / 2
        let self = this;
        setSettings.FS_RUNNING_STATUS = true;
        return new Promise(async (resolve, reject) => {
            setSettings.FS_ENABLE_STATUS = true;
            // let freeSpinTriggerSound = freeSpinTrigger.play();
            // freeSpinTriggerSound.on('end', async function () {
            //     await self.openCloseCurtains(true);
            //
            //
            //     self.switchBackground(true);
            //     self.switchPanel(true);
            //     self.switchCurrentReelPanel();
            //     await sleep(500);
            //     /*await */
            //     self.openCloseCurtains(false);
            //
            //     /*let modifiedText = new PIXI.Text("BIG SCREEN", {
            //         font: "Arial",
            //         fill: "0x000000",
            //         fontSize: 32,
            //         fontWeight: 800,
            //     });
            //
            //     let introText = "\nIT’S SHOW TIME.  \n" + modifiedText + "\n GAMES AWARDED\n";
            //     await self.showDialogBox('initial', introText);*/
            //     await self.showDialogBox();
            //     //await self.showDialogBox(true);
            //     //await self.showDialogModal(true);
            //     self.curtainsStatus = true;
            //     return resolve();
            // })
        })
    }

    async disabled() {
        let self = this;
        return new Promise(async (resolve, reject) => {
            setSettings.FS_ENABLE_STATUS = false;
            self.hideAndResetModal()
            await sleep(500)
            await self.openCloseCurtains(true, 'close');
            self.switchBackground(false);
            self.switchPanel(false);
            self.switchCurrentReelPanel(false);
            await sleep(500)
            disableEnableButtons(true);
            footer.showHideDarkSpinButton(false, 'disabled');
            await self.openCloseCurtains(false, 'close');
            await sleep(500);
            self.curtainsStatus = false;
            setSettings.FREE_GAME_AWARDED_SOUND = false;
            setSettings.FS_RUNNING_STATUS = false;
            if(spinH.autospin)
                spinH.activeSpinButton();

            return resolve();
        })
    }

    async openCloseCurtains(courtainStatus, fsStatus = 'open') {
        let self = this;
        return new Promise(async (resolve, reject) => {

                footer.hideElementsForCurtains(false, !settings.DEVICE.includes('D'));
                if (!settings.DEVICE.includes('D') && footer.mobileBetSet.visible === true)
                    footer.closeCoinsAction();

                if (!settings.DEVICE.includes('D') && footer.mobileInfoBar.visible === true)
                    footer.closeMenuAction();

                let cStart = curtainStart.play();
                cStart.on('end', function () {
                    curtain.loop = true;
                    curtain.play();
                });
                this.runnerAnimation.gotoAndPlay(0);

                /*if (!courtainStatus && fsStatus === 'open') {
                    await self.showDialogBox();
                    return resolve();
                }*/
                this.runnerAnimation.onFrameChange = async function (a) {
                    let f = 8 / freeS.runnerAnimation.totalFrames;
                    if (courtainStatus) {
                        freeS.runnerAnimation.zIndex = settings.DEVICE.includes('D') ? 7 : 10;
                        game.reelOverlay.alpha += f / 10;
                    } else if (!courtainStatus && fsStatus === 'close')
                        game.reelOverlay.alpha = 0;
                };
                this.runnerAnimation.onComplete = function () {
                    footer.hideElementsForCurtains(true, !settings.DEVICE.includes('D'));
                    curtain.stop();
                    curtainClose.play();
                    setSettings.STOP_PAYLINE_ANIMATION = true;

                    self.runnerAnimation.textures.reverse();

                    if (!courtainStatus)
                        freeS.runnerAnimation.zIndex = 3;

                    if (!courtainStatus && fsStatus === 'open') {
                        /*freeSpinStart.play();*/
                        //await self.showDialogBox();
                    }

                    if (fsStatus === 'close')
                        footer.showHideSpinButton(true);

                    return resolve();
                };
            }
        )
    }

    switchBackground(freeSpinStatus) {

        oAL.gameBackgroundImg.texture = freeSpinStatus ? this.spritesheet.background.textures['freespins_bg.png'] : oAL.backgroundElements['background'];
        oAL.backgroundImg.texture = freeSpinStatus ? oAL.backgroundElements['free_spin_portrait_background'] : oAL.backgroundElements['portrait_background'];
        //titleContainer.visible = freeSpinStatus && isMobile() & !isLandscape();
        //oAL.gameBackgroundImg.x = oAL.gameBackgroundImg.parent.width / 2 - oAL.gameBackgroundImg.width / 2;

        oAL.titleImg.texture = freeSpinStatus ? this.spritesheet.background.textures['freespins_title.png'] : oAL.backgroundElements['title'];
        /*oAL.titleImg.x = (GAME_WIDTH / 2) - (oAL.titleImg.width / 2);*/
        game.reelBackground.texture = freeSpinStatus ? this.spritesheet.background.textures['reel_bg_02.png'] : (isMobile() ? oAL.backgroundElements['reel_background'] : '')
        // game.reelBackground.texture = PIXI.Texture.from('newAssets/nn/dist/basegame/desktop/baseGameLayout/baseGameBg.png');

        //game.reelBackground.x = game.reelBackground.parent.width / 2 - game.reelBackground.width / 2;
        game.reelBackground.x = freeS.reelContainer.x;

        if (isMobile() && !isLandscape()) {
            /*game.reelBackground.y = (freeSpinStatus ? -25 : 100);*/
            game.reelBackground.y = -25;
        } else {
            game.reelBackground.y = 100//REEL_CONTAINER_MARGIN_TOP
        }
    }

    switchPanel(freeSpinStatus) {
        showHideContainers();
        let winText = footer.winContainer.getChildByName('win-text');
        let totalBalanceText = footer.totalBalanceContainer.getChildByName('total-balance-text');
        let betText = footerContainer.getChildByName('bet-container').getChildByName('bet-text');
        if (freeSpinStatus) {
            this.freeSpinContainer.visible = false;
            footer.footerBgTexture.texture = this.freeSpinPanel['footer_panel_bg'];
            //oAL.panel = this.freeSpinPanel;

            footer.linesBetAmount.text = !freeSpinStatus ? settings.START_GAME_PAYLINES : settings.FS_COUNT;

            let linesBet = footerContainer.getChildByName('bet-lines-container').getChildByName('lines-bet-text')
            if (!isMobile()) {
                /*return;*/
                /*footer.totalBalanceContainer.scale.set(footer.winContainer.scale.x, footer.winContainer.scale.y);
                footer.totalBalanceContainer.width = footer.totalBalanceContainer.getChildByName('total-balance-image').width;*/
                footer.winContainer.x = 505//footer.betContainer.x;
                footer.winContainer.width = 147//footer.betContainer.width;
                //footer.winContainer.scale.set(1, 1)
                //footer.totalBalanceContainer.height = footer.winContainer.height ;
                betText.y = 16;
                let totalWinText = footer.totalBalanceContainer.getChildByName('total-balance-text');
                linesBet.x = 0;
                linesBet.text = 'FREE SPINS REMAINING';
                linesBet.style = {fill: '0xffffff', wordWrap: true, fontSize: 11};
                linesBet.x = linesBet.parent.width / 2 - linesBet.width / 2;
                /*footer.linesBet.x = 204;
                footer.linesBet.y = 6;*/
                footer.linesBetAmount.style = {fill: '0xffffff', wordWrap: true, fontSize: 14};
                /*footer.linesBetAmount.x = 232;*/
                footer.betImage.visible = false;
                footer.totalBalanceContainer.visible = true;
                footer.plusButton.visible = false;
                footer.minusButton.visible = false;
                //footer.winImage.x = footer.winImage.x - footer.totalBalanceImage.width;
                this.totalWinAmount.y = footer.winAmount.y - 4;/*TODO: need to remove -4*/
                this.totalWinAmount.x = (footer.totalBalanceContainer.width / 2) - (this.totalWinAmount.width / 2)

                totalBalanceText.style = winText.style = {fill: "0xffffff", fontSize: 16};
                totalBalanceText.y = winText.y
                footer.paylineWin.style = {fill: "0xfcc603", fontSize: 12};
                footer.winAmount.style = {fill: "0xffffff", fontSize: 18};
                this.totalWin.zIndex = 9;
                this.totalWinAmount.zIndex = 9;
                //this.totalWin.visible = true; /*TODO: comment for testing*/
                this.totalWinAmount.visible = true;
                this.totalWinAmount.style.fill = footerContainer.getChildByName('total-balance-container').getChildByName('total-balance-text').style.fill = 0xffffff
                footerContainer.getChildByName('total-balance-container').getChildByName('total-win-amount').style = {
                    fill: 0xffff00,
                    fontSize: 32,
                    fontWeight: 'bold'
                }
                footer.betContainer.visible = false;
                footer.centerElements([
                    {
                        element: footer.winContainer.getChildByName('win-text'),
                        container: footer.winContainer
                    },
                    {
                        element: footer.winAmount,
                        container: footer.winContainer
                    },
                    {
                        element: totalWinText,
                        container: footer.totalBalanceImage
                    },
                    {
                        element: this.totalWinAmount,
                        container: footer.totalBalanceImage
                    }
                ]);

            } else {
                betText.text = 'WIN';

                winText.text = 'TOTAL WIN'
                footer.betAmount.text = '';
                if(!isLandscape()){
                    linesBet.text = /*settings.GAME_PLAY_MODE === 'demo' ? 'LINES' : */'FREE SPINS REMAINING';
                } else {
                    linesBet.text = /*settings.GAME_PLAY_MODE === 'demo' ? 'LINES' : */'FREE SPINS\nREMAINING';
                    /*linesBet.style.fontSize = 15;*/
                    linesBet.y = 4;
                }

                linesBet.x = linesBet.parent.width / 2 - linesBet.width / 2;

                //footer.linesBet.x = settings.GAME_PLAY_MODE === 'demo' ? 85 : 55;
                /*footer.linesBet.y = 4;*/
                /*footer.linesBetAmount.x = settings.GAME_PLAY_MODE === 'demo' ? 85 : 105;*/
                /*TODO: need to comment out bonus Image on mobile*/

                /*footer.balanceImage.getChildByName('balance-text').text = 'BALANCE';*/
                /*footer.bonusImage.getChildByName('bonus-text').text = settings.GAME_PLAY_MODE === 'demo' ? 'DEMO MODE' : 'BONUS';
                footer.bonusImage.getChildByName('bonus-text').x =  (footer.bonusImage.width / 2) - (footer.bonusImage.getChildByName('bonus-text').width / 2);*/
                footer.totalBalanceContainer.visible = false;
                footer.betImage.visible = true;//settings.GAME_PLAY_MODE === 'demo';
                footer.totalBalanceImage.visible = true;


                this.totalWin.visible = settings.GAME_PLAY_MODE !== 'demo';
                this.totalWin.style = {/*font: "10px Arial", */fill: "0xff8080", fontSize: 20};
                this.totalWinAmount.style = {/*font: "10px Arial", */fill: 0xffffff, fontSize: 20};
                this.totalWinAmount.visible = settings.GAME_PLAY_MODE !== 'demo';
                this.totalWin.zIndex = settings.GAME_PLAY_MODE !== 'demo' ? 9 : 0;
                this.totalWinAmount.zIndex = settings.GAME_PLAY_MODE !== 'demo' ? 9 : 0;
                textAdjustment(!settings.DEVICE.includes('D'))
                footer.betAmount.style.fontSize = 20;
            }
            footer.linesBetAmount.text = settings.FS_COUNT;

            footer.linesBetAmount.x = footer.linesBetImage.width / 2 - footer.linesBetAmount.width / 2;

            footer.plusButton.visible = false;
            footer.minusButton.visible = false;
            this.totalWinAmount.visible = true;

        } else {

            footer.betContainer.visible = true;
            footer.totalBalanceContainer.visible = false;
            footer.retrieveFooterValues(!settings.DEVICE.includes('D'));

            /*winText.x = winText.parent.width / 2 - winText.width / 2;*/
            setFooterContainerPosition();
            if(isMobile()){
                betText.text = 'BET';
                footer.betAmount.text = user.currencySymbol + settings.START_GAME_BET.toFixed(2);

                winText.text = 'WIN'
                textAdjustment(!settings.DEVICE.includes('D'))
            }
        }

        //textAdjustment(!settings.DEVICE.includes('D'))
    }

    async showDialogBox(flag = 'initial', awardedText = "\nIT’S SHOW TIME.  \nBIG SCREEN FREE\n GAMES AWARDED\n") {
        let self = this;
        this.dialogContainer.visible = true;
        self.lightFlashing.play();
        this.dialogContainer.y = isMobile() && !isLandscape() ? -160 : 0;
        /**Set font size #2 */
        // this.dialogBoxText.text = awardedText;
        // self.dialogBoxText.style.fontSize = 30
        if (flag !== 'initial') {
            game.reelOverlay.alpha = 0.4;
            // self.dialogBoxText.style.fontSize = 30
            self.dialogBoxText.removeChildren()
            self.dialogBoxText.addChild(this.freeSpinEndText())
        } else {
            self.dialogBoxText.removeChildren()
            self.dialogBoxText.addChild(this.freeSpinStartText())
        }
        /** */
        //await sleep(100000000000000000000);
        this.dialogBoxText.x = this.dialogContainer.width / 2 - this.dialogBoxText.width / 2;
        /** DONE: Center the text vertically
         * Review hard-coded value */
        this.dialogBoxText.y = this.dialogContainer.height / 2 - this.dialogBoxText.height / 2 - 50
        // this.dialogContainer.height / 2 - this.dialogBoxText.height / 2;
        /** */
        let featurePage = app.loader.resources['feature_page'].spritesheet;
        let playButton = new PIXI.Container();
        playButton.name = 'play-button';
        playButton.visible = true;
        playButton.y = 330;
        playButton.buttonMode = true;
        playButton.interactive = true;
        var graphics = new PIXI.Sprite(featurePage.textures['fpContinue_n.png']);

        return new Promise(async (resolve, reject) => {
            if (!self.dialogContainer.getChildByName('playButton')) {
                playButton.addChild(graphics);
                playButton.name = "playButton";
                playButton.x = this.dialogContainer.width / 2 - playButton.width / 2

                playButton.on('pointerdown', function () {
                    graphics.texture = featurePage.textures['fpContinue_p.png']
                    self.hideDialogBox(flag);
                    return resolve();
                })
                    .on('pointerover', function () {
                        graphics.texture = featurePage.textures['fpContinue_h.png']
                    })
                    .on('pointerout', function () {
                        graphics.texture = featurePage.textures['fpContinue_n.png']
                    });

                self.dialogContainer.addChild(playButton);
            } else {
                self.dialogContainer.getChildByName('playButton').visible = (flag === 'initial')
            }

            TweenMax.to(self.dialogBoxStageLight, 0.5, {
                boxShadow: "0px 0px 20px 5px rgba(255,255,255)",
                repeat: 8,
                alpha: 1,
                yoyo: true,
                onComplete: async function () {
                    //await sleep(1000);
                    self.hideDialogBox();
                },
                onStart: async function (e) {
                    setSettings.FREE_GAME_AWARDED_SOUND = true
                },
            });

            await sleep(4000)
            return resolve();
        });
    }

    hideDialogBox(flag) {
        let self = this;
        TweenMax.to(self.dialogContainer, 1, {
            onComplete: async function () {
                //await self.showInfoModal(true);
                self.awardedTextControl();
                game.reelOverlay.alpha = 0;
                self.dialogContainer.visible = false;
            },
            onUpdate: function () {
                let f = 8 / freeS.runnerAnimation.totalFrames;

                if (game.reelOverlay.alpha > 0)
                    game.reelOverlay.alpha -= f / 10
            },
            onStart: async function (e) {
                settings.SWITCH_LINES_BET = flag === 'initial';
                self.lightFlashing.stop();
                self.dialogBoxStageLight.alpha = 0;
            },
            y: -self.dialogContainer.height,
            ease: Bounce.easeOut,
        });
    }

    async hideAndResetModal() {
        let self = this;
        return new Promise((resolve, reject) => {
            TweenMax.to(self.freeSpinContainer, 0.6, {
                onComplete: function (e) {
                    self.awardedText.text = "";
                    self.awardedText.x = (self.freeSpinContainer.width / 2) - self.awardedText.width / 2;
                    self.freeSpinContainer.x = GAME_WIDTH / 2 - self.freeSpinContainer.width / 2;

                    return resolve();
                },
                y: -self.freeSpinContainer.height, ease: Power0.easeOut,
            });
        })
    }

    async showInfoModal(showInfoModal) {
        let self = this;
        return new Promise((resolve, reject) => {
            TweenMax.to(this.freeSpinContainer, 0.6, {
                onStart: function () {
                    //console.log('start')
                },
                onComplete: function (e) {
                    //console.log('complete')
                    return resolve();
                },
                y: showInfoModal ? 0 : -self.freeSpinContainer.height, ease: Bounce.easeOut,
            });
        })
    }

    /**
     * USE WITH DEFAULT SYMBOL
     * */
    switchCurrentReelPanelOld(status = true) {
        for (let i = 0; i < game.reels.length; i++) {
            const r = game.reels[i];
            // Update symbol texture on current reel.
            for (let j = 0; j < r.symbols.length; j++) {
                const s = r.symbols[j];
                let symbolSource = settings.FS_ENABLE_STATUS ? freeS.slotTextures : game.slotTextures;
                s.texture = symbolSource[s.symbolElement];
            }
        }
    }

    /**
     * USE WITH DIFFERENT SYMBOL
     * */
    switchCurrentReelPanel(status = true) {

        if (status) {
            game.reelContainer.visible = false;
            freeS.reelContainer.visible = true;
            oAL.reelDividerImg.visible = false
        } else {
            /*GO TO NORMAL SPIN*/
            game.reelContainer.visible = true;
            freeS.reelContainer.visible = false;
            /*if ((isMobile() && isLandscape()) || !isMobile())*/
            oAL.reelDividerImg.visible = true
        }
    }

    awardedTextControl(startGame = false) {
        /*if (settings.FS_STATUS) {*/
        if(settings.FS_ENABLE_STATUS){
            this.awardedText.text = "",//"FREE SPIN \nTOTAL WIN: " + user.currencySymbol + (parseFloat(settings.FS_TOTAL_WIN)).toFixed(2) + "\n\n" + (settings.FS_COUNT - startGame) + " plays remaining";
                this.awardedText.x = (this.freeSpinContainer.width / 2) - this.awardedText.width / 2;
            this.totalWinAmount.text = user.currencySymbol + (parseFloat(settings.FS_TOTAL_WIN)).toFixed(2);
            if(isMobile()){
                footer.winAmount.text = user.currencySymbol + (parseFloat(settings.FS_TOTAL_WIN)).toFixed(2);
            }
            footer.centerElements([
                {
                    element: this.totalWinAmount,
                    container: footer.totalBalanceImage
                }
            ]);
            //this.totalWinAmount.x =  /*freeS.totalWinAmount.parent.width*/footer.totalBalanceImage.width - freeS.totalWinAmount.width - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;

            if (settings.SWITCH_LINES_BET)
                footer.linesBetAmount.text = settings.FS_COUNT;
        }
    }

    static lerp(a1, a2, t) {

        return a1 * (1 - t) + a2 * t;
    }

    backout(amount) {
        return (t) => (--t * t * ((amount + 1) * t + amount) + 1);
    }

    freeSpinStartText() {
        let container = new PIXI.Container()

        let row1 = new PIXI.Text(`! IT'S SHOWTIME !`, {
            fontSize: 24,
            fontWeight: 700
        })

        let row2 = new PIXI.Text(`BIG SCREEN`, {
            fontSize: 32,
            fontWeight: 700
        })

        let row3 = new PIXI.Text(`FREE GAMES`, {
            fontSize: 28,
            fontWeight: 700
        })

        let row4 = new PIXI.Text(`AWARDED`, {
            fontSize: 24,
            fontWeight: 700
        })

        container.addChild(row1)
        container.addChild(row2)
        container.addChild(row3)
        container.addChild(row4)
        row1.x = container.width / 2 - row1.width / 2
        row1.y = 0

        row2.x = container.width / 2 - row2.width / 2
        row2.y = row1.y + row1.height + 10

        row3.x = container.width / 2 - row3.width / 2
        row3.y = row2.y + row2.height

        row4.x = container.width / 2 - row4.width / 2
        row4.y = row3.y + row3.height

        return container
    }

    freeSpinEndText() {
        let container = new PIXI.Container()

        let row1 = new PIXI.Text(`WELL DONE`, {
            fontSize: 28,
            fontWeight: 700
        })

        let row2 = new PIXI.Text(`FREE GAMES ENDED`, {
            fontSize: 28,
            fontWeight: 700
        })

        let row3 = new PIXI.Text(`${user.currencySymbol}${(parseFloat(settings.FS_TOTAL_WIN)).toFixed(2)}`, {
            fontSize: 48,
            fontWeight: 700
        })

        let row4 = new PIXI.Text(`HAS BEEN ADDED`, {
            fontSize: 24,
            fontWeight: 700
        })

        let row5 = new PIXI.Text(`TO YOUR BALANCE`, {
            fontSize: 24,
            fontWeight: 700
        })

        container.addChild(row1)
        container.addChild(row2)
        container.addChild(row3)
        container.addChild(row4)
        container.addChild(row5)
        row1.x = container.width / 2 - row1.width / 2
        row1.y = 20

        row2.x = container.width / 2 - row2.width / 2
        row2.y = row1.y + row1.height

        row3.x = container.width / 2 - row3.width / 2
        row3.y = row2.y + row2.height + 10

        row4.x = container.width / 2 - row4.width / 2
        row4.y = row3.y + row3.height + 10

        row5.x = container.width / 2 - row4.width / 2
        row5.y = row4.y + row4.height

        return container
    }
}

function resetWinContainerStyling() {

    let xWinContainer, yWinContainer = 0;
    if (isMobile()) {
        if (isLandscape()) {
            xWinContainer = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'].mobile.landscape.x
            yWinContainer = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'].mobile.landscape.y
        } else {
            xWinContainer = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'].mobile.portrait.x
            yWinContainer = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'].mobile.portrait.y
        }
    } else {
        xWinContainer = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'].desktop.x
        yWinContainer = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'].desktop.y
    }
    footer.winContainer.x = xWinContainer
    footer.winContainer.y = yWinContainer
    footer.winContainer.width = 170
    footer.winContainer.height = 60;
    footer.winContainer.scale.set(1, 1);
    const height = footer.winContainer.height
    const width = footer.winContainer.width

    const winText = footer.winContainer.getChildByName('win-text')
    winText.style = {
        fontSize: 17,
        fontFamily: 'Arial'
    }

    const winAmount = footer.winContainer.getChildByName('win-amount')
    winAmount.style = {
        fontSize: 24,
        fontFamily: 'Arial',
        fontWeight: 'bold'
    }

    const paylineWin = footer.winContainer.getChildByName('payline-win')
    paylineWin.style = {
        fontSize: 14,
        fontFamily: 'Arial'
    }

    winText.x = width / 2 - winText.width / 2
    winText.y = 4 //settings.FOOTER_CONTAINER_MARGIN

    winAmount.x = width / 2 - winAmount.width / 2
    winAmount.y = winText.height + 2

    paylineWin.x = width / 2 - paylineWin.width / 2
    paylineWin.y = height - paylineWin.height - 2

    footer.winContainer.width = width
    footer.winContainer.height = height;
}