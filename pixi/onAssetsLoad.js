class
onAssetsLoad {
    /*    leftSideIndicatorNumbersImg;
        rightSideIndicatorNumbersImg;*/

    constructor(app, gameContainer) {
        this.firstUpdate = false;
        this.app = app;
        this.gameContainer = gameContainer;
        /*this.mainTextures = [];*/
        this.footer = [];
        this.panel = [];
        this.backgroundElements = [];
        this.winLine;
        // this.titleImg;
        this.highlightedTitleImg;
        this.respinTextTitle;
        this.backgroundImg;
        this.gameBackgroundImg;

        this.tick;
        this.fonts;

        this.loadingBar = new PIXI.Container();
        this.loadingBar.name = 'loading-bar-container';
        this.loadingBar.zIndex = 11;
        this.loadingBar.position.y = 426;

        this.loadingBarSprite = new PIXI.Sprite();
        this.loadingBarSprite.name = 'loading-bar';
        this.loadingBarSprite.zIndex = 11;
        this.loadingBar.addChild(this.loadingBarSprite);


        this.progressAnimation = new PIXI.Sprite();
        this.progressAnimation.visible = true;
        this.progressAnimationMask = new PIXI.Graphics();
        this.progressAnimationMask.beginFill(0xFFCC00, 1);
        this.progressAnimationMask.drawRoundedRect(4, 0, 520, 24, 50);
        this.progressAnimationMask.endFill();
        this.progressAnimationMask.name = 'mask';

        this.loadingBar.addChild(this.progressAnimationMask); // make sure mask it added to display list somewhere!
        this.loadingBar.mask = this.progressAnimationMask;

        /*this.loadingBar.addChild(this.progressAnimation);*/


        this.loadingBarProgressText = new PIXI.Text('', {font: "10px Arial", fill: "white"});
        this.loadingBarProgressText.style.fontSize = 18;

        this.loadingBarProgressText.y = 428;
        this.loadingBarProgressText.zIndex = 12;

        this.loadingBarProgressText.name = 'loadingBarProgressText';
        //this.preloaderImg.addChild(this.loadingBarProgressText);

        if (isMobile() && !isLandscape()) {
            /*   this.loadingBar.position.y = 526;
               this.loadingBarProgressText.y = 552;*/
        }

        let loader = this.app.loader;
        loader.baseUrl = "newAssets";
        loader

            /*NN START*/
            .add('featurePage', 'nn/dist/featurePage.json?ver=' + version)
            .add('panels_nn', 'nn/dist/common/panels.json?ver=' + version)
            .add('prizeElementsNN', 'nn/dist/common/prizeElements.json?ver=' + version)
            .add('respinNN', 'nn/dist/common/respin.json?ver=' + version)
            .add('anticipation', 'nn/dist/common/anticipationOverlay.json?ver=' + version)
            .add('nn_basegame', 'nn/dist/basegame.json?ver=' + version)
            .add('nn_symbols', 'nn/dist/symbols.json?ver=' + version)
            .add('nn_infoPages', 'nn/dist/common/infoPages.json?ver=' + version)

            .add('indicators_1', 'nn/dist/common/paylines/payline01.json?ver=' + version)
            .add('indicators_2', 'nn/dist/common/paylines/payline02.json?ver=' + version)
            .add('indicators_3', 'nn/dist/common/paylines/payline03.json?ver=' + version)
            .add('indicators_4', 'nn/dist/common/paylines/payline04.json?ver=' + version)
            .add('indicators_5', 'nn/dist/common/paylines/payline05.json?ver=' + version)
            .add('indicators_6', 'nn/dist/common/paylines/payline06.json?ver=' + version)
            .add('indicators_7', 'nn/dist/common/paylines/payline07.json?ver=' + version)
            .add('indicators_8', 'nn/dist/common/paylines/payline08.json?ver=' + version)
            .add('indicators_9', 'nn/dist/common/paylines/payline09.json?ver=' + version)
            .add('indicators_10', 'nn/dist/common/paylines/payline10.json?ver=' + version)
            .add('indicators_11', 'nn/dist/common/paylines/payline11.json?ver=' + version)
            .add('indicators_12', 'nn/dist/common/paylines/payline12.json?ver=' + version)
            .add('indicators_13', 'nn/dist/common/paylines/payline13.json?ver=' + version)
            .add('indicators_14', 'nn/dist/common/paylines/payline14.json?ver=' + version)
            .add('indicators_15', 'nn/dist/common/paylines/payline15.json?ver=' + version)
            .add('indicators_16', 'nn/dist/common/paylines/payline16.json?ver=' + version)
            .add('indicators_17', 'nn/dist/common/paylines/payline17.json?ver=' + version)
            .add('indicators_18', 'nn/dist/common/paylines/payline18.json?ver=' + version)
            .add('indicators_19', 'nn/dist/common/paylines/payline19.json?ver=' + version)
            .add('indicators_20', 'nn/dist/common/paylines/payline20.json?ver=' + version)
            .add('indicators_21', 'nn/dist/common/paylines/payline21.json?ver=' + version)
            .add('indicators_22', 'nn/dist/common/paylines/payline22.json?ver=' + version)
            .add('indicators_23', 'nn/dist/common/paylines/payline23.json?ver=' + version)
            .add('indicators_24', 'nn/dist/common/paylines/payline24.json?ver=' + version)
            .add('indicators_25', 'nn/dist/common/paylines/payline25.json?ver=' + version)


            /*MM START*/
            .add('panels', '_movie magic/dist/common/panels.json?ver=' + version)
            .add('basegame', '_movie magic/dist/basegame.json?ver=' + version)
            .add('fonts', '_movie magic/dist/common/fonts.json?ver=' + version)
            .add('freeGamesLayout', '_movie magic/dist/freeGames/freeGamesLayout.json?ver=' + version)
            .add('indicators', '_movie magic/dist/common/payLines.json?ver=' + version)
            // 15.11.2021 Added Prize elements mapping
            .add('prizeElements', '_movie magic/dist/common/prizeElements.json?ver=' + version)
            .add('dialog', '_movie magic/dist/common/dialog Templates.json?ver=' + version)
            .add('infoPages', '_movie magic/dist/common/infopages.json?ver=' + version)
            .add('preloader', '_movie magic/dist/common/preLoader.png?ver=' + version)
            .add('feature_page', '_movie magic/dist/common/featurePage.json?ver=' + version)
            /*MM END*/
            .add('FS_floodlights', '_movie magic/dist/freeGames/transitionToFreeGames/floodlight.json?ver=' + version)
            .add('curtains', '_movie magic/dist/freeGames/transitionToFreeGames/curtains.json?ver=' + version)
            .add('FS_symbols_animated_bigSymbols_anticipation', '_movie magic/dist/freeGames/symbols/anticipation.json?ver=' + version)
            .add('FS_symbols_fsframes', '_movie magic/dist/freeGames/symbols/frameHighlight.json?ver=' + version)
            /*.add('FS_symbols_animated_bigSymbols_anticipation', 'freespins/symbols/animated/big symbols/anticipation.json?ver=' + version)*/
            .add('FS_symbols_animated_bigSymbols_scatter', 'freespins/symbols/animated/big symbols/scatter.json?ver=' + version)
            .add('FS_symbols_animated_bigSymbols_wild', 'freespins/symbols/animated/big symbols/wild.json?ver=' + version)
            .add('FS_symbols_static', 'freespins/symbols/static.json?ver=' + version)
            /*INFO*/
            .add('background_sound', 'sounds/background.mp3?ver=' + version)

            /*TODO: need animation for this*/
            .add('coin_sequence', 'panel/coin_sequence/coin_sequence.json?ver=' + version)
            // .add('coin_fountain_animation', 'panel/coin_animation/coin_animation.json?ver=' + version)
            .add('coin_fountain_animation', 'nn/dist/common/prizeElements.json?ver=' + version)


            .add('ticker_mid', 'sounds/tickerMid.mp3?ver=' + version)
            .add('ticker_end', 'sounds/tickerEnd.mp3?ver=' + version);


        loader.onStart.add(this.startLoading.bind(this));
        /*loader.onProgress.add(this.showProgress.bind(this));
        loader.onComplete.add(this.doneLoading.bind(this));*/
        loader.load(this.onAssetsLoaded.bind(this));
    }

    startLoading() {
        if(isMobile() && isAndroid())
            fullScreenLayer()
        var queryParams = getUrlParameters();
        var validateParameters = validateGameLaunch(queryParams);
        setSettings.PRELOAD_STATUS = true;

        if (!validateParameters) {
        } else if (queryParams['real'] == 'demo' && (typeof queryParams['tournament'] === 'undefined')) {
            startDemoMode();
        } else if (queryParams['real'] == 'true' && (typeof queryParams['tournament'] === 'undefined' || parseInt(queryParams['tournament']) === 0)) {
            startRealMode();
        } else if (typeof queryParams['tournament'] !== 'undefined' && parseInt(queryParams['tournament']) > 0) {
            startTournamentMode();
        } else {
            startDemoMode();
        }

        initPreloadContainer().then((res) => {
            let loader = this.app.loader;

            loader.onProgress.add(this.showProgress.bind(this));
            loader.onComplete.add(this.doneLoading.bind(this));

            loadBrandTexture();
            setPreloadContainerPosition();
        }, (err) => {
            //console.log('preload err', err)
        });
    }

    async showProgress(v) {
        if (!this.firstUpdate) {
            this.firstUpdate = false;
            this.loadingBar.addChild(this.progressAnimation);
        }

        var queryParams = getUrlParameters();

        await sleep(500)
        this.loadingBarProgressText.text = parseInt(v.progress) + '%';
        this.progressAnimation.x = (this.progressAnimation.width * v.progress / 100) - this.progressAnimation.width;
        //this.progressAnimation.y = 0;
        if (isMobile()) {
            this.loadingBarProgressText.x = (preloaderContainer.width / preloaderContainer.scale.x) / 2 - (oAL.loadingBarProgressText.width) / 2;
        } else {
            oAL.loadingBarProgressText.x = app.stage.width / app.stage.scale.x / 2 - (oAL.loadingBarProgressText.width) / 2;
        }

        this.progressAnimation.name = 'progressAnimation';

        if (queryParams.clientType && !isMobile()) {
            preloaderContainer.x = 0 //window.innerWidth / 2 - preloaderContainer.width / 2
        } else if (queryParams.clientType && queryParams.clientType === 'M') {
            //preloaderContainer.x = window.innerWidth / 2 - preloaderContainer.width / 2
        }
    }

    async doneLoading(loader, resources) {

        var queryParams = getUrlParameters();
        var validateParameters = validateGameLaunch(queryParams);
        if (!validateParameters) {
            startGamePermission = false;
            modalDialog.show('Invalid Parameters. Please try again later')
        } else {
            /*startGamePermission = false;*/
            await sleep(1000);
            app.stage.getChildByName('WGS').visible = false;
            //this.showFeaturePage(resources);
            footer.setFooterImages();
            await this.showFeaturePage();
        }
    }

    showFeaturePage() {
        // setSettings.PRELOAD_STATUS = false;
        buildFeatureButton()
        buildFeaturePage()
        this.startGame();
    }
    // async showFeaturePage(resources) {
    //     setSettings.PRELOAD_STATUS = false;
    //     buildFeatureButton()
    //     buildFeaturePage()
    //     await sleep(6000);
    //     this.startGame();
    // }

     startGame() {
        // if (!startGamePermission)
        //     return;

         if (settings.GAME_START)
            return;
        setSettings.GAME_START = true;

        this.buildFooter();

        // if (app.stage.getChildByName('feature-container') !== null)
        //     app.stage.removeChildAt(app.stage.getChildIndex(app.stage.getChildByName('feature-container')));

        if(isMobile() && !isLandscape()) {
            this.highlightedTitleImg.scale.set(0.5, 0.5);
        }
        freeS.reelContainer.visible = false;
        // preloaderContainer.children = [];
        titleContainer.visible = true;//!(isMobile() && !isLandscape());
        footerContainer.visible = true;

        this.gameContainer.visible = true;
        gameCompleted.containerLineIndicators.visible = true;
        oAL.sound.background.loop = true;
        oAL.sound.background.volume = 0.25;
        oAL.sound.background.play();
        setSettings.ACTIVITY = 8;
        game.gameContainer.addChild(game.fakeReelContainer);
        game.gameContainer.addChild(freeS.reelContainer);

        game.gameContainer.getChildByName('reel-container').zIndex = 1;
        game.gameContainer.getChildByName('fake-reel-container').zIndex = 2;

        startInterval();

        switch (settings.GAME_PLAY_MODE) {
            case "real":
                footer.showRealPanel();
                break;
            case "tournament":
                setTimeout(() => {
                    onBalanceDisplayed(parseFloat(settings.USER_BALANCE));
                }, 2000)
                rebuy.showRebuy();
                if (settings.TOURNAMENT_REBUY === "true") {
                    setTimeout(function () {
                        rebuy.showRebuyPopup();
                    }, 500)
                }
                footer.fixTournamentTextPosition();

                break;
            case "demo":
            default:
                footer.showDemoPanel();
                break;
        }
        footer.setPosition(!settings.DEVICE.includes('D'));

        buildMessage();
        this.tick = new Tick();
        freeS.init();
        if (settings.FSV_STATUS) {
            fsv.init();
            disableEnableButtons(false);
            footer.showHideDarkSpinButton(true);
            fsv.showHideAwardedModal();
        }

        if (settings.FS_STATUS) {
            disableEnableButtons(false);
            footer.showHideDarkSpinButton(true);
        }
        setInfoMasterTextures();
        setTimeout(function () {
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 2000);
        setTimeout(() => {
            resetPosition();
        }, 200)
        setCurrency();
        setActionContainerPosition();
        switchTitleTexture();
        prepareLayoutForDevice();
        footerContainer.sortableChildren = true;
        /* var appGraphics = new PIXI.Sprite(this.panel['app_graphic']);
         appGraphics.name = 'app-graphic';
         appGraphics.zIndex = 0;
         appGraphics.width = window.innerWidth
         appGraphics.height = window.innerHeight
         app.stage.addChild(appGraphics)*/
        addGraphic();
        /*TODO:need to check on landscape and desktop and flip*/
        this.backgroundImg.zIndex = 1;
        this.backgroundImg.visible = (isMobile() && !isLandscape());
        textAdjustment(!settings.DEVICE.includes('D'))
        resizeApp();
        if(settings.GAME_PLAY_MODE === 'tournament'){
            taasWidget.onInit();
        }


        infoModal.init();

    }

    async continueBtn() {
        if (app.stage.getChildByName('feature-container') !== null)
            app.stage.removeChildAt(app.stage.getChildIndex(app.stage.getChildByName('feature-container')));

        preloaderContainer.children = [];
    }


    onAssetsLoaded(loader, resources) {
        //return false;
        this.sound = {
            'background': PIXI.sound.Sound.from(resources['background_sound']),
            'ticker_start': PIXI.sound.Sound.from(resources['ticker_start']),
            'ticker_mid': PIXI.sound.Sound.from(resources['ticker_mid']),
            'ticker_end': PIXI.sound.Sound.from(resources['ticker_end']),
        };

        let fonts = resources['fonts'].spritesheet;
        this.fonts = {
            '0': fonts.textures['pngs/0.png'],
            '1': fonts.textures['pngs/1.png'],
            '2': fonts.textures['pngs/2.png'],
            '3': fonts.textures['pngs/3.png'],
            '4': fonts.textures['pngs/4.png'],
            '5': fonts.textures['pngs/5.png'],
            '6': fonts.textures['pngs/6.png'],
            '7': fonts.textures['pngs/7.png'],
            '8': fonts.textures['pngs/8.png'],
            '9': fonts.textures['pngs/9.png'],
            ',': fonts.textures['pngs/comma.png'], //comma
            '.': fonts.textures['pngs/period.png'], // dot
            '$': fonts.textures['pngs/$.png'],
            '€': fonts.textures['pngs/Euro.png'],
        };
        let mSource = !settings.DEVICE.includes('D') ? 'Mobile' : 'Desktop';
        let isMobile = !settings.DEVICE.includes('D');

        let backgroundElements = resources['basegame'].spritesheet;
        let nnBackgroundElements = resources['nn_basegame'].spritesheet;
        let panelsElements = resources['panels'].spritesheet;
        let panelsElements_nn = resources['panels_nn'].spritesheet;
        let prizeElements_nn = resources['prizeElementsNN'].spritesheet;
        let respin_nn = resources['respinNN'].spritesheet;

        this.backgroundElements = {
            // 'footer_panel': isMobile ? panelsElements.textures['mobile/landscape/baseGame/baseGame/panel_blank.png'] : panelsElements.textures['desktop/baseGame/bgDesktopPanel.png'],
            'footer_panel_nn': isMobile ? panelsElements_nn.textures['mobile/landscape/baseGame/baseGame/panel_blank.png'] : panelsElements_nn.textures['desktop/real_demo/desktopBaseGamePanel.png'],
            'background': nnBackgroundElements.textures['desktop/baseGameLayout/baseGameBg.png'],
            'portrait_background': backgroundElements.textures['baseGameLayout/portrait/basegame_bg.png'],
            'portrait_background_nn': nnBackgroundElements.textures['mobile/portrait/basegameLayout/basegameBg.png'],
            'reel_background': backgroundElements.textures['baseGameLayout/portrait/reel_red_bg.png'],
            'free_spin_portrait_background': backgroundElements.textures['baseGameLayout/portrait/fs_bg.png'],
            'side_indicator': backgroundElements.textures['baseGameLayout/side_indicator.png'],
            'side_indicator_bg': backgroundElements.textures['baseGameLayout/side_indicator.png'],
            'side_indicator_numbers_h': backgroundElements.textures['baseGameLayout/side_indicator_numbers_h.png'],
            'left_side_indicator_numbers_n': backgroundElements.textures['baseGameLayout/left_side_indicators_n.png'],
            'right_side_indicator_numbers_n': backgroundElements.textures['baseGameLayout/right_side_indicators_n.png'],
            'title': nnBackgroundElements.textures['desktop/baseGameLayout/baseGameTitle.png'],
            'highlightedTitle': prizeElements_nn.animations['title'],
            'respinTitle': respin_nn.animations['2xRespin'],
            'portrait_title': panelsElements.textures['mobile/portrait/baseGame/header.png'],
            'landscape_title': panelsElements.textures['mobile/landscape/baseGame/header.png'],
        };

        this.panel = {
            'app_graphic': panelsElements.textures['mobile/portrait/baseGame/body.png'],
            'coins': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/coins_n.png';
                return panelsElements.textures[key];
            }()),
            'quick_spin': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/normalSpin_n.png';
                return panelsElements.textures[key];
            }()),
            'quick_spin_hover': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/normalSpin_h.png';
                return panelsElements.textures[key];
            }()),
            'disable_quick_spin': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/quickspin_d.png';
                return panelsElements.textures[key];
            }()),
            'menu_d': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/menu_d') + '/menu_d.png';
                return panelsElements.textures[key];
            }()),
            'menu_close_d': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/Sharedbuttons') : '/baseGame') + '/menu_close_d.png';
                return panelsElements.textures[key];
            }()),
            'sound': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/sound_n.png';
                return panelsElements_nn.textures[key];
            }()),
            'mute_sound_hover': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/soundOff_h.png';
                return panelsElements_nn.textures[key];
            }()),
            'sound_hover': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/sound_h.png';
                return panelsElements_nn.textures[key];
            }()),
            'disable_sound': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/sound_d.png';
                return panelsElements_nn.textures[key];
            }()),
            'mute_sound': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/soundOff_n.png';
                return panelsElements_nn.textures[key];
            }()),
            'disable_mute_sound': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/soundOff_d.png';
                return panelsElements_nn.textures[key];
            }()),
            'info': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/paytable_n.png';
                return panelsElements_nn.textures[key];
            }()),
            'info_hover': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/paytable_h.png';
                return panelsElements_nn.textures[key];
            }()),
            'disable_info': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/paytable_d.png';
                return panelsElements_nn.textures[key];
            }()),

            'coins_plus_n': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/coinsUP_n.png';
                return panelsElements_nn.textures[key];
            }()),
            'coins_plus_h': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/coinsUP_h.png';
                return panelsElements_nn.textures[key];
            }()),
            'coins_plus_d': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/coinsUP_d.png';
                return panelsElements_nn.textures[key];
            }()),
            'coins_plus_p': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/coinsUP_p.png';
                return panelsElements_nn.textures[key];
            }()),
            'coins_minus_n': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/coinsDOWN_n.png';
                return panelsElements_nn.textures[key];
            }()),
            'coins_minus_h': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/coinsDOWN_h.png';
                return panelsElements_nn.textures[key];
            }()),
            'coins_minus_d': (function () {

                let key;
                if (isMobile) {
                    key = mSource.toLowerCase() + '/sharedButtons/coinsDOWN_d.png';
                } else {
                    key = mSource.toLowerCase() + '/baseGame/arrow_down_d.png';
                }

                return panelsElements_nn.textures[key];
            }()),
            'coins_minus_p': (function () {
                let key = mSource.toLowerCase() + (isMobile ? ('/sharedButtons') : '/baseGame') + '/coinsDOWN_p.png';
                return panelsElements_nn.textures[key];
            }()),

            'plus_coin': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_up_n.png'],
            'plus_coin_hover': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_up_h.png'],
            'disable_plus_coin': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_up_d.png'],
            'minus_coin': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_down_n.png'],
            'minus_coin_hover': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_down_h.png'],
            'disable_minus_coin': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/arrow_down_d.png'],
            'footer_panel': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/spin_n.png'],
            'rebuy_pop_up': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/spin_n.png'],
            'win_popup': panelsElements.textures[mSource.toLowerCase() + '/baseGame' + (isMobile ? '/baseGame' : '') + '/spin_n.png'],
        };
        loadFooterContainerTextures(panelsElements_nn);
        loadActionContainerTextures(panelsElements_nn);
        freeS.spritesheet.background = resources['freeGamesLayout'].spritesheet;
        freeS.spritesheet.floodlights = resources['FS_floodlights'].spritesheet;
        /*freeS.spritesheet.transitions = [];
        freeS.spritesheet.transitions.push(resources['curtains-0'].spritesheet, resources['curtains-1'].spritesheet)*/
        freeS.spritesheet.transitions = resources['curtains'].spritesheet;
        freeS.spritesheet.bigSymbols_anticipation = resources['FS_symbols_animated_bigSymbols_anticipation'].spritesheet;
        freeS.spritesheet.bigSymbols_scatter = resources['FS_symbols_animated_bigSymbols_scatter'].spritesheet;
        freeS.spritesheet.bigSymbols_wild = resources['FS_symbols_animated_bigSymbols_wild'].spritesheet;
        freeS.spritesheet.fsframes = resources['FS_symbols_fsframes'].spritesheet;
        freeS.spritesheet.static = resources['FS_symbols_static'].spritesheet;
        freeS.spritesheet.frame_symbol_background = resources['FS_symbols_fsframes'].spritesheet.textures['bigSymbols/lg_frame0001.png'];
        freeS.freeSpinPanel = {
            'spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'spin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_h.png'],
            'spin_disabled': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_d.png'],
            'stop_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames/' + (mSource === 'Mobile/landscape' ? 'stopSpin_n.png' : 'stop_lg_n.png')],
            'stop_spin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/' + (mSource === 'Mobile/landscape' ? 'stopSpin_h.png' : 'stop_lg_h.png')],
            'info': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/paytable_n.png'],
            'info_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/paytable_h.png'],
            'disable_info': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/paytable_d.png'],
            'sound': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/sound_n.png'],
            'sound_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/sound_h.png'],
            'disable_sound': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/sound_d.png'],
            'mute_sound': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/soundOff_n.png'],
            'mute_sound_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/soundOff_h.png'],
            'disable_mute_sound': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/soundOff_d.png'],
            'quick_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_n.png'],
            'quick_spin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_h.png'],
            'normal_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_n.png'],
            'normal_spin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_h.png'],
            'disable_normal_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_d.png'],
            'disable_quick_spin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_d.png'],
            'autoplay': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/autoplay_n.png'],
            'autoplay_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/autoplay_h.png'],
            'cancel_autoplay': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/cancelAutoplay_n.png'],
            'cancel_autoplay_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/cancelAutoplay_h.png'],
            'disable_autoplay': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/autoplay_d.png'],
            'disable_cancel_autoplay': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/cancelAutoplay_d.png'],
            'plus_coin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_up_n.png'],
            'plus_coin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_up_h.png'],
            'disable_plus_coin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_up_d.png'],
            'minus_coin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_down_n.png'],
            'minus_coin_hover': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_down_h.png'],
            'disable_minus_coin': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/arrow_down_d.png'],
            'footer_panel': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'rebuy_pop_up': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'win_popup': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'home_n': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/home_n.png'],
            'home_d': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/home_d.png'],
            'home_h': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/home_h.png'],
            'home_p': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/home_p.png'],
            'mobile_coin_selector_bg': panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/spin_n.png'],
            'footer_panel_bg': !isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/fgDesktopPanel.png'] : panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/panel_blank.png'],
            'mobile_bet_set': isMobile ? panelsElements.textures['mobile/landscape/baseGame/baseGame/display_screen2.png'] : '',
            'mobile_coins_enable_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_n.png'] : '',
            'mobile_coins_enable_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_d.png'] : '',
            'mobile_coins_enable_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_h.png'] : '',
            'mobile_coins_enable_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_p.png'] : '',
            'mobile_coins_close_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_close_n.png'] : '',
            'mobile_coins_close_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_close_d.png'] : '',
            'mobile_coins_close_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_close_h.png'] : '',
            'mobile_coins_close_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coins_close_p.png'] : '',
            'mobile_coins_plus_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coinsUP_n.png'] : '',
            'mobile_coins_plus_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coinsUP_h.png'] : '',
            'mobile_coins_plus_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coinsUP_d.png'] : '',
            'mobile_coins_plus_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/coinsUP_p.png'] : '',
            'mobile_info_menu': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/display_screen1.png'] : '',
            'mobile_quickspin_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_n.png'] : '',
            'mobile_quickspin_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_h.png'] : '',
            'mobile_quickspin_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_d.png'] : '',
            'mobile_quickspin_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/quickspin_p.png'] : '',
            'mobile_normalspin_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_n.png'] : '',
            'mobile_normalspin_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_h.png'] : '',
            'mobile_normalspin_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_d.png'] : '',
            'mobile_normalspin_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/normalSpin_p.png'] : '',
            'mobile_menu_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_n.png'] : '',
            'mobile_menu_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_h.png'] : '',
            'mobile_menu_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_d.png'] : '',
            'mobile_menu_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_p.png'] : '',
            'mobile_menu_close_n': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_close_p.png'] : '',
            'mobile_menu_close_h': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_close_h.png'] : '',
            'mobile_menu_close_d': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_close_d.png'] : '',
            'mobile_menu_close_p': isMobile ? panelsElements.textures[mSource.toLowerCase() + '/freeGames' + '/menu_close_p.png'] : '',
        };
        freeS.panelBackup['original_panel'] = this.panel;

        freeS.dialog = resources['dialog'].spritesheet;
        game.symbols = resources['basegame'].spritesheet;
        game.nnSymbols = resources['nn_symbols'].spritesheet;
        // game.nnFeaturePage = resources['featurePage'].spritesheet;

//special game symbols
        freeS.slotTextures = [
            game.nnSymbols.textures["static/symWild_n.png"],
            game.nnSymbols.textures["static/symHigh1_n.png"],
            game.nnSymbols.textures["static/symHigh2_n.png"],
            game.nnSymbols.textures["static/symHigh3_n.png"],
            game.nnSymbols.textures["static/symHigh4_n.png"],
            game.nnSymbols.textures["static/symA_n.png"],
            game.nnSymbols.textures["static/symK_n.png"],
            game.nnSymbols.textures["static/symQ_n.png"],
            game.nnSymbols.textures["static/symJ_n.png"],
            game.nnSymbols.textures["static/symScatter_n.png"],
            game.nnSymbols.textures["static/symStar_n.png"],
        ];

        freeS.bigSlotTextures = [
            freeS.spritesheet.static.textures["big symbols/big_sym_wild_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_ms1_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_ms2_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_ms3_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_ms4_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_A_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_K_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_Q_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_J_n.png"],
            freeS.spritesheet.static.textures["big symbols/big_sym_scatter_n.png"],
        ];

        freeS.bigSymbolHighlighted = {
            "0": freeS.spritesheet.static.textures["big symbols/big_sym_wild_h.png"],
            "1": freeS.spritesheet.static.textures["big symbols/big_sym_ms1_h.png"],
            "2": freeS.spritesheet.static.textures["big symbols/big_sym_ms2_h.png"],
            "3": freeS.spritesheet.static.textures["big symbols/big_sym_ms3_h.png"],
            "4": freeS.spritesheet.static.textures["big symbols/big_sym_ms4_h.png"],
            "5": freeS.spritesheet.static.textures["big symbols/big_sym_A_h.png"],
            "6": freeS.spritesheet.static.textures["big symbols/big_sym_K_h.png"],
            "7": freeS.spritesheet.static.textures["big symbols/big_sym_Q_h.png"],
            "8": freeS.spritesheet.static.textures["big symbols/big_sym_J_h.png"],
            "9": freeS.spritesheet.static.textures["big symbols/big_sym_scatter_h.png"]
        };

        game.slotTextures = [
            game.nnSymbols.textures["static/symWild_n.png"],
            game.nnSymbols.textures["static/symHigh1_n.png"],
            game.nnSymbols.textures["static/symHigh2_n.png"],
            game.nnSymbols.textures["static/symHigh3_n.png"],
            game.nnSymbols.textures["static/symHigh4_n.png"],
            game.nnSymbols.textures["static/symA_n.png"],
            game.nnSymbols.textures["static/symK_n.png"],
            game.nnSymbols.textures["static/symQ_n.png"],
            game.nnSymbols.textures["static/symJ_n.png"],
            game.nnSymbols.textures["static/sym10_n.png"],
            game.nnSymbols.textures["static/symStar_n.png"],
            game.nnSymbols.textures["static/symScatter_n.png"],
        ];

        game.slotTexturesHighlighted = {

            'symbol0_h': game.nnSymbols.animations["highlightedFrame/symWild_h"],
            'symbol1_h': game.nnSymbols.animations["highlightedFrame/symHigh1_h"],
            'symbol2_h': game.nnSymbols.animations["highlightedFrame/symHigh2_h"],
            'symbol3_h': game.nnSymbols.animations["highlightedFrame/symHigh3_h"],
            'symbol4_h': game.nnSymbols.animations["highlightedFrame/symHigh4_h"],
            'symbol5_h': game.nnSymbols.animations["highlightedFrame/symA_h"],
            'symbol6_h': game.nnSymbols.animations["highlightedFrame/symK_h"],
            'symbol7_h': game.nnSymbols.animations["highlightedFrame/symQ_h"],
            'symbol8_h': game.nnSymbols.animations["highlightedFrame/symJ_h"],
            'symbol9_h': game.nnSymbols.animations["highlightedFrame/sym10_h"],
            /*'symbol9_h': game.symbols.textures["symbols/static/sym_09_scatter_n.png"]*/
            'symbol10_h': game.nnSymbols.animations["highlightedFrame/symStar_h"],

            'symbol11_h': game.nnSymbols.animations["highlightedFrame/symScatter_h"],
        };


        game.symbolAnimation = {
            "0": game.nnSymbols.animations['animated/symWild/wildSeq'],
            "0-h": game.nnSymbols.animations['highlightedFrame/symWild_h'],
            "1": game.nnSymbols.animations["highlightedFrame/symHigh1_h"],
            "2": game.nnSymbols.animations["highlightedFrame/symHigh2_h"],
            "3": game.nnSymbols.animations["highlightedFrame/symHigh3_h"],
            "4": game.nnSymbols.animations["highlightedFrame/symHigh4_h"],
            "5": game.nnSymbols.animations['animated/symA/symAce'],
            "6": game.nnSymbols.animations['animated/symK/symKing'],
            "7": game.nnSymbols.animations['animated/symQ/symQueen'],
            "8": game.nnSymbols.animations['animated/symJ/symJack'],
            "9": game.nnSymbols.animations['animated/sym10/sym10'],
            "10": game.nnSymbols.animations['animated/symStar/starSeq'],
            "10-h": game.nnSymbols.animations['highlightedFrame/symStar_h'],
            "11": game.nnSymbols.animations['animated/symScatter/scatterAnimSeq'],
            "11-h": game.nnSymbols.animations['highlightedFrame/symScatter_h']
        };

        game.animations = {
            "frame_symbol_anim": game.symbols.animations['symbols/frameHighlight'],
            "frame_symbol_flashing": game.symbols.animations['symbols/frameHighlight/sm_flashing'],
            "frame_symbol_background": game.symbols.textures['symbols/frameHighlight/frame_hilight.png'],
            "coin_sequence": resources['coin_sequence'].spritesheet,
            "coin_fountain_animation": resources['coin_fountain_animation'].spritesheet,
        };

        /*infoModal.infoSprites = resources['infoPages'].spritesheet;*/
        loadInfoTextures(resources['nn_infoPages'].spritesheet);

        // this.titleImg = new PIXI.Sprite(this.backgroundElements['title']);
        this.highlightedTitleImg = new PIXI.AnimatedSprite(this.backgroundElements['highlightedTitle']);

        this.respinTextTitle = new PIXI.AnimatedSprite(this.backgroundElements['respinTitle']);
        this.respinTextTitle.zIndex = 10;

        this.respinTextTitle.visible = false;



        this.buildBackground();

        game.paylines = [];
        let totalMargin = settings.REEL_TOTAL_MARGIN;

        let color = [0xfff000, 0xffaa00, 0xff1200, 0xffabc0, 0xff9900];

        let count = 0;
        for (let i = 0; i < settings.REEL_NUMBER; i++) {
            var bg = new PIXI.Sprite(PIXI.Texture.WHITE);

            let rc = new PIXI.Container();
            rc.x = i * SYMBOL_CONTAINER_WIDTH;
            rc.width = SYMBOL_CONTAINER_WIDTH;
            rc.height = 3 * SYMBOL_SIZE;
            rc.name = "reel-" + i;

            game.reelContainer.addChild(rc);

            const reel = {
                container: rc,
                symbols: [],
                position: 0,
                previousPosition: 0,
                blur: new PIXI.filters.BlurFilter(),
            };

            reel.blur.blurX = 0;
            reel.blur.blurY = 0;
            rc.filters = [reel.blur];

            // Build the symbols
            for (let j = 0; j < 12; j++) {
                let randomSymbol = Math.floor(Math.random() * game.slotTextures.length);
                let rand = randomSymbol === 0 ? 1 : randomSymbol;
                if (typeof DEFAULT_PANEL[i][j - 1] !== "undefined" && j > 0) {
                    rand = DEFAULT_PANEL[i][j - 1];
                }
                const symbol = new PIXI.Sprite(game.slotTextures[rand]);
                /*let counter = new PIXI.Text(j);
                symbol.addChild(counter);*/
                symbol.fake = j < 4;
                symbol.symbolElement = rand;
                symbol.y = j * SYMBOL_CONTAINER_SIZE;
                symbol.x = (SYMBOL_CONTAINER_WIDTH / 2) - (SYMBOL_WIDTH / 2);
                count++;
                symbol.symbolPosition = count - 2;
                symbol.column = i;
                symbol.row = j;
                reel.symbols.push(symbol);
                symbol.zOrder = 2;
                rc.addChild(symbol);
            }

            game.reels.push(reel);
            bg.tint = color[i];
            bg.width = SYMBOL_CONTAINER_WIDTH;
            bg.height = 3 * SYMBOL_SIZE;
            bg.alpha = 0.4
            //rc.addChild(bg);
        }

        let freeCount = 0;
        let freeMargin = 0;
        /*BUILD FREESPIN CONTAINER*/
        for (let a = 0; a < 3; a++) {
            let fRc = new PIXI.Container();


            fRc.zIndex = 5;
            if (a > 0)
                freeMargin += totalMargin / 2;

            freeS.reelContainer.addChild(fRc);

            const freeSreel = {
                container: fRc,
                symbols: [],
                position: 0,
                previousPosition: 0,
                blur: new PIXI.filters.BlurFilter(),
            };

            freeSreel.blur.blurX = 0;
            freeSreel.blur.blurY = 0;
            fRc.filters = [freeSreel.blur];
            fRc.name = 'freeSReel'

            if (a === 0) {
                fRc.width = SYMBOL_CONTAINER_WIDTH;
            } else if (a === 1) {
                fRc.width = SYMBOL_CONTAINER_WIDTH * 3
                fRc.x = SYMBOL_CONTAINER_WIDTH;
            } else if (a === 2) {
                fRc.width = SYMBOL_CONTAINER_WIDTH
                fRc.x = SYMBOL_CONTAINER_WIDTH * settings.REEL_NUMBER - SYMBOL_CONTAINER_WIDTH
            }


            // Build the symbols
            let symbolLength = 12//4;

            for (let b = 0; b < symbolLength; b++) {
                let randomSymbol = Math.floor(Math.random() * freeS.slotTextures.length);
                let rand = randomSymbol === 0 ? 1 : randomSymbol;

                const freeSsymbol = new PIXI.Sprite(a !== 1 ? freeS.slotTextures[rand] : freeS.bigSlotTextures[rand]);
                freeSsymbol.symbolElement = rand;
                // Scale the symbol to fit symbol area.

                freeSsymbol.width = a !== 1 ? SYMBOL_CONTAINER_WIDTH : freeSsymbol.width;
                freeSsymbol.height = a !== 1 ? SYMBOL_SIZE : freeSsymbol.height;
                freeSsymbol.y = a !== 1 ? b * SYMBOL_SIZE + (b * SYMBOL_MARGIN_TOP) : b * freeSsymbol.width + (b * SYMBOL_MARGIN_TOP);

                freeCount++;
                freeSsymbol.symbolPosition = freeCount - 2;
                freeSsymbol.pickPosition = a + (b - 1) * 5;
                freeSsymbol.column = a;
                freeSsymbol.row = a;
                freeSreel.symbols.push(freeSsymbol);
                freeSsymbol.zOrder = 2;
                fRc.addChild(freeSsymbol);
            }

            freeS.reels.push(freeSreel);
        }
        /*END FREESPIN CONTAINER*/

        for (let i = 0; i <= 2; i++) {
            let gameAnticipation = [];
            if (i === 1) { //BIGSYMBOL
                gameAnticipation = new PIXI.AnimatedSprite(freeS.spritesheet.bigSymbols_anticipation.animations['bigSymbols/fgBigSymAnticiaption']);
            } else {
                // gameAnticipation = new PIXI.AnimatedSprite(resources['basegame'].spritesheet.animations['symbols/anticipation/bgSymAnticiaption']);
                gameAnticipation = new PIXI.AnimatedSprite(resources['anticipation'].spritesheet.animations['anticipationSeq']);
            }
            gameAnticipation.name = 'anticipation' + i;
            gameAnticipation.zIndex = 1;
            gameAnticipation.loop = true;
            gameAnticipation.visible = false;
            if (i === 0)
                gameAnticipation.x = 80//game.reelContainer.x;
            else if (i === 1)
                gameAnticipation.x = GAME_WIDTH / 2 - gameAnticipation.width / 2;
            else
                gameAnticipation.x = 771//game.reelContainer.children[game.reelContainer.children.length - 1].x + game.reelContainer.x;

            gameAnticipation.y = 150;
            gameAnticipation.animationSpeed = 0.5;
            game.anticipation.push(gameAnticipation);
            gameContainer.addChild(gameAnticipation);
        }

        const mask = new PIXI.Sprite(PIXI.Texture.WHITE);
        const freeSpinMask = new PIXI.Sprite(PIXI.Texture.WHITE);
        /*mask.tint = 0xff0000;*/
        freeSpinMask.height = mask.height = SYMBOL_SIZE * 3 + (3 * SYMBOL_MARGIN_TOP);
        freeSpinMask.width = mask.width = settings.REEL_NUMBER * SYMBOL_CONTAINER_WIDTH;

        game.fakeReelContainer.y = freeS.reelContainer.y = game.reelContainer.y = isMobile && !isLandscape() ? 0 : REEL_CONTAINER_MARGIN_TOP;
        game.fakeReelContainer.x = freeS.reelContainer.x = game.reelContainer.x = (function () {
            let position = (GAME_WIDTH / 2) - ((SYMBOL_CONTAINER_WIDTH * settings.REEL_NUMBER) / 2)
            if (isMobile && !isLandscape()) {
                position = 0//gameContainer.width / 2 - (game.reelContainer.width * gameContainer.scale.x) / 2//(GAME_WIDTH / 2) -  (game.reelContainer.width * gameContainer.scale.x) / 2;
            }
            return position;
        }());
        /*game.fakeReelContainer.height = freeS.reelContainer.height = game.reelContainer.height = SYMBOL_SIZE * 3;
        game.fakeReelContainer.width = freeS.reelContainer.width = game.reelContainer.width = SYMBOL_CONTAINER_WIDTH * 5;*/

        game.reelContainer.addChild(mask);
        game.reelContainer.mask = mask;
        freeS.reelContainer.addChild(freeSpinMask);
        freeS.reelContainer.mask = freeSpinMask;
        game.reelContainer.sortableChildren = true;
        /*let reelBG = new PIXI.Sprite(PIXI.Texture.WHITE);
        reelBG.name = 'reel-bg';
        reelBG.width = game.reelContainer.width;
        reelBG.height = game.reelContainer.height;
        reelBG.tint = 0x610700;
        reelBG.zIndex = -19
        game.reelContainer.addChild(reelBG);*/


        game.gameContainer.addChild(game.reelContainer, game.reelBackground, game.reelOverlay, freeS.reelContainer);
        game.reelOverlay.x = game.reelContainer.x /*+ 5*/;
        game.reelOverlay.y = 100//game.reelContainer.y /*- 5*/;
        game.reelOverlay.width = game.reelContainer.width + 20;
        game.reelOverlay.height = game.reelContainer.height - SYMBOL_SIZE + 60;
        game.reelOverlay.alpha = 0;
        game.reelOverlay.visible = false;
        game.reelOverlay.zIndex = 2;

        setTimeout(function () {
            gameCompleted.loadLinesIndicators(resources);
            //gameCompleted.loadLines(resources);
        }, 2000)
        this.buildWinLine();
    }

    buildWinLine() {
        this.winLine = new PIXI.Graphics();
        this.winLine.name = 'win-line';
        this.winLine.visible = false;

        this.winLine.beginFill(0x00d0ff);
        this.winLine.lineStyle(1, 0x1000ff);
        this.winLine.drawRect(0, 0, game.reelContainer.width, 6);
        this.winLine.x = game.reelContainer.x;
        this.winLine.y = game.reelContainer.y + ((SYMBOL_SIZE * 3 + (3 * SYMBOL_MARGIN_TOP)) / 2 - 2);
        this.winLine.zIndex = 4;
        app.stage.addChild(this.winLine);
    }

    buildBackground() {
        this.backgroundImg = new PIXI.Sprite(this.backgroundElements['portrait_background_nn']); /*TODO: need background  for desktop and landscape*/
        this.backgroundImg.name = 'background-image';
        this.backgroundImg.width = window.innerWidth;
        this.backgroundImg.height = window.innerHeight;
        this.backgroundImg.visible = false;

        app.stage.addChild(this.backgroundImg);

        // if((isMobile() && isLandscape()) || !isMobile() ) {
            this.gameBackgroundImg = new PIXI.Sprite(this.backgroundElements['background']);
            this.gameBackgroundImg.width = GAME_WIDTH;
            this.gameBackgroundImg.name = 'game-background-image'
            this.gameBackgroundImg.height = GAME_HEIGHT;
            this.gameContainer.addChild(this.gameBackgroundImg);
        // }

        // this.titleImg.name = 'image';
        this.highlightedTitleImg.name = 'imageHighlighted';
        this.respinTextTitle.name = 'respin-img';

        titleContainer.addChild(this.highlightedTitleImg);
        gameContainer.addChild(this.respinTextTitle);

        if (isMobile())
            game.reelBackground.texture = this.backgroundElements['reel_background']
        game.reelBackground.x = game.reelContainer.x
    }

    buildFooter() {
        let isMobile = !settings.DEVICE.includes('D')
        footer.show(isMobile)
    }
}



