class Info {
    constructor(app) {
        this.infoFullSprites = [];
        this.infoSprites = [];
        this.mainContainer = new PIXI.Container();
        this.mainContainer.name = 'info-container';
        this.bg;

        this.pageIndex = 1;

        app.stage.addChild(this.mainContainer);
    }

    async init() {
        /* this.mainContainer.y = -app.stage.height;*/
        this.mainContainer.alpha = 0;
        this.mainContainer.zIndex = 9999;
        this.mainContainer.sortChildren = true;
        this.mainContainer.name = 'main-info'

        this.bg = new PIXI.Sprite(PIXI.Texture.WHITE);

        this.bg.tint = 0x000000
        this.bg.alpha = 0.7
        this.bg.x = 0;
        this.bg.y = 0;
        this.bg.name = 'bg-info'


        this.closeBtn = new PIXI.Sprite();
        /*this.closeBtn = new PIXI.Sprite(this.infoSprites.textures["buttons and page indicators/close_n.png"]);*/
        this.closeBtn.name = 'close-info-btn';
        this.closeBtn.interactive = true;
        this.closeBtn.buttonMode = true;
        this.closeBtn.on('pointerdown', this.hideInfo.bind(this));
        this.panel = new PIXI.Sprite(/*this.infoSprites.textures['ipPage1.png']*/);
        /*if (isMobile() && !isLandscape())
            this.panel = new PIXI.Sprite(this.infoSprites.textures['mobile portrait/page1_.png']);
        else
            this.panel = new PIXI.Sprite(this.infoSprites.textures['ipPage1.png']);*/

        this.controlContainer = new PIXI.Container();
        this.rightArrow = new PIXI.Sprite(/*this.infoSprites.textures['buttons and page indicators/R_arrow_n.png']*/);
        this.leftArrow = new PIXI.Sprite(/*this.infoSprites.textures['buttons and page indicators/L_arrow_d.png']*/);
        this.bulletPage = new PIXI.Sprite(/*this.infoSprites.textures['buttons and page indicators/page1_h.png']*/);
        setInfoTextures();


        this.controlContainer.addChild(this.rightArrow, this.bulletPage, this.leftArrow);

        this.closeBtn.zIndex = 9;
        //bg.zIndex = 2
        this.panel.zIndex = 3;
        this.controlContainer.zIndex = 4;
        this.mainContainer.addChild(this.bg, this.panel, this.controlContainer, this.closeBtn);

        this.closeBtn.width = 24;
        this.closeBtn.height = 24;
        /*this.closeBtn.x = GAME_WIDTH - this.closeBtn.width - 20;
        this.closeBtn.y = 20;
        this.panel.x = (GAME_WIDTH / 2) - (this.panel.width / 2);
        this.panel.y = (GAME_HEIGHT / 2) - (this.panel.height / 2);*/

        /*this.controlContainer.x = (GAME_WIDTH / 2) - (this.controlContainer.width / 2);
        this.controlContainer.y = this.panel.height - this.controlContainer.height;*/

        /*this.bulletPage.x = (this.controlContainer.width / 2) - (this.bulletPage.width / 2);
        this.rightArrow.x = (this.controlContainer.width / 2) + (this.bulletPage.width / 2);
        this.leftArrow.x = (this.controlContainer.width / 2) - (this.bulletPage.width / 2) - (this.leftArrow.width) - 11;*/
        setElementPosition();
        this.controlPanel();
    }

    controlPanel() {
        this.rightArrow.interactive = true;
        this.leftArrow.interactive = true;
        let self = this;

        if (self.pageIndex === 1)
            self.leftArrow.texture = self.infoSprites['left_arrow_disabled'];

        /*RIGHT*/
        this.rightArrow.on('pointerover', function () {
            if (self.pageIndex < 4)
                self.rightArrow.texture = self.infoSprites['right_arrow_hover']
        });

        this.rightArrow.on('pointerdown', function () {
            if (self.pageIndex >= 4)
                return;
            button.play();
            self.pageIndex += 1;
            self.bulletPage.texture = self.infoSprites['page' + self.pageIndex + '_h']

            self.panel.texture = self.infoSprites['ipPage' + self.pageIndex];

            if (self.pageIndex > 1)
                self.leftArrow.texture = self.infoSprites['left_arrow_normal'];

            if (self.pageIndex > 3)
                self.rightArrow.texture = self.infoSprites['right_arrow_disabled'];

        });

        this.rightArrow.on('pointerout', function () {
            if (self.pageIndex < 4)
                self.rightArrow.texture = self.infoSprites['right_arrow_normal']
            else
                self.rightArrow.texture = self.infoSprites['right_arrow_disabled']
        });

        /*LEFT*/
        this.leftArrow.on('pointerover', function () {
            //console.log(self.pageIndex, self.pageIndex > 1);
            if (self.pageIndex > 1)
                self.leftArrow.texture = self.infoSprites['left_arrow_hover']
        });

        this.leftArrow.on('pointerdown', function () {
            if (self.pageIndex <= 1)
                return;
            button.play()
            self.pageIndex -= 1;
            self.bulletPage.texture = self.infoSprites['page' + self.pageIndex + '_h']
            self.panel.texture = self.infoSprites['ipPage' + self.pageIndex];
            if (self.pageIndex < 4)
                self.rightArrow.texture = self.infoSprites['right_arrow_normal'];

            if (self.pageIndex < 2)
                self.leftArrow.texture = self.infoSprites['left_arrow_normal'];

        });

        this.leftArrow.on('pointerout', function () {
            if (self.pageIndex > 1)
                self.leftArrow.texture = self.infoSprites['left_arrow_normal']
            else
                self.leftArrow.texture = self.infoSprites['left_arrow_disabled']


        });
    }


    showInfo() {
        TweenMax.to(this.mainContainer, 0.4, {y: 0, alpha: 1, ease: Power1.easeOut});
        footer.infoBtn.interactive = false;
        footer.spinButtonSprite.interactive = false;
        footer.soundButton.interactive = false;
        footer.plusButton.interactive = false;
        footer.minusButton.interactive = false;
        footer.fastSpinButton.interactive = false;
        footer.autoSpinButton.interactive = false;
        setElementPosition()
      //  this.pageIndex = 1;
    }

    hideInfo() {
        TweenMax.to(this.mainContainer, 0.4, {y: -app.stage.height, alpha: 0, ease: Power1.easeOut});
        if (typeof footer.infoBtn !== "undefined")
            footer.infoBtn.interactive = true;
        footer.spinButtonSprite.interactive = true;
        if (typeof footer.soundButton !== "undefined")
            footer.soundButton.interactive = true;
        if (typeof footer.plusButton !== "undefined")
            footer.plusButton.interactive = true;
        if (typeof footer.minusButton !== "undefined")
            footer.minusButton.interactive = true;
        if (typeof footer.fastSpinButton !== "undefined")
            footer.fastSpinButton.interactive = true;
        if (typeof footer.autoSpinButton !== "undefined")
            footer.autoSpinButton.interactive = true;
    }
}
