class Test {
    constructor() {
        this.lines = [];
        this.stop = false;
        this.activePaylineIndicators = [];
        this.winnLineIndex = 0;
    }

    destroy() {
        this.stop = true;
    }

    async showWinningSymbolsNew() {

        if (this.stop) return;
        if (settings.GAME_RUNNING) return;
        setSettings.WINNING_ROTATION_STATUS = true;
        gameCompleted.addRemoveBlurAllSymbols(true);

        let gamePayline = game.paylines[this.winnLineIndex];
        let paylineText = '';
        /*let containerBackgroundSprite = new PIXI.Sprite(PIXI.Texture.from(app.loader.resources.prizeElements.data.animations['prizeBackground/prizeElement'][4]))*/
        let containerBackgroundSprite = new PIXI.Sprite(app.loader.resources.prizeElementsNN.spritesheet.textures['prizeOverlay/prizeElementBg.png'])

        if (typeof gamePayline === "undefined") return;

        if (typeof gamePayline !== "undefined")
            paylineText = "LINE " + gamePayline.payline + " WINS " + user.currencySymbol + formatMoney(gamePayline.amount.toFixed(2));

        if (typeof gamePayline !== "undefined" && gamePayline.payline === -1)
            paylineText = "SCATTER PAYS " + user.currencySymbol + game.paylines[this.winnLineIndex].amount.toFixed(2);

        if (game.paylines[this.winnLineIndex].amount.toFixed(2) > 0) {
            gameCompleted.winText.children = [];

            let winImageContainer = new PIXI.Container();
            // containerBackgroundSprite.width = winImageContainer.width
            // containerBackgroundSprite.height = winImageContainer.height
            winImageContainer.addChild(containerBackgroundSprite)
            let currency = new PIXI.Sprite(oAL.fonts[user.currencySymbol]);
            currency.name = 'font';
            currency.scale.set(0.45, 0.45)
            currency.x = 20;
            currency.y = containerBackgroundSprite.height / 2 - currency.height / 1.7
            winImageContainer.addChild(currency);
            winImageContainer.zIndex = 9;
            winImageContainer.name = 'win-image-container';
            let symbolWidth = currency.width;
            let lineWinAmount = formatMoney(game.paylines[this.winnLineIndex].amount.toFixed(2))
            for (var i = 0; i < lineWinAmount.length; i++) {
                let fontElement = winImageContainer.children;

                let charImage = new PIXI.Sprite(oAL.fonts[lineWinAmount.charAt(i)]);
                charImage.name = 'font';

                charImage.scale.set(0.35, 0.35)
                charImage.x = fontElement[fontElement.length - 1].x + fontElement[fontElement.length - 1].width;
                charImage.y = 10//containerBackgroundSprite.height / 2 - charImage.height / 2
                symbolWidth += charImage.width

                winImageContainer.addChild(charImage)
            }

            gameCompleted.winText.scale.set(0.6, 0.6)

            gameCompleted.winText.addChild(winImageContainer);
            containerBackgroundSprite.width =  symbolWidth + currency.x * 2//winImageContainer.width + currency.x
            containerBackgroundSprite.height = winImageContainer.height

            //gameCompleted.winText.text = user.currencySymbol + game.paylines[this.winnLineIndex].amount.toFixed(2);

            if (!isLandscape())
                gameCompleted.winText.x = (GAME_WIDTH / 2) + (gameCompleted.winText.width / 2); // (gameContainer.width / gameContainer.scale.x / 2) - (gameCompleted.winText.width / 2);
            else
                gameCompleted.winText.x = (GAME_WIDTH / 2) - (gameCompleted.winText.width / 2);

            gameCompleted.winText.y = game.reelContainer.y + ((SYMBOL_SIZE * 3 + (3 * SYMBOL_MARGIN_TOP)) / 2 - (gameCompleted.winText.height / 2));
        }
        setAndCenterPaylineText(paylineText)

        let symbolValue;
        let symbolElement;

        Test.hideAllChildFromFakeContainer();
        /*Add animations*/

        if (gameCompleted.firstLoop && game.paylines[this.winnLineIndex].payline !== -1) {
            lineWin.play();
        }


        if (/*gameCompleted.firstLoop && */!settings.STOP_PAYLINE_ANIMATION) {
            for (let j = 0; j < game.paylines[this.winnLineIndex].win_symbols.length; j++) {
                let paylineContainer = game.fakeReelContainer.getChildByName(game.paylines[this.winnLineIndex].payline);

                if (spinH.gameType.name === 'free-spin' && [1, 2, 3].includes(game.paylines[this.winnLineIndex].win_symbols[j].col)) {

                    let symbol = paylineContainer.getChildByName(j + '-' + game.paylines[this.winnLineIndex].win_symbols[j].value);
                    if (symbol === null)
                        continue;

                    freeS.reelContainer.children[1].children[1].visible = false;

                    /*if (symbol.getChildByName('big-frame-animation') !== null)
                        symbol.getChildByName('big-frame-animation').visible = false
                    if (symbol.getChildByName('big-symbol') !== null)
                        symbol.getChildByName('big-symbol').visible = false*/

                    if (paylineContainer.getChildByName('big') !== null)
                        paylineContainer.getChildByName('big').visible = false
                    /*TweenMax.to(symbol, 0.2, {*/

                    /*});*/

                    // if(game.paylines.length !== 0) {
                    //     game.addStars();
                    // }
                } else {
                    // if(game.paylines.length !== 0) {
                    //     game.addStars();
                    // }

                    /*symbolValue = game.paylines[this.winnLineIndex].win_symbols[j].value;

                    symbolElement = game.symbolAnimation[symbolValue];

                    let symbol = paylineContainer.getChildByName(j + '-' + symbolValue);
                    console.log(game.paylines[this.winnLineIndex].payline, j + '-' + symbolValue, symbol.bigSymbolContainer, 'symbol123');

                    let symbolFrame = symbol.getChildByName('frame-animation');
                    if (symbolFrame !== null) {
                        symbolFrame.visible = true;
                        symbolFrame.zIndex = 2;
                    }
                    let symbolAnim;
                    if ([0, 9].includes(symbolValue)) {
                        symbolAnim = symbol.getChildByName('symbolAnim-' + symbolValue + '-h');
                        if (symbolAnim !== null) {
                            symbolAnim.gotoAndPlay(0);
                            symbolAnim.visible = true;
                            symbolAnim.loop = false;
                        }
                    } else {
                        symbolAnim = symbol.getChildByName('symbolAnim-' + symbolValue);
                        if (symbolAnim !== null) {
                            symbolAnim.gotoAndPlay(0);
                            symbolAnim.visible = true;
                            symbolAnim.loop = false;
                        }
                    }*/
                }

                symbolValue = game.paylines[this.winnLineIndex].win_symbols[j].value;

                symbolElement = game.symbolAnimation[symbolValue];

                    let symbol = paylineContainer.getChildByName(j + '-' + symbolValue);

                if (j === 2 && symbol)
                    gameCompleted.winText.y = (game.reelContainer.y + symbol.y) + (SYMBOL_SIZE / 2 - gameCompleted.winText.height / 2)
                if(symbol && symbol.visible === false) {
                    symbol.visible = true;
                }

                let symbolFrame = symbol ? symbol.getChildByName('frame-animation') : null;
                if (symbolFrame !== null) {
                    symbolFrame.visible = true;
                    symbolFrame.zIndex = 2;
                }

                let symbolFrameBackground = symbol ? symbol.getChildByName("symbol-frame-background") : null;
                if (symbolFrameBackground !== null) {
                    symbolFrameBackground.visible = true;
                }

                let symbolAnim;
                if ([0, 10].includes(symbolValue)) {
                    symbolAnim = symbol ? symbol.getChildByName('symbolAnim-' + symbolValue + '-h') : null;
                    if (symbolAnim !== null) {
                        symbolAnim.gotoAndPlay(0);
                        symbolAnim.visible = true;
                        symbolAnim.loop = false;
                    }
                } else {
                    symbolAnim = symbol ? symbol.getChildByName('symbolAnim-' + symbolValue) : null;
                    if (symbolAnim !== null) {
                        symbolAnim.gotoAndPlay(0);
                        symbolAnim.visible = true;
                        symbolAnim.loop = false;
                    }
                }
            }
        }
        if (settings.STOP_PAYLINE_ANIMATION)
            return;

        game.fakeReelContainer.getChildByName(game.paylines[this.winnLineIndex].payline).visible = true;

        if (typeof gameCompleted.linesIndicators[game.paylines[this.winnLineIndex].payline] !== "undefined" && !settings.STOP_PAYLINE_ANIMATION)
            gameCompleted.linesIndicators[game.paylines[this.winnLineIndex].payline].visible = true;

        if (game.paylines[this.winnLineIndex].payline !== 0 && !settings.STOP_PAYLINE_ANIMATION) {

            if (this.stop) return;

            this.activePaylineIndicators.push(game.paylines[this.winnLineIndex].payline);

            await sleep(1000);
            gameCompleted.hideActivePayline();
            gameCompleted.hideAllLinesIndicators();

            /*if (game.paylines.length > 0 && typeof game.paylines[this.winnLineIndex] !== "undefined" && game.paylines[this.winnLineIndex].payline > 0) {
                gameCompleted.lines[game.paylines[this.winnLineIndex].payline - 1].visible = false;
            }*/
            if (game.paylines.length === this.winnLineIndex + 1 && !settings.STOP_PAYLINE_ANIMATION) {
                  setSettings.WINNING_ROTATION_STATUS = false;
                if (((spinH.autospin && !settings.FS_STATUS) || (settings.FS_STATUS && freeS.curtainsStatus ) || (settings.FSV_STATUS) && !(settings.FS_STATUS && freeS.curtainsStatus)) && !game.respin) {
                    //spinH.winningRound = false;
                    spinH.removeAllAnimatedSprite();
                    gameCompleted.addRemoveBlurAllSymbols(false);
                    console.log('active spin 3')
                    spinH.activeSpinButton();
                    return;
                } else {
                    console.log('spin2');

                    if(game.paylines.length > 1 && game.respin && game.nrFreeSpin === 1) {
                        // actionContainer.getChildByName("spin-btn").visible = false;
                        // actionContainer.getChildByName("dark-spin").visible = true;
                        settings.STOP_PAYLINE_ANIMATION = true;

                        game.startSpin = false;
                        game.addStars(game.nrStarPos);
                        // game.linesIndicators.visible = false;

                        let val = game.fakeReelContainer.children.length;
                        for (let i = 0; i < val; i++) {
                            game.fakeReelContainer.getChildAt(i).removeChildren();
                        }
                    } else {
                        gameCompleted.firstLoop = false;
                        this.winnLineIndex = 0;
                    }

                }
            } else {

                this.winnLineIndex++;
            }

            if (!settings.GAME_RUNNING && !settings.STOP_PAYLINE_ANIMATION) {

                spinH.removeAllAnimatedSprite();
                gameCompleted.addRemoveBlurAllSymbols(false);
                if (this.stop) return;

                this.showWinningSymbolsNew();

            }
        } else {

            game.paylines[this.winnLineIndex] = [];
        }
    }

    static hideAllChildFromFakeContainer() {
        for (let i = 0; i < game.fakeReelContainer.children.length; i++) {
            // game.fakeReelContainer.children[i].visible = false;
        }
    }

}
