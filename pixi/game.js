class Game {
    constructor(app, gameContainer) {
        this.name = 'game';
        this.app = app;
        this.coinSequienceAnimation;
        this.tweenCoinAnim;
        this.gameContainer = gameContainer;
        this.slotPosition = [];
        this.animations = [];
        this.paylines = [];
        this.anticipation = [];
        this.lines = [];
        this.tweening = [];
        this.reels = [];
        this.slotTextures = [];
        this.win = 0;
        this.reelContainer = new PIXI.Container();
        this.reelBackground = new PIXI.Sprite();
        this.reelBackground.name = 'reel-background';

        this.respin = false;
        this.nrStarPos = '';
        this.startSpin = true;
        this.nrFreeSpin = 0;

        this.reelOverlay = new PIXI.Sprite(PIXI.Texture.WHITE);
        this.reelOverlay.name = 'reel-overlay';
        this.reelOverlay.tint = 0x000000;
        this.symbols = [];
        this.reelContainer.name = 'reel-container';
        this.fakeReelContainer = new PIXI.Container();
        this.fakeReelContainer.name = 'fake-reel-container';

        this.scatterCount = 0;
        this.nrScatter = 0;
        this.reel1ScatterActive = false;
        this.reel2ScatterActive = false;
        this.reel3ScatterActive = false;
        this.randomElement = {0: true, 1: true, 2: true, 3: true, 4: true};
        this.app.ticker.add((delta) => {
            let now = new Date();

            const remove = [];
            for (let i = 0; i < this.tweening.length; i++) {
                const t = this.tweening[i];


                const phase = Math.min(1, (now - t.start) / t.time);

                t.propertyBeginValue = 0; //reset value
                t.target = 180; //reset value

                t.object[t.property] = spinH.lerp(t.propertyBeginValue, t.target, t.easing(phase));


                if (t.change)
                    t.change(t);

                /*if (this.reel1ScatterActive && this.reel2ScatterActive && parseInt(t.object.reelPosition) === 4 && !settings.RESET_SPIN_VALUE) {
                    //now = new Date(new Date - 6000);
                    t.start = Date.now(Date.now() - 2000);
                    t.target = 60; //reset value
                    setSettings.RESET_SPIN_VALUE = true;
                }*/

                if (phase > 0.6)
                    this.randomElement[parseInt(t.object.reelPosition)] = false;


                if (phase === 1) {

                    if (t.object.scatterSymbol) {
                        console.log('scatter here ...................22222222',)

                        this.scatterCount += t.object.scatterSymbolCount;
                        /*TODO: play scatter sound, sound map from 908-2*/

                        console.log('scatter here ...', this.scatterCount)
                        game.checkAndRunSymbolHighlight(t.object.container);
                        if (parseInt(t.object.reelPosition) === 0) { //reel 0 is reel 1
                            footer.winAmount.text = '';
                            this.reel1ScatterActive = true;
                            game.checkAndRunSymbolHighlight(t.object.container)
                                scatterDrop1.play();
                        } else if (parseInt(t.object.reelPosition) === 1) {  //reel 2 is reel 3
                            if (this.reel1ScatterActive) {
                                game.checkAndRunSymbolHighlight(t.object.container)
                                scatterDrop2.play();
                                // setSettings.ANTICIPATION = true;
                                this.reel2ScatterActive = true;
                            } else {
                                game.checkAndRunSymbolHighlight(t.object.container)
                                this.reel1ScatterActive = true;
                                scatterDrop1.play();
                            }
                            // let anticipationSequence = game.anticipation[game.anticipation.length - 1];
                            // anticipationSequence.visible = true;
                            // anticipationSequence.gotoAndPlay(0)
                        } else if (parseInt(t.object.reelPosition) === 3) {
                            if (this.reel1ScatterActive && this.reel2ScatterActive) {
                                game.checkAndRunSymbolHighlight(t.object.container)
                                scatterDrop3.play();
                                this.reel3ScatterActive = true;
                                // setSettings.ANTICIPATION = false;
                            } else if (this.reel1ScatterActive || this.reel2ScatterActive) {
                                game.checkAndRunSymbolHighlight(t.object.container)
                                scatterDrop2.play();
                            } else {
                                game.checkAndRunSymbolHighlight(t.object.container)
                                scatterDrop1.play();
                            }
                        } else if (parseInt(t.object.reelPosition) === 4) {
                            if (this.reel1ScatterActive && this.reel2ScatterActive && this.reel3ScatterActive) {
                                game.checkAndRunSymbolHighlight(t.object.container)
                                scatterDrop4.play();
                            } else if (this.reel1ScatterActive && this.reel2ScatterActive) {
                                scatterDrop3.play();
                            } else if (this.reel1ScatterActive && this.reel3ScatterActive) {
                                scatterDrop3.play();
                            } else if (this.reel2ScatterActive && this.reel3ScatterActive) {
                                scatterDrop3.play();
                            } else if (this.reel1ScatterActive || this.reel2ScatterActive) {
                                scatterDrop2.play();
                            } else if (this.reel1ScatterActive || this.reel3ScatterActive) {
                                scatterDrop2.play();
                            } else if (this.reel2ScatterActive || this.reel3ScatterActive) {
                                scatterDrop2.play();
                            }
                        } else if ([2, 4].includes(parseInt(t.object.reelPosition))/* && this.reel1ScatterActive && this.reel2ScatterActive*/) {
                            // game.removeSymbolHighlight(t.object.container)
                        } else {
                            if (parseInt(t.object.reelPosition) === 1) {
                                footer.winAmount.text = '';
                            }
                        }
                    }

                    /*FS STATUS ACTIVE*/
                    /* if (settings.FS_STATUS && parseInt(t.object.reelPosition) === 0 && !settings.RESET_SPIN_VALUE) {
                         this.tweening[this.tweening.length - 1].time += 4000;
                         setSettings.RESET_SPIN_VALUE = true;
                     }*/

                    t.object.scatterSymbol = false; /*TODO: nee to uncomment this*/
                    reelStop.play();

                    t.object[t.property] = t.target;
                    if (t.complete) t.complete(t);
                    remove.push(t);
                } else if (settings.STOP_REEL) {
                    for (let l = 0; l < Object.keys(game.randomElement).length; l++) {
                        this.randomElement[l] = false;
                    }

                    setSettings.ANTICIPATION = false;
                    reelStop.play();

                    footer.winAmount.text = '';
                    t.object.scatterSymbol = false;
                    t.object[t.property] = t.target;
                    if (t.complete) t.complete(t);
                    remove.push(t);
                }
            }

            for (let i = 0; i < remove.length; i++)
                this.tweening.splice(this.tweening.indexOf(remove[i]), 1);

        });
        this.app.ticker.add((delta) => {
            // Update the slots.

            for (let i = 0; i < this.reels.length; i++) {

                const r = this.reels[i];
                // Update blur filter y amount based on speed.
                // This would be better if calculated with time in mind also. Now blur depends on frame rate.
                r.blur.blurY = (r.position - r.previousPosition) * 40;
                r.previousPosition = r.position;

                // Update symbol positions on reel.
                for (let j = 0; j < r.symbols.length; j++) {

                    const s = r.symbols[j];
                    const prevy = s.y;

                    s.y = ((r.position + j) % r.symbols.length) * SYMBOL_CONTAINER_SIZE - SYMBOL_CONTAINER_SIZE;

                    if ((s.y < 0 && prevy > SYMBOL_SIZE) || Object.keys(spinH.slotPosition).length > 0) {
                        let symbolSource = /*settings.FS_ENABLE_STATUS ? freeS.slotTextures : */this.slotTextures;
                        if (typeof spinH.slotPosition[i] !== "undefined") {

                            if (parseInt(spinH.slotPosition[i][j]) === 11 && (j > 0 && j < 4)) {
                                r.scatterSymbol = true;
                                r.scatterSymbolCount = (r.scatterSymbolCount || 0) + 1;
                            }
                            r.reelPosition = i;

                            /*if(!game.randomElement[i])
                                console.log(game.randomElement[i], 'random');*/
                            s.texture = symbolSource[spinH.slotPosition[i][j]];

                            if (parseInt(game.slotPosition[i][j]) === 11) {
                                this.nrScatter += 1;
                            }

                            // spinH.slotPosition[i][j] = new PIXI.AnimatedSprite(game.slotTexturesHighlighted['symbol11_h']);
                            // this.reelContainer.getChildAt(i).getChildAt(j).symbolElement = 10;

                            if (!game.respin && this.nrFreeSpin === 0) {
                                if (parseInt(game.slotPosition[i][j]) === 10 && [2].includes(i)) {
                                    this.nrStarPos = j;
                                    this.respin = true;
                                    this.nrFreeSpin += 1;
                                    // this.reelContainer.getChildAt(2).getChildAt(j).visible = false;
                                    this.startSpin = false;

                                }
                            }


                            s.symbolElement = spinH.slotPosition[i][j];
                            //s.x = Math.round((SYMBOL_WIDTH - s.width) / 2);
                            //if(!game.randomElement[i]){

                            delete spinH.slotPosition[i][j];
                            if (Object.keys(spinH.slotPosition[i]).length === 0)
                                delete spinH.slotPosition[i];
                            //}
                        }
                    }
                }
            }
            if(this.nrScatter < 3) {
                this.nrScatter = 0;
            }
            if (this.respin) {
                actionContainer.getChildByName("spin-btn").visible = false;
                actionContainer.getChildByName("dark-spin").visible = true;
            }
        });
    }

    addStars(starPos) {
        // this.reelContainer.getChildAt(2).visible = false;

        oAL.highlightedTitleImg.play()
        ninjaStar.play();

        setTimeout(() => {
            oAL.highlightedTitleImg.gotoAndStop(0)
        }, 1500)

        let timeStar = 0;
        for (let k = 1; k <= 3; k++) {
            let startPosition;
            let disablePos = [];
            let symbolH = new PIXI.AnimatedSprite(game.symbolAnimation['10']);
            symbolH.zIndex = 20;
            symbolH.loop = true;
            symbolH.animationSpeed = 0.800;
            symbolH.name = 'symbol-h';
            symbolH.x = SYMBOL_SIZE * 3 + 5;
            symbolH.play();

            if (starPos === 3) {
                disablePos = [0, 3, 2, 1];
                startPosition = SYMBOL_SIZE * 2 + 5;
                if (k === 1) {
                    symbolH.y = startPosition;
                } else if (k === 2) {
                    symbolH.y = startPosition - SYMBOL_SIZE;
                } else if (k === 3) {
                    symbolH.y = startPosition - SYMBOL_SIZE * 2;
                }
            } else if (starPos === 1) {
                disablePos = [0, 1, 2, 3];
                if (k === 1) {
                    symbolH.y = 5;
                } else if (k === 2) {
                    symbolH.y = SYMBOL_SIZE + 5;
                } else if (k === 3) {
                    symbolH.y = SYMBOL_SIZE * 2 + 5;
                }
            } else if (starPos === 2) {
                disablePos = [0, 2, 1, 3];
                startPosition = SYMBOL_SIZE + 5;
                if (k === 1) {
                    symbolH.y = SYMBOL_SIZE + 5;
                } else if (k === 2) {
                    symbolH.y = 5;
                } else if (k === 3) {
                    symbolH.y = SYMBOL_SIZE * 2 + 5;
                }

            }
            // this.reelContainer.getChildAt(i).removeChildren();
            //
            if (k === 2) {
                timeStar = 400;
            } else if (k === 3) {
                timeStar = 800;
            }
            setTimeout(() => {
                this.reelContainer.getChildAt(2).getChildAt(disablePos[k]).visible = false;
                game.fakeReelContainer.addChild(symbolH);

                if (k === 3) {
                    oAL.respinTextTitle.visible = true;
                    oAL.respinTextTitle.loop = false;
                    oAL.respinTextTitle.animationSpeed = 0.400;
                    oAL.respinTextTitle.play();
                    actionContainer.getChildByName("spin-btn").visible = false;
                    actionContainer.getChildByName("dark-spin").visible = true;
                }
            }, timeStar)

            if (isMobile() && !isLandscape()) {
                oAL.respinTextTitle.x = 230 // GAME_WIDTH_PORTRAIT / 2 + oAL.respinTextTitle.width / 10;
                oAL.respinTextTitle.x = GAME_WIDTH - (oAL.respinTextTitle.width * 1.1)
                oAL.respinTextTitle.y = -GAME_HEIGHT_PORTRAIT / 7;
            } else {
                oAL.respinTextTitle.x = GAME_WIDTH / 2 - oAL.respinTextTitle.width / 2;
                oAL.respinTextTitle.y = 0//GAME_HEIGHT / 6 + oAL.respinTextTitle.height / 6;
            }
            oAL.zIndex = 15;
            // oAL.respinTextTitle.visible = true;

            gameCompleted.winText.visible = false;

            setTimeout(() => {
                this.startSpin = true;
                spinH.activeSpinButton();
                respinStart.play();
            }, 3000)
        }

    }

    triggerHighlightedTitle() {
        if (game.nrScatter >= 3) {
            game.nrScatter = 0;

            oAL.highlightedTitleImg.play()
            setTimeout(() => {
                oAL.highlightedTitleImg.gotoAndStop(0)
            }, 1500)
        }
    }

    restScatterCounter() {
        for (let i = 0; i < game.reels.length; i++) {
            game.reels[i].scatterSymbolCount = 0;
        }
    }

    checkAndRunSymbolHighlight(container) {
        for (let k = 0; k < container.children.length; k++) {
            if (container.children[k].children.length > 0)
                container.children[k].children[0].play();
        }
    }

    removeSymbolHighlight(container) {
        console.log(container.parent.children[0].children[0]);
        for (let i = 0; i <= 2; i++) {
            for (let a = 0; a < container.parent.children[i].children.length; a++) {
                container.parent.children[i].children[a].children = []
            }
        }

    }
}



