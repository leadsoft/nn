class ShowSymbols {
    constructor() {
        this.lines = [];
        this.stop = false;
        this.frameAnimPosition = {};
    }

    destroy() {
        this.stop = true;
    }

    async buildFakeMap() {
        return new Promise(async function (resolve) {
            let winPayline = game.paylines;
            let winSymbolValue = [];
            let totalMargin = settings.REEL_TOTAL_MARGIN;
            let symbolValue;
            let symbolElement;
            for (let i = 0; i < winPayline.length; i++) {
                let paylineContainer = new PIXI.Container();
                paylineContainer.name = winPayline[i].payline;

                let bigSymbol = false;
                for (let j = 0; j < winPayline[i].win_symbols.length; j++) {
                    let symbol = game.reels[winPayline[i].win_symbols[j].col].symbols[winPayline[i].win_symbols[j].row];
                    if (spinH.gameType.name === 'free-spin' && [1, 2, 3].includes(winPayline[i].win_symbols[j].col)) {
                        if (!bigSymbol) {
                            /*TODO: build big symbol functionality*/

                            symbolValue = settings.FS_FEATURES_RES;
                            winSymbolValue.push(symbolValue);
                            symbolElement = freeS.bigSymbolHighlighted[symbolValue];
                            let symbolContainer = new PIXI.Container();
                            symbolContainer.sortableChildren = true;
                            symbolContainer.name = 'big';
                            symbolContainer.bigName = 'big' + j + '-' + symbolValue;

                            /*let bSelementMargin = 0;
                            if (symbol.column > 0)
                                bSelementMargin = symbol.column * (totalMargin / (settings.REEL_NUMBER - 1));*/

                            symbolContainer.x = SYMBOL_CONTAINER_WIDTH//game.reelContainer.width / 2 - BIG_SYMBOL_WIDTH / 2;
                            symbolContainer.bigSymbolContainer = true;
                            paylineContainer.addChild(symbolContainer);

                            game.fakeReelContainer.addChild(paylineContainer);

                            bigSymbol = true;
                        }
                    }

                    symbolValue = winPayline[i].win_symbols[j].value;


                    winSymbolValue.push(symbolValue);

                    symbolElement = game.symbolAnimation[symbolValue];

                    let symbolContainer = new PIXI.Container();
                    symbolContainer.sortableChildren = true;
                    symbolContainer.name = j + '-' + symbolValue;

                    /*let elementMargin = 0;
                    if (symbol.column > 0)
                        elementMargin = symbol.column * (totalMargin / (settings.REEL_NUMBER - 1));*/


                    if(symbolValue !== 11) {
                        symbolContainer.x = SYMBOL_CONTAINER_WIDTH * symbol.column//symbol.width * symbol.column + elementMargin;
                        symbolContainer.y = SYMBOL_CONTAINER_SIZE * (symbol.row - 1)/* + (symbol.row * SYMBOL_MARGIN_TOP)*/;
                    } else {
                        symbolContainer.x = SYMBOL_CONTAINER_WIDTH * symbol.column - 14//symbol.width * symbol.column + elementMargin;
                        symbolContainer.y = SYMBOL_CONTAINER_SIZE * (symbol.row - 1) + 16/* + (symbol.row * SYMBOL_MARGIN_TOP)*/;
                    }

                    paylineContainer.addChild(symbolContainer);

                    game.fakeReelContainer.addChild(paylineContainer);

                    // if (j === 2 && game.nrFreeSpin === 2) {
                    //     symbolContainer.x = -1000;
                    // }

                    if (i === winPayline.length - 1 && j === winPayline[i].win_symbols.length - 1)
                        return resolve();
                }
            }
        })
    }

    async showAllWinningSymbols(spinIndex = 0) {

        setSettings.STATUS_SENT = false;
        setSettings.INITIAL_WIN = true;
        let winSound = 'minorWin';
        let self = this;
        self.frameAnimPosition = {};
        let winPayline = [];
        let winSymbolValue = [];
        let totalMargin = settings.REEL_TOTAL_MARGIN;
        await this.buildFakeMap();
        //await sleep(10000);
        return new Promise(async function (resolve) {
            winPayline = game.paylines;

            let symbolValue;
            let symbolElement;
            gameCompleted.addRemoveBlurAllSymbols(true);
            game.fakeReelContainer.visible = true;

            for (let payline = 0; payline < game.paylines.length; payline++) {
                let bigSymbolSelected = false;
                for (let j = 0; j < game.paylines[payline].win_symbols.length; j++) {

                    let fakeContainer = game.fakeReelContainer.getChildByName(game.paylines[payline].payline);

                    symbolValue = game.paylines[payline].win_symbols[j].value;
                    winSymbolValue.push(symbolValue);
                    let fakeSymbol = fakeContainer.getChildByName(j + '-' + symbolValue);


                    if(fakeSymbol && fakeSymbol.visible === false) {
                        fakeSymbol.visible = true;
                    }
                    /*if (symbolValue !== 0 && symbolValue !== 11) {*/
                    let fakeSymbolFrame = /*settings.FS_ENABLE_STATUS ? freeS.animations :*/ game.animations;
                    let symbolFrameAnim = new PIXI.AnimatedSprite(fakeSymbolFrame['frame_symbol_flashing']);
                    let symbolFrameBackground = new PIXI.Sprite(game.animations['frame_symbol_background']);
                    symbolFrameBackground.name = 'symbol-frame-background';

                    // fakeSymbol.addChild(symbolFrameAnim, symbolFrameBackground);

                    symbolFrameAnim.animationSpeed = settings.SYMBOL_FRAME_SPEED;
                    symbolFrameAnim.zIndex = 1;
                    symbolFrameAnim.name = 'frame-animation';
                    symbolFrameAnim.x = (SYMBOL_CONTAINER_WIDTH - symbolFrameAnim.width) / 2;
                    symbolFrameAnim.y = (SYMBOL_SIZE - symbolFrameAnim.height) / 2;

                    symbolFrameBackground.x = (SYMBOL_CONTAINER_WIDTH - symbolFrameBackground.width) / 2;
                    symbolFrameBackground.y = (SYMBOL_SIZE - symbolFrameBackground.height) / 2;

                    symbolFrameAnim.loop = true;
                    symbolFrameAnim.play();

                    if (fakeSymbol && self.frameAnimPosition[fakeSymbol.name] !== undefined) {
                        symbolFrameAnim.visible = false;
                        symbolFrameBackground.visible = false;

                        self.frameAnimPosition[fakeSymbol.name] = true;

                    }

                    /*}*/

                    // symbolElement = game.symbolAnimation[symbolValue];
                    if(symbolValue === 10) {
                        symbolElement = game.symbolAnimation[symbolValue + "-h"];
                    } else {
                        symbolElement = game.symbolAnimation[symbolValue];
                    }

                    let symbolAnim = '';
                    if(symbolElement) {
                        symbolAnim = new PIXI.AnimatedSprite(symbolElement);
                    }

                    symbolAnim.name = 'symbolAnim-' + symbolValue;
                    symbolAnim.zIndex = 2;
                    symbolAnim.x = SYMBOL_CONTAINER_WIDTH / 2 - SYMBOL_WIDTH / 2;
                    if(symbolValue === 10 && game.paylines.length > 0) {
                        game.reelContainer.getChildAt(2).getChildAt(game.nrStarPos).visible = false;
                        symbolAnim.loop = false;
                    } else {
                        symbolAnim.loop = false;
                    }

                    if(fakeSymbol && symbolAnim) {
                        fakeSymbol.addChild(symbolAnim);
                        symbolAnim.animationSpeed = 0.300;
                        symbolAnim.play();

                        symbolAnim.y = -(symbolAnim.height - SYMBOL_SIZE);
                    }

                    if ([0, 10].includes(symbolValue)) {
                        let symbolAnimHighlighted = new PIXI.AnimatedSprite(game.symbolAnimation[symbolValue + '-h']);
                        symbolAnimHighlighted.name = 'symbolAnim-' + symbolValue + '-h';
                        symbolAnimHighlighted.zIndex = 2;

                        symbolAnimHighlighted.loop = false;
                        symbolAnimHighlighted.visible = false;

                        if(symbolValue !== 11) {
                            symbolAnimHighlighted.x = SYMBOL_CONTAINER_WIDTH / 2 - SYMBOL_WIDTH / 2;
                            symbolAnimHighlighted.y = -(symbolAnimHighlighted.height - SYMBOL_SIZE);
                        } else {
                            // symbolContainer.x = SYMBOL_CONTAINER_WIDTH * symbol.column - 14//symbol.width * symbol.column + elementMargin;
                            // symbolContainer.y = SYMBOL_CONTAINER_SIZE * (symbol.row - 1) + 16/
                            symbolAnimHighlighted.x = SYMBOL_CONTAINER_WIDTH / 2 - SYMBOL_WIDTH / 2 + 14;
                            symbolAnimHighlighted.y = -(symbolAnimHighlighted.height - SYMBOL_SIZE) - 15;
                        }
                        fakeSymbol && symbolAnimHighlighted && fakeSymbol.addChild(symbolAnimHighlighted);
                        symbolAnimHighlighted.animationSpeed = 0.400;
                    }

                    if (spinH.gameType.name === 'free-spin' && [1, 2, 3].includes(game.paylines[payline].win_symbols[j].col)) {
                        fakeSymbol.visible = false;
                        if (!bigSymbolSelected) {
                            symbolValue = settings.FS_FEATURES_RES;
                            winSymbolValue.push(symbolValue);

                            let bigSymbol = new PIXI.Sprite(freeS.bigSymbolHighlighted[symbolValue]);
                            let fakeBigSymbol = fakeContainer.getChildByName('big');
                            bigSymbol.name = 'big-symbol';
                            bigSymbolSelected = true;
                            fakeBigSymbol.addChild(bigSymbol);
                            bigSymbol.y = 15;
                            bigSymbol.x = 0//game.reelContainer.width / 2 - bigSymbol.width / 2;

                            let fakeBigSymbolFrame = freeS.animations;
                            let symbolFrameAnim = new PIXI.AnimatedSprite(fakeBigSymbolFrame['big_symbol_flashing']);
                            let bigSymbolFrameBackground = new PIXI.Sprite(freeS.spritesheet.frame_symbol_background);
                            fakeBigSymbol.addChild(bigSymbolFrameBackground, symbolFrameAnim);

                            symbolFrameAnim.animationSpeed = settings.SYMBOL_FRAME_SPEED;
                            symbolFrameAnim.zIndex = 1;

                            symbolFrameAnim.x = (bigSymbol.width - symbolFrameAnim.width) / 2;
                            symbolFrameAnim.y = (bigSymbol.height - symbolFrameAnim.height) / 2;

                            bigSymbolFrameBackground.x = (bigSymbol.width - bigSymbolFrameBackground.width) / 2;
                            bigSymbolFrameBackground.y = (bigSymbol.height - bigSymbolFrameBackground.height) / 2;

                            symbolFrameAnim.name = 'big-frame-animation';

                            symbolFrameAnim.loop = true;
                            symbolFrameAnim.play();

                            if (self.frameAnimPosition[fakeBigSymbol.name] !== undefined) {
                                symbolFrameAnim.visible = false;
                                bigSymbolFrameBackground.visible = false;
                            }

                            self.frameAnimPosition[fakeBigSymbol.name] = true;
                        }
                    }
                }



                if (typeof gameCompleted.linesIndicators[winPayline[payline].payline] !== "undefined" && !settings.STOP_PAYLINE_ANIMATION) {
                    gameCompleted.linesIndicators[winPayline[payline].payline].visible = true;
                }
            }

            /*await sleep(300000);*/
            /*if(spinH.gameType.name === 'free-spin')
                await sleep(1000000);*/

            /*if (typeof winSymbolValue.find(element => element === 0) !== "undefined" && !winSymbolValue.find(element => element === 9)) {*/
            if (!winSymbolValue.some(r => [0, 5, 6, 7, 8, 9, 10, 11].indexOf(r) >= 0) ) {
                winSound = 'highWin';
            } else if (!winSymbolValue.some(r => [0, 1, 2, 3, 4, 10, 11].indexOf(r) >= 0)) {
                winSound = 'lowWin';
            } else if (!winSymbolValue.some(r => [0, 10, 11].indexOf(r) >= 0)  && winSymbolValue.some(r => [1,2,3,4,5,6,7,8,9].indexOf(r) >= 0)) {
                winSound = 'win';
            }
            else if (!winSymbolValue.some(r => [5, 6, 7, 8, 9, 10, 11].indexOf(r) >= 0) && winSymbolValue.find(element => element === 0) ){// && winSymbolValue.some(r => [1,2,3,4].indexOf(r) >= 0)) {
                winSound = 'highWildWin';
            } else if (!winSymbolValue.some(r => [1, 2, 3, 4].indexOf(r) >= 0) && winSymbolValue.find(element => element === 0) ){//&& winSymbolValue.some(r => [5,6,7,8,9].indexOf(r) >= 0)) {
                winSound = 'lowWildWin';
            } else if (!winSymbolValue.some(r => [10, 11].indexOf(r) >= 0) && winSymbolValue.find(element => element === 0) && winSymbolValue.some(r => [1,2,3,4,5,6,7,8,9].indexOf(r) >= 0)) {
                winSound = 'wildWin';
            } else if (!winSymbolValue.some(r => [0,1,2,3,4,5,6,7,8,9,10].indexOf(r) >= 0) && winSymbolValue.find(element => element === 11)) {
                if(winSymbolValue.length <= 5) {
                    winSound = 'scatterWin';
                } else {
                    winSound = 'scatterLargeWin';
                }
            } else if (winSymbolValue.some(r => [0,1,2,3,4,5,6,7,8,9,11].indexOf(r) >= 0) && winSymbolValue.find(element => element === 10)) {
                winSound = 'mixWin';
            }

            // else if (typeof winSymbolValue.find(element => element === 0) !== "undefined" && !winSymbolValue.find(element => element === 9) && winSymbolValue.some(r => [5, 6, 7, 8].indexOf(r) >= 0)) {
            //     winSound = 'majorWildWin';
            // } else if (winSymbolValue.some(r => [1, 2, 3, 4].indexOf(r) >= 0) && !winSymbolValue.some(r => [0, 9].indexOf(r) >= 0)) {
            //     winSound = 'majorWin';
            // } else if (typeof winSymbolValue.find(element => element === 0) !== "undefined" && !winSymbolValue.find(element => element === 9) && winSymbolValue.some(r => [1, 2, 3, 4].indexOf(r) >= 0)) {
            //     winSound = 'minorWildWin';
            // }

            if (settings.LAST_WIN_AMOUNT.toFixed(2) > 0) {


                let winImageContainer = new PIXI.Container();
                winImageContainer.name = "ss-win-image-container";

                let containerBackgroundSprite = new PIXI.Sprite(app.loader.resources.prizeElementsNN.spritesheet.textures['prizeOverlay/prizeElementBg.png'])
                winImageContainer.addChild(containerBackgroundSprite)

                let currency = new PIXI.Sprite(oAL.fonts[user.currencySymbol]);
                currency.scale.set(0.45, 0.45)
                currency.name = 'font';
                currency.x = 30;
                currency.y = containerBackgroundSprite.height / 2 - currency.height / 1.7
                winImageContainer.addChild(currency);
                winImageContainer.zIndex = 9;
                winImageContainer.name = 'win-image-container';
                let symbolWidth = currency.width + 10;

                let winAmount = formatMoney(settings.LAST_WIN_AMOUNT.toFixed(2))
                for (var i = 0; i < winAmount.length; i++) {
                    let fontElement = winImageContainer.children;
                    let charImage = new PIXI.Sprite(oAL.fonts[winAmount.charAt(i)]);
                    charImage.name = 'font';
                    charImage.scale.set(0.35, 0.35)
                    charImage.x = fontElement[fontElement.length - 1].x + fontElement[fontElement.length - 1].width;
                    charImage.y = 10//containerBackgroundSprite.height / 2 - charImage.height / 2
                    symbolWidth += charImage.width
                    winImageContainer.addChild(charImage)
                }
                gameCompleted.winText.addChild(winImageContainer);
                containerBackgroundSprite.width =  symbolWidth + currency.x * 2//winImageContainer.width + currency.x
                containerBackgroundSprite.height = winImageContainer.height
                gameCompleted.winText.scale.set(0.8, 0.8)

                //gameCompleted.winText.text = user.currencySymbol + settings.LAST_WIN_AMOUNT.toFixed(2); //TODO: revert this
                if (!isLandscape())
                    gameCompleted.winText.x = (GAME_WIDTH / 2) + (gameCompleted.winText.width / 8); // (gameContainer.width / gameContainer.scale.x / 2) - (gameCompleted.winText.width / 2);
                else
                    gameCompleted.winText.x = (GAME_WIDTH / 2) - (gameCompleted.winText.width / 2);
                gameCompleted.winText.y = game.reelContainer.y + ((SYMBOL_SIZE * 3 + (3 * SYMBOL_MARGIN_TOP)) / 2 - (gameCompleted.winText.height / 2));
                //oAL.winLine.visible = true;
            }



            if (!gameCompleted.winTextAdded) {
                gameCompleted.winTextAdded = true;
                gameCompleted.winText.name = 'win-text';

                gameCompleted.winText.zIndex = 5;
                gameContainer.addChild(gameCompleted.winText);
            }

            if (settings.GAME_RUNNING)
                return;
            if (settings.LAST_WIN_AMOUNT.toFixed(2) > 0) {

                let wSound = eval(winSound).play();

                setTimeout(() => {
                    if(isMobile() && settings.FS_ENABLE_STATUS){
                        oAL.tick.startTick(settings.LAST_WIN_AMOUNT.toFixed(2), null, footer.betAmount, 'normal-win', false);
                    } else {
                        oAL.tick.startTick(settings.LAST_WIN_AMOUNT.toFixed(2), null, footer.winAmount, 'normal-win', false);
                    }

                }, 500)

                wSound.on('end', async function () {
                    if (settings.GAME_RUNNING)
                        return;
                    setSettings.TICKER_SOUND_STATUS = true;

                    if (spinIndex !== settings.CURRENT_SPIN_INDEX)
                        return;

                    /*oAL.tick.startTick(settings.LAST_WIN_AMOUNT.toFixed(2), null, footer.winAmount, 'normal-win', false);*/
                    if (!settings.GAME_RUNNING && !settings.FS_STATUS)
                        footer.showHideSpinButton(true)

                    //await checkTickerStatus();
                    if (self.stop) return;

                    await sleep(1000)
                    await waitUntilIsDone('TICK_STATUS');
                    if (!settings.STATUS_SENT) {

                        setSettings.STATUS_SENT = true;
                        gameCompleted.addRemoveBlurAllSymbols(false);
                        gameCompleted.hideActivePayline();
                        gameCompleted.hideAllLines();
                        gameCompleted.hideAllLinesIndicators();
                        /////////////////////////////////////////

                        if(game.paylines.length === 1 && game.respin && game.nrFreeSpin === 1) {
                            // actionContainer.getChildByName("spin-btn").visible = false;
                            // actionContainer.getChildByName("dark-spin").visible = true;
                            game.startSpin = false;
                            game.addStars(game.nrStarPos);
                            // game.linesIndicators.visible = false;

                            let val = game.fakeReelContainer.children.length;
                            for (let i = 0; i < val; i++) {
                                // if (game.fakeReelContainer.children[i].name !== "symbol-h") {
                                    game.fakeReelContainer.getChildAt(i).removeChildren();
                                // }
                            }
                        }


                        if (!settings.FS_STATUS && settings.FS_COUNT === 0)
                            setSettings.FS_STOP = true;
                        else if (settings.FS_STATUS && !settings.FS_START)
                            setSettings.FS_START = true;

                        if(game.paylines.length === 1){
                            let paylineText = '';
                            let gamePayline = game.paylines[0];

                            if (typeof gamePayline === "undefined") return;

                            if (typeof gamePayline !== "undefined")
                                paylineText = "LINE " + gamePayline.payline + " WINS " + user.currencySymbol + formatMoney(gamePayline.amount.toFixed(2));

                            if (typeof gamePayline !== "undefined" && gamePayline.payline === -1)
                                paylineText = "SCATTER PAYS " + user.currencySymbol + game.paylines[0].amount.toFixed(2);

                            setAndCenterPaylineText(paylineText)
                        }
                        return resolve(1);
                    }
                });
            } else {
                await sleep(1000)
                if (!settings.STATUS_SENT) {
                    setSettings.STATUS_SENT = true;

                    gameCompleted.addRemoveBlurAllSymbols(false);
                    gameCompleted.hideActivePayline();
                    gameCompleted.hideAllLines();
                    gameCompleted.hideAllLinesIndicators();

                    if (!settings.FS_STATUS && settings.FS_COUNT === 0)
                        setSettings.FS_STOP = true;
                    else if (settings.FS_STATUS && !settings.FS_START)
                        setSettings.FS_START = true;

                    return resolve(1);
                }
            }
        })
    }
}

/*function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}*/

