var stompTaasClient = "";
var rebuyResp = {}
var tournamentStylingData = {
    display: true,
    height: { desktop: 32, mobile: 30 },
    coordinates: { x: 0, y: 50 },
    infoWidget: { width: 250 }
}
var tourDetail = false
var updateCaptured = false
var processUpdate = false
var openInfoWindow = false

function getRemainTime(milis) {

}

function hideRankInfo() {

}

function initTournament() {
    addWidget();
    tourDetail = {
        currency: settings.TOURNAMENT_CURRENCY,
        type: 2,//settings.TOURNAMENT_RANK_TYPE,
        rank: settings.TOURNAMENT_RANK,
        rankDetails: 12,
        betsToQualify: settings.TOURNAMENT_QUALIFY_NUM,
        bets: 2,
        stake: settings.TOURNAMENT_REBUY_STAKE,
        rebuy: settings.TOURNAMENT_REBUY === 'true',
        totalPrizeCount: 1,
        cashPrize: 1,
        bonusPrize: 3,
        rewardsPrize: 5,
        detail: {
            currency: settings.TOURNAMENT_CURRENCY,
            participants: 3,
            endsInMillis: 123123123,
            endType: 1
        }
    };
    updateTournamentDetails(tourDetail)
}

function updateTournamentDetails(e) {
    let widgetPanel = document.getElementById('ranking-widget');
    let infoPanel = document.getElementById('rank-info');
    let infoContent = document.getElementById('rank-info-content');
    let rankNo = document.getElementById('rank-no');
    let balance = document.getElementById('balance');
    let rebuyBtn = document.getElementById('rebuy-btn');
    let widgetContent = document.getElementById('widget-content');
    let rebuyMsgPanel = document.getElementById('rebuy-msg-box');
    let rebuyConfirmMsg = document.getElementById('rebuyConfirmMsg');
    let rebuyConfirmBtn = document.getElementById('rebuy-confirm');
    let rebuyCloseBtn = document.getElementById('rebuy-close');

    if (infoPanel) infoPanel.style.display = "none";
    if (infoContent) infoContent.innerHTML = "";
    if (rebuyMsgPanel) rebuyMsgPanel.style.display = "none";
    if (rebuyConfirmMsg) rebuyConfirmMsg.innerHTML = "";
    if (rebuyConfirmBtn) rebuyConfirmBtn.style.display = "block";
    if (rebuyCloseBtn) rebuyCloseBtn.innerHTML = "Cancel";
    if (e.type == 1) {
        if (typeof tourDetail !== 'object')
            tourDetail = {game: e};
        else {
            if (!e.preventCapture && (updateCaptured === false || !processUpdate)) {
                updateCaptured = e;
                return false;
            } else if (processUpdate) {
                tourDetail = {game: updateCaptured};
                updateCaptured = false;
                processUpdate = false;
            }
        }
        console.log(tourDetail, 'tourDetail');
        if (e.error) {
            if (widgetPanel) widgetPanel.style.display = 'none';
            if (rebuyMsgPanel) rebuyMsgPanel.style.display = "block";
            if (rebuyConfirmBtn) rebuyConfirmBtn.style.display = "none";
            if (rebuyCloseBtn) rebuyCloseBtn.innerHTML = "OK";
            if (rebuyConfirmMsg) {
                rebuyConfirmMsg.innerHTML = e.error;
            }
        } else {
            if (widgetPanel && widgetPanel.classList.contains('active') && !tourDetail.detail) {
                console.log('here1...');
                getTaasTournamentDetail(0);
            } else {

                if (widgetPanel) widgetPanel.style.display = 'inline-flex';
                let currSymbol = (tourDetail.game.currencySymbol ? tourDetail.game.currencySymbol : (tourDetail.detail && tourDetail.detail.currencySymbol ? tourDetail.detail.currencySymbol : '$'));

                let isQualified = !(typeof tourDetail.game.betsToQualify != 'undefined' && +tourDetail.game.betsToQualify > 0);
                console.log(currSymbol);
                if (rankNo) rankNo.innerHTML = isQualified ? "Rank: " + tourDetail.game.rank : "Bets to qualify: " + tourDetail.game.betsToQualify;
                if (balance) balance.innerHTML = isQualified ? (tourDetail.game.rank && typeof tourDetail.game.rankBalance != 'undefined' ? "Ranking Balance: " + currSymbol + tourDetail.game.rankBalance.toFixed(2) : "Balance: " + currSymbol + tourDetail.game.stake) : "Bets made: " + tourDetail.game.bets;
                if (rebuyBtn) rebuyBtn.style.display = tourDetail.game.rebuy ? "block" : "none";
                if (widgetContent && (tourDetail.detail || (widgetPanel && widgetPanel.classList.contains('active')))) {
                    widgetContent.innerHTML = (!isQualified && typeof tourDetail.game.stake != 'undefined' ? '<div class="info-data">Balance: ' + currSymbol + tourDetail.game.stake.toFixed(2) + '</div>' : '') +
                        (tourDetail.detail && tourDetail.detail.endsInMillis && (!tourDetail.detail.endType || tourDetail.detail.endType == 1) ? '<div class="info-data">Time Left: ' + getRemainTime(tourDetail.detail.endsInMillis) + '</div>' : '');
                }
            }
        }
    } else if (e.type == 2) {
        if (!openInfoWindow && typeof tourDetail == 'object' && !tourDetail.detail) {
            tourDetail.detail = e;
            updateTournamentDetails({type: 1, preventCapture: true});
        } else {
            if (openInfoWindow) {
                tourDetail.detail = e;
                openInfoWindow = false;
                updateTournamentDetails({type: 1, preventCapture: true});
            }
            console.log(e, 'here...3')
            Object.keys(e).forEach((ind, key) => {
                console.log(e[ind], 'e[ind]')
                //tourDetail.detail[ind] = e[ind]
            });
            if (infoPanel) infoPanel.style.display = "block";
            if (infoContent) {
                let currSymbol = (tourDetail.currencySymbol ? tourDetail.currencySymbol : (tourDetail.detail && tourDetail.detail.currencySymbol ? tourDetail.detail.currencySymbol : '$'));

                infoContent.innerHTML = (typeof tourDetail.detail.endType != 'undefined' ? '<li><label>End Type:</label>' +
                        '<span>' + (tourDetail.detail.endType == 1 ? 'Scheduled' : 'Knockout') + '</span></li>' : '') +
                    (typeof tourDetail.bets != 'undefined' ? '<li><label>Bets Made:</label>' +
                        '<span>' + tourDetail.bets + '</span></li>' : '') +
                    (typeof tourDetail.detail.participants != 'undefined' ? '<li><label>Participants:</label>' +
                        '<span>' + tourDetail.detail.participants + '</span></li>' : '') +
                    (typeof tourDetail.rank != 'undefined' && typeof tourDetail.detail.totalPrizeCount != 'undefined' ?
                            '<li><label>Prize Position:</label> <span>' + tourDetail.rank + ' of ' + tourDetail.detail.totalPrizeCount + '</span></li>' :
                            (typeof tourDetail.detail.totalPrizeCount != 'undefined' ? '<li><label>Prize Positions:</label>' +
                                '<span>' + tourDetail.detail.totalPrizeCount + '</span></li>' : '')
                    ) +
                    ('<li class="prizes"><label>Your current prize allocation</label>' +
                        (tourDetail.rank && (typeof tourDetail.cashPrize != 'undefined' || typeof tourDetail.bonusPrize != 'undefined' || typeof tourDetail.rewardsPrize != 'undefined') ?
                            ('<ul>' +
                                (typeof tourDetail.cashPrize != 'undefined' ? '<li><label>Cash: </label><span>' + currSymbol + formatNumber(tourDetail.cashPrize, 2) + '</span></li>' : '') +
                                (typeof tourDetail.bonusPrize != 'undefined' ? '<li><label>Bonus: </label><span>' + currSymbol + formatNumber(tourDetail.bonusPrize, 2) + '</span></li>' : '') +
                                (typeof tourDetail.rewardsPrize != 'undefined' ? '<li><label>Reward Points: </label><span>' + formatNumber(tourDetail.rewardsPrize, 0) + '</span></li>' : '') +
                                '</ul>') :
                            ('<p>Currently not in a prize position. Keep trying.</p>'))
                        + '</li>');
            }
        }
    } else if (e.type == 3) {
        rebuyResp = e;
        if (rebuyMsgPanel) rebuyMsgPanel.style.display = "block";
        if (rebuyConfirmBtn) rebuyConfirmBtn.style.display = "block";
        if (rebuyCloseBtn) rebuyCloseBtn.innerHTML = "Cancel";
        if (rebuyConfirmMsg) {
            if (typeof e.fromCurrencySymbol != 'undefined' && typeof e.rate != 'undefined') {
                rebuyConfirmMsg.innerHTML = 'Would you like buy-in to tournament ' + '#' + e.tournamentId + ' - ' + e.description + '? Your balance will be debited ' + e.fromCurrencySymbol + (+e.fee * +e.rate).toFixed(2) + ' (' + e.toCurrencySymbol + e.fee.toFixed(2) + ' ' + e.toCurrencyCode + ' @ ' + e.rate.toFixed(3) + ').<span class="goodluck"> Good Luck!</span>';
            } else {
                rebuyConfirmMsg.innerHTML = 'Would you like to Rebuy back into tournament #' + e.tournamentId + ' - ' + e.description + '? Your balance will be debited ' + e.toCurrencySymbol + e.fee.toFixed(2) + '.<span class="goodluck"> Good Luck!</span>';
            }
        }
    } else if (e.type == 4) {
        if (rebuyMsgPanel) rebuyMsgPanel.style.display = "block";
        if (rebuyConfirmBtn) rebuyConfirmBtn.style.display = "none";
        if (rebuyCloseBtn) rebuyCloseBtn.innerHTML = "OK";
        if (rebuyConfirmMsg) {
            if (e.success) {
                if (typeof rebuyResp.fromCurrencySymbol != 'undefined' && typeof rebuyResp.rate != 'undefined') {
                    rebuyConfirmMsg.innerHTML = 'Your balance has been debited ' + (rebuyResp.fromCurrencySymbol ? rebuyResp.fromCurrencySymbol : '') + (rebuyResp.fee && rebuyResp.rate ? (+rebuyResp.fee * +rebuyResp.rate).toFixed(2) + ' (' + (rebuyResp.toCurrencySymbol ? rebuyResp.toCurrencySymbol : '') + rebuyResp.fee.toFixed(2) + ' ' + (rebuyResp.toCurrencyCode ? rebuyResp.toCurrencyCode : '') + ' @ ' + rebuyResp.rate.toFixed(3) + ')' : '') + ' from your account and added to your tournament balance.<span class="goodluck"> Good Luck!</span>';
                } else {
                    rebuyConfirmMsg.innerHTML = 'Your balance has been debited ' + (rebuyResp.toCurrencySymbol ? rebuyResp.toCurrencySymbol : '') + (rebuyResp.fee ? rebuyResp.fee.toFixed(2) : '') + ' from your account and added to your tournament balance.<span class="goodluck"> Good Luck!</span>';
                }
                tourDetail.game.rebuy = false;
                if (typeof e.stake != 'undefined') tourDetail.game.stake = +e.stake;
                if (typeof e.betsToQualify != 'undefined') tourDetail.game.betsToQualify = +e.betsToQualify;
                if (typeof e.rank != 'undefined') tourDetail.game.rank = +e.rank;

                let currSymbol = (tourDetail.game.currencySymbol ? tourDetail.game.currencySymbol : (tourDetail.detail && tourDetail.detail.currencySymbol ? tourDetail.detail.currencySymbol : '$'));
                let isQualified = !(typeof tourDetail.game.betsToQualify != 'undefined' && +tourDetail.game.betsToQualify > 0);

                if (rankNo) rankNo.innerHTML = isQualified ? "Rank: " + tourDetail.game.rank : "Bets to qualify: " + tourDetail.game.betsToQualify;
                if (balance) balance.innerHTML = isQualified ? (tourDetail.game.rank && typeof tourDetail.game.rankBalance != 'undefined' ? "Ranking Balance: " + currSymbol + tourDetail.game.rankBalance.toFixed(2) : "Balance: " + currSymbol + tourDetail.game.stake.toFixed(2)) : "Bets made: " + tourDetail.game.bets;
                if (rebuyBtn) rebuyBtn.style.display = tourDetail.game.rebuy ? "block" : "none";
                if (widgetContent && (tourDetail.detail || (widgetPanel && widgetPanel.classList.contains('active')))) {
                    widgetContent.innerHTML = (!isQualified && typeof tourDetail.game.stake != 'undefined' ? '<div class="info-data">Balance: ' + currSymbol + tourDetail.game.stake.toFixed(2) + '</div>' : '') +
                        (tourDetail.detail && tourDetail.detail.endsInMillis && (!tourDetail.detail.endType || tourDetail.detail.endType == 1) ? '<div class="info-data">Time Left: ' + getRemainTime(tourDetail.detail.endsInMillis) + '</div>' : '');
                }
                let widgetBox = document.getElementById("ranking-widget-container");
                if (widgetBox) {
                    let event = new CustomEvent("TournamentRebuySuccess", {bubbles: true, detail: {}});
                    widgetBox.dispatchEvent(event);
                }
            } else {
                rebuyConfirmMsg.innerHTML = e.errorMessage;
            }
        }
    } else {
        stompTaasClient.debug("Unknown Update (" + e.type + "): " + JSON.stringify(e));
    }
}

function toggleRankWidget() {
    let widget = document.getElementById('ranking-widget');
    if (widget) {
        if (widget.classList.contains('active')) {
            widget.classList.remove('active');
        } else {
            widget.classList.add('active');
            /*if (!tourDetail.detail) {
                getTaasTournamentDetail(0);
            }*/
        }
    }
}

function rebuySubmit() {
    rebuyTaasComplete();
    let rebuyMsgPanel = document.getElementById('rebuy-msg-box');
    if (rebuyMsgPanel) rebuyMsgPanel.style.display = "none";
}

function rebuyClose() {
    rebuyResp = {};
    let rebuyMsgPanel = document.getElementById('rebuy-msg-box');
    if (rebuyMsgPanel) rebuyMsgPanel.style.display = "none";
    let rebuyConfirmMsg = document.getElementById('rebuyConfirmMsg');
    if (rebuyConfirmMsg) rebuyConfirmMsg.innerHTML = "";
    if (tourDetail.error) disconnectTaasStomp();
}

function formatNumber(t, d) {
    return (t = (t = (+t).toFixed(d)).toString().split("."))[0].replace(/\B(?=(\d{3})+(?!\d))/g, ","), d ? t.join(".") : t[0];
}

function getWidgetCoordinates() {
    let xPos = (widgetTaasData.config.coordinates && typeof widgetTaasData.config.coordinates.x != 'undefined' ? +widgetTaasData.config.coordinates.x : 0);
    let yPos = (widgetTaasData.config.coordinates && typeof widgetTaasData.config.coordinates.y != 'undefined' ? +widgetTaasData.config.coordinates.y : 0);
    let canvas = document.getElementsByClassName("taas-widget-anchor");
    if (canvas.length) {
        let marginLeft = canvas[0].offsetLeft;
        let marginTop = canvas[0].offsetTop;
        if (!isNaN(+marginLeft) && +marginLeft > 0) {
            xPos += +marginLeft;
        }
        if (!isNaN(+marginTop) && +marginTop > 0) {
            yPos += +marginTop;
        }
    }
    return [xPos, yPos];
}

function addWidget() {

    let rankWidgetHtml = '<div class="ranking-widget" id="ranking-widget" style="display: inline-flex; height: 32px; left: 0; top: 58px;">' +
        '<div class="rank-no" id="rank-no">' +
        '   </div><div class="rank-no" id="balance">' +
        '</div>' +
        '<div class="rebuy-btn" id="rebuy-btn" style="display:none;">' +
        '   <a href="javascript:void(0)" onclick="rebuyTaasConfirm()">RE-BUY NOW</a>' +
        '</div>' +
        '<div class="collapase-info" id="widget-content">' +
        '</div>' +
        '<div class="info-collapse">' +
        '   <a href="javascript:void(0)" onclick="updateTournamentDetails(2)" class="info-btn-rank">  ' +
        '       <img src="http://taaswidget.net:8080/taas/info-icon.png">' +
        '   </a>' +
        '   <a href="javascript:void(0)" onclick="toggleRankWidget()" class="arrow-rank">' +
        '       <img src="http://taaswidget.net:8080/taas/right-arrow.png">' +
        '   </a>' +
        '</div>' +
        '</div>' +
        '<div class="modal-ui-block-outer" id="rebuy-msg-box" style="display:none;">' +
        '   <div class="modal-ui-block popup-ui-block"><div class="inner-content-modal">' +
        '       <p id="rebuyConfirmMsg"></p>' +
        '   <div class="btn-block tournamanent-btnblock">' +
        '       <a href="javascript:void(0)" id="rebuy-close" onclick="taas.rebuyClose()">Cancel</a>' +
        '       <a href="javascript:void(0)" id="rebuy-confirm" onclick="taas.rebuySubmit()">Yes</a>' +
        '   </div>' +
        '</div>' +
        '</div>' +
        '</div>';

    let rankInfoHtml = '<div id="rank-info" class="modal-ranking-outer" style="display:none;">' +
        '<div class="ranking-modal" style="' + (tournamentStylingData.infoWidget && typeof tournamentStylingData.infoWidget.width != 'undefined' ? 'width:' + tournamentStylingData.infoWidget.width + 'px;' : '') + '">' +
        '   <button onclick="taas.hideRankInfo()" class="dismiss-modal">&times;</button>' +
        '   <ul id="rank-info-content"></ul>' +
        '</div>' +
        '</div>';

    let widgetBox = document.getElementById("ranking-widget-container");
    let infoBox = document.getElementById("ranking-info-container");
    if (widgetBox) {
        widgetBox.innerHTML = rankWidgetHtml;
    } else {
        var t = document.createElement("div");
        t.setAttribute("id", "ranking-widget-container");
        t.innerHTML = rankWidgetHtml;
        document.body.appendChild(t);
    }
    if (infoBox) {
        infoBox.innerHTML = rankInfoHtml;
    } else {
        var t = document.createElement("div");
        t.setAttribute("id", "ranking-info-container");
        t.innerHTML = rankInfoHtml;
        document.body.appendChild(t);
    }
}