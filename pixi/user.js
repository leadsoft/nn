class User {
    constructor() {
        this.balance = 0;
        this.currencySymbol = '$'
    }

    async debitUserBalance() {
        return new Promise(async function (resolve, reject) {
            if (settings.GAME_PLAY_MODE === 'tournament' && (settings.TOURNAMENT_REBUY === 'true' && parseFloat(settings.USER_BALANCE).toFixed(2) < parseFloat(settings.MIN_BET * settings.START_GAME_PAYLINES).toFixed(2)))
                return resolve({status: false, type: 'tournament_rebuy'});

            if (parseFloat(settings.USER_BALANCE).toFixed(2) - parseFloat(settings.MIN_BET * settings.START_GAME_PAYLINES).toFixed(2) < 0 && settings.GAME_PLAY_MODE !== 'real')
                return resolve({status: false, msg: messages.insufficient_funds, type: 'insufficient_funds'});
            /*else if(settings.GAME_PLAY_MODE === 'real' && settings.START_GAME_BET > settings.USER_BALANCE)*/
            else if (!settings.FSV_STATUS && settings.GAME_PLAY_MODE === 'real' && settings.START_GAME_BET > parseFloat(settings.USER_BALANCE) + parseFloat(settings.PROMO_CHIPS))
                return resolve({status: false, msg: messages.real_mode_insufficient_funds, type: 'real_funds'});

            let totalBet = settings.MIN_BET * settings.START_GAME_PAYLINES;
            let initialPromoChips = settings.PROMO_CHIPS;
            let initialUserBalance = settings.USER_BALANCE;

            if(!settings.FSV_STATUS)
                if (settings.REAL_CHIP_FIRST) {
                    /**
                     * 1. reduce to 0 USER_BALANCE
                     * 2. reduce PROMO_CHIPS with difference between USER_BALANCE AND STAKE
                     * */
                    setSettings.USER_BALANCE = initialUserBalance - (initialUserBalance - totalBet >= 0 ? totalBet : initialUserBalance);
                    setSettings.PROMO_CHIPS = initialPromoChips - (settings.USER_BALANCE > 0 ? 0 : totalBet - initialUserBalance);
                } else {
                    /**
                     * 1. reduce to 0 PROMO_CHIPS balance
                     * 2. reduce USER_BALANCE with difference between PROMO_CHIPS AND STAKE
                     * */

                    setSettings.PROMO_CHIPS = initialPromoChips - (initialPromoChips - totalBet >= 0 ? totalBet : initialPromoChips);
                    setSettings.USER_BALANCE = initialUserBalance - (settings.PROMO_CHIPS > 0 ? 0 : totalBet - initialPromoChips);
                }

            if (parseInt(settings.TOURNAMENT_RANK_TYPE) === 1)
                setSettings.RANK_USER_BALANCE -= (settings.MIN_BET * settings.START_GAME_PAYLINES);


            return resolve({status: true, balance: settings.USER_BALANCE});
        })
    }
}

