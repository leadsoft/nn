let footerCoordinates = {
    'info_btn': {
        'real_mode': {
            'desktop': {
                x: 159,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 160
                },
                'landscape': {
                    x: 210,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 159,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 160
                },
                'landscape': {
                    x: 210,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 159,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 160
                },
                'landscape': {
                    x: 210,
                    y: 0
                }
            }
        }
    },
    'sound_btn': {
        'real_mode': {
            'desktop': {
                x: 159,
                y: 35
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 159,
                y: 35
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 159,
                y: 35
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        }
    },
    'spin_btn': {
        'real_mode': {
            'desktop': {
                x: 805,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: -20,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 805,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: -20,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 805,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        }
    },
    'fast_spin_btn': {
        'real_mode': {
            'desktop': {
                x: 875,
                y: 32
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 80
                },
                'landscape': {
                    x: 105,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 875,
                y: 32
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 80
                },
                'landscape': {
                    x: 105,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 875,
                y: 32
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 80
                },
                'landscape': {
                    x: 105,
                    y: 0
                }
            }
        }
    },
    'auto_spin_btn': {
        'real_mode': {
            'desktop': {
                x: 875,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 875,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 875,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        }
    },
    'home_btn': {
        'real_mode': {
            'desktop': {
                x: 5,
                y: 499
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 5,
                y: 499
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 5,
                y: 499
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        }
    },
    'bet_per_line_container': {
        'real_mode': {
            'desktop': {
                x: 513,
                y: 30
            },
            'mobile': {
                x: 45,
                y: 30
            }
        },
        'demo_mode': {
            'desktop': {
                x: 513,
                y: 30
            },
            'mobile': {
                x: 45,
                y: 30
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 513,
                y: 30
            },
            'mobile': {
                x: 45,
                y: 30
            }
        }
    },
    'win_container': {
        'real_mode': {
            'desktop': {
                x: 636,
                y: 0
            },

            'mobile': {
                'portrait': {
                    x: 670,
                    y: 0
                },
                'landscape': {
                    x: 725,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 636,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 687,
                    y: 0
                },
                'landscape': {
                    x: 725,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 636,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 687,
                    y: -95
                },
                'landscape': {
                    x: 725,
                    y: 0
                }
            }
        }
    },
    'bet_container': {
        'real_mode': {
            'desktop': {
                x: 500,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 565,
                    y: 0
                },
                'landscape': {
                    x: 530,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 500,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 565,
                    y: 0
                },
                'landscape': {
                    x: 530,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 500,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 565,
                    y: 0
                },
                'landscape': {
                    x: 530,
                    y: 0
                }
            }
        }
    },
    'balance_container': {
        'real_mode': {
            'desktop': {
                x: 279,
                y: 0
            },

            'mobile': {
                'portrait': {
                    x: 15,
                    y: 0
                },
                'landscape': {
                    x: 130,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 279,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 15,
                    y: 0
                },
                'landscape': {
                    x: 130,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 279,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 15,
                    y: 0
                },
                'landscape': {
                    x: 130,
                    y: 0
                }
            }
        }
    },
    'total_balance_container': {
        'real_mode': {
            'desktop': {
                x: 636,
                y: 0
            },

            'mobile': {
                'portrait': {
                    x: 155,
                    y: 0
                },
                'landscape': {
                    x: 750,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 636,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 155,
                    y: 0
                },
                'landscape': {
                    x: 750,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 636,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 155,
                    y: 0
                },
                'landscape': {
                    x: 750,
                    y: 0
                }
            }
        }
    },
    'bonus_container': {
        'real_mode': {
            'desktop': {
                x: 279,
                y: 61
            },
            'mobile': {
                'portrait': {
                    x: 388,
                    y: 0
                },
                'landscape': {
                    x: 330,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 279,
                y: 61
            },
            'mobile': {
                'portrait': {
                    x: 388,
                    y: 0
                },
                'landscape': {
                    x: 330,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 279,
                y: 30
            },
            'mobile': {
                'portrait': {
                    x: 388,
                    y: 0
                },
                'landscape': {
                    x: 330,
                    y: 0
                }
            }
        }
    },
    'bets_to_qualify_container': {
        'real_mode': {
            'desktop': {
                x: 585,
                y: 30
            },
            'mobile': {
                x: 685,
                y: 30
            }
        },
        'demo_mode': {
            'desktop': {
                x: 585,
                y: 30
            },
            'mobile': {
                x: 685,
                y: 30
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 585,
                y: 30
            },
            'mobile': {
                x: 685,
                y: 30
            }
        }
    },
    'bets_made_container': {
        'real_mode': {
            'desktop': {
                x: 800,
                y: 30
            },
            'mobile': {
                x: 890,
                y: 30
            }
        },
        'demo_mode': {
            'desktop': {
                x: 800,
                y: 30
            },
            'mobile': {
                x: 890,
                y: 30
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 800,
                y: 30
            },
            'mobile': {
                x: 890,
                y: 30
            }
        }
    },
    'rank_balance_container': {
        'real_mode': {
            'desktop': {
                x: 650,
                y: 30
            },
            'mobile': {
                'portrait': {
                    x: 690,
                    y: 0
                },
                'landscape': {
                    x: 690,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 650,
                y: 30
            },
            'mobile': {
                'portrait': {
                    x: 690,
                    y: 0
                },
                'landscape': {
                    x: 690,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 650,
                y: 30
            },
            'mobile': {
                'portrait': {
                    x: 690,
                    y: 0
                },
                'landscape': {
                    x: 690,
                    y: 0
                }
            }
        }
    },
    'time_left_container': {
        'real_mode': {
            'desktop': {
                x: 525,
                y: 30
            },
            'mobile': {
                x: 523,
                y: 30
            }
        },
        'demo_mode': {
            'desktop': {
                x: 525,
                y: 30
            },
            'mobile': {
                x: 523,
                y: 30
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 525,
                y: 30
            },
            'mobile': {
                x: 523,
                y: 30
            }
        }
    },
    'rank_container': {
        'real_mode': {
            'desktop': {
                x: 812,
                y: 30
            },
            'mobile': {
                x: 890,
                y: 30
            }
        },
        'demo_mode': {
            'desktop': {
                x: 812,
                y: 30
            },
            'mobile': {
                x: 890,
                y: 30
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 812,
                y: 30
            },
            'mobile': {
                x: 890,
                y: 30
            }
        }
    },
    'lines_bet_container': {
        'real_mode': {
            'desktop': {
                x: 196,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 65,
                    y: 0
                },
                'landscape': {
                    x: 5,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 196,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 65,
                    y: 0
                },
                'landscape': {
                    x: 5,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 196,
                y: 0
            },
            'mobile': {
                'portrait': {
                    x: 65,
                    y: 0
                },
                'landscape': {
                    x: 5,
                    y: 0
                }
            }
        }
    },
    'plus_btn': {
        'real_mode': {
            'desktop': {
                x: 875,
                y: 16
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 875,
                y: 16
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 875,
                y: 16
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 0,
                    y: 0
                }
            }
        }
    },
    'minus_btn': {
        'real_mode': {
            'desktop': {
                x: 875,
                y: 16
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 160
                },
                'landscape': {
                    x: 210,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 875,
                y: 16
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 160
                },
                'landscape': {
                    x: 210,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 875,
                y: 16
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 160
                },
                'landscape': {
                    x: 210,
                    y: 0
                }
            }
        }
    },

    'amount_per_line': {
        'real_mode': {
            'desktop': {
                x: 875,
                y: 16
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 115,
                    y: 0
                }
            }
        },
        'demo_mode': {
            'desktop': {
                x: 875,
                y: 16
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 115,
                    y: 0
                }
            }
        },
        'tournament_mode': {
            'desktop': {
                x: 875,
                y: 16
            },
            'mobile': {
                'portrait': {
                    x: 0,
                    y: 0
                },
                'landscape': {
                    x: 115,
                    y: 0
                }
            }
        }
    },
};
