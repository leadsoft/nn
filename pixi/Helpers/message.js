let messages = {};
messages['total_win_bonus'] = "TOTAL WIN:"
messages['insufficient_funds'] = "You have insufficient balance for this wager.<br>Click CLOSE, then lower your wager and try again."
messages['real_mode_insufficient_funds'] = "Insufficient credit balance. <br><br>Please reduce the bet or deposit additional credits."
messages['7004'] = "Insufficient funds to enter tournament."
messages['7095'] = "Rebuy limit exceeded."
messages['7098'] = "Maximum number of entrants has been reached."
messages['7501'] = "Another game has been started in the tournament."
messages['7503'] = "Insufficient funds to rebuy the tournament."
messages['7508'] = "Balance too high to qualify for re-buy."
messages['7001'] = "Balance too high to qualify for re-buy."
messages['7003'] = "Tournament does not exist or player not registered."
messages['7005'] = "Player has no Player Name."
messages['7020'] = "Tournament has not started yet."
messages['7021'] = "Tournament has finished."
messages['7505'] = "Tournament has finished."
messages['7097'] = "Tournament has already started.."
messages['7099'] = "Registration closed."
messages['7502'] = "Rebuy option is not available at this time."
messages['7504'] = "Rebuy option is not available at this time."
messages['7506'] = "You are not permitted to buy-In to free roll tournaments at this time. Please contact customer service."
messages['7507'] = "You are not permitted to buy-In to tournaments at this time.  Please contact customer service."
messages['1003'] = "No session can be found.  Please login first."
function buildMessage() {
    messages['rebuy_insufficient_funds'] = "You have insufficient balance for this wager. Select \nRebuy or lower your wager to continue";
    messages['rebuy_default'] = "Would you like buy-in to tournament #<span style='color: #FFF; background-color: transparent'>" + settings.TOURNAMENT_ID + "</span> - " + settings.TOURNAMENT_NAME + "?. Your balance will be debited <span style='color: #FFF; background-color: transparent'>" + user.currencySymbol + settings.TOURNAMENT_REBUY_FEE + "</span>\n.Good Luck";
    messages['rebuy_exchange'] = "Would you like to Rebuy into tournament <span style='color: #FFFF00'>" + settings.TOURNAMENT_NAME + "</span>?\nIf Yes, your balance will be debited |@string (1 @ @value)#FFFF00^."
}
