class Tick {
    constructor() {
        let self = this;
        self.from = 1;
        self.to = 3;
        self.initialValue = 0;
        self.totalValue = 0;
        self.remainValue = 0;
        self.incrementValue = 0;
        self.animation;
        self.type = 'normal-win';
        self.playTick = true;
        app.ticker.add((delta) => {
            if (self.remainValue > 0 /*&& !self.stopTick*/) {
                if (settings.SPIN_STATUS/* || !this.playTick*/ || settings.STOP_PAYLINE_ANIMATION) {
                    self.incrementValue = self.remainValue;
                } else if (self.remainValue >= 800) {
                    self.incrementValue = 100;
                } else if (self.remainValue >= 500 && self.remainValue <= 800) {
                    self.incrementValue = 50;
                } else if (self.remainValue >= 100 && self.remainValue <= 500) {
                    self.incrementValue = 10;
                } else if (self.remainValue >= 50 && self.remainValue < 100) {
                    self.incrementValue = 5;
                } else if (self.remainValue < 50 && self.remainValue >= 30) {
                    self.incrementValue = 3;
                } else if (self.remainValue < 30 && self.remainValue >= 10) {
                    self.incrementValue = 2;
                } else if (self.remainValue < 10 && self.remainValue >= 1) {
                    self.incrementValue = 0.5;
                } else if (self.remainValue < 1 && self.remainValue > 0.2) {
                    self.incrementValue = 0.1;
                } else if (self.remainValue < 0.2 && self.remainValue > 0) {
                    self.incrementValue = 0.01;
                }

                self.remainValue = parseFloat(self.remainValue).toFixed(2) - parseFloat(self.incrementValue).toFixed(2);

                let additionlText = self.type === 'bonus' ? messages.total_win_bonus : '';
                if (self.from !== null)
                    self.from.text = additionlText + ' ' + user.currencySymbol + parseFloat(self.remainValue).toFixed(2);
                self.totalValue = self.totalValue + self.incrementValue;

                if (!settings.GAME_RUNNING)
                    self.to.text = user.currencySymbol + formatMoney(parseFloat(self.totalValue).toFixed(2));

                if (self.remainValue === 0) {
                    setSettings.TICK_STATUS = false;
                    let currentFrame = self.animation.currentFrame;
                    let animationFrame = self.animation.totalFrames - currentFrame;
                    for (let i = 0; i < animationFrame; i++) {
                        self.animation.gotoAndStop(currentFrame + i);

                        if (animationFrame === i + 1 /*&& !this.playTick*/) {
                            if (gameContainer.getChildByName('coin-animation') !== null)
                                gameContainer.removeChildAt(gameContainer.getChildIndex(gameContainer.getChildByName('coin-animation')));
                            /*gameCompleted.winText.text = '';*/
                            //gameCompleted.winText.children = [];
                            oAL.winLine.visible = false;
                        }
                    }
                }

                if (isMobile() && !isLandscape())
                    gameCompleted.winText.x = (GAME_WIDTH / 2) + (gameCompleted.winText.width / 8); //(gameContainer.width / gameContainer.scale.x / 1) - (gameCompleted.winText.width / 2);
                else
                    gameCompleted.winText.x = (GAME_WIDTH / 2) - (gameCompleted.winText.width / 2);

                gameCompleted.winText.y = game.reelContainer.y + ((SYMBOL_SIZE * 3 + (3 * SYMBOL_MARGIN_TOP)) / 2 - (gameCompleted.winText.height / 2));

                /*footer.winAmount.x = (footer.winAmount.parent.width / 2) - (footer.winAmount.width / 2);
                footer.winAmount.y = (footer.winAmount.parent.height / 2) - (footer.winAmount.height / 2);*/
                if (settings.SWITCH_LINES_BET && !isMobile()) {

                    if(settings.FS_STATUS){
                        footer.winAmount.y = footer.winAmount.parent.getChildByName('win-text').y + footer.winAmount.parent.getChildByName('win-text').height;
                        footer.winAmount.x = footer.winAmount.parent.width / 2 - footer.winAmount.width / 1.3;
                    } else {
                        footer.winAmount.y = 15;
                        footer.winAmount.x = footer.winAmount.parent.width - footer.winAmount.width - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN//360;
                    }

                } else if (settings.SWITCH_LINES_BET && isMobile()) {
                    if (isLandscape()) {
                        footer.winAmount.x = footer.winAmount.parent.width / 2 - footer.winAmount.width / 2;
                    } else {
                        if(settings.FS_ENABLE_STATUS){
                            footer.betAmount.x = footer.betAmount.parent.width / 2 - footer.betAmount.width / 2;
                        } else {
                            footer.winAmount.x = footer.winAmount.parent.width / 2 - footer.winAmount.width / 2;
                        }
                    }

                    /*footer.winAmount.y = footer.winAmount.parent.height - footer.winAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;*/
                } else {
                    if (settings.DEVICE.includes('D')) {

                        footer.winAmount.y = footer.winAmount.parent.parent.getChildByName('bg-panel').height - footer.winAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
                        footer.winAmount.x = footer.winAmount.parent.width / 2 - footer.winAmount.width / 1.3;
                    } else {
                        if(isLandscape()){
                            footer.winAmount.y = footer.winAmount.parent.getChildByName('win-image').height - footer.winAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN - 8;
                        } else {
                            footer.winAmount.y = footer.winAmount.parent.getChildByName('win-image').height / 2 - footer.winAmount.height / 2;
                        }
                        if(settings.FS_ENABLE_STATUS){
                            footer.betAmount.x = footer.betAmount.parent.width / 2 - footer.betAmount.width / 2;
                        } else {
                            footer.winAmount.x = footer.winAmount.parent.width / 2 - footer.winAmount.width / 2;
                        }
                    }

                }

            } else if (gameContainer.getChildByName('coin-animation') !== null) {
                if (game.paylines.length === 0)
                    setSettings.TICK_STATUS = false;
                gameContainer.removeChildAt(gameContainer.getChildIndex(gameContainer.getChildByName('coin-animation')));
            }
        });
    }

    async startTick(value, from, to, type = 'normal-win', playAnimation = true) {
        //this.playTick = playAnimation;
        //await sleep(1000);
        if (settings.STOP_PAYLINE_ANIMATION)
            return;
        tickerStartBalance();
        setSettings.TICK_STATUS = true;
        await this.loadFrames(type);


        this.initialValue = 0;
        this.totalValue = 0;
        this.incrementValue = 0;
        let self = this;
        /*START Animation*/
        if (type === 'normal-win' && playAnimation) {
            self.animation.visible = true;
            self.animation.play();
        } else if (playAnimation) {
            /*game.tweenCoinAnim = TweenMax.to(self.animation, 0.3, {
                repeat: 30,
                x: (footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x + footer.winImage.width) - self.animation.width / 2 - 20,
                y: (GAME_HEIGHT - settings.FOOTER_HEIGHT) - footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y + 8,
            });*/
            self.animation.visible = true;
            self.animation.play();
        }

        this.type = type;
        this.from = from;
        this.to = to;
        this.remainValue = this.initialValue = value;
        this.incrementValue = this.remainValue / 10;
    }

    async loadFrames(type) {
        let self = this;
        return new Promise((resolve, reject) => {
            let isMobile = !settings.DEVICE.includes('D');
            /*LOAD resources*/

            self.animation = new PIXI.AnimatedSprite(game.animations['coin_fountain_animation'].animations['coin']);
            self.animation.animationSpeed = 0.350;
            self.animation.loop = true;
            self.animation.x = (footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x + footer.winImage.width / 2) - self.animation.width / 1.1;
            self.animation.y = 300;
            self.animation.zIndex = 20;
            self.animation.name = 'coin-animation';
            // self.animation.visible = false;
            gameContainer.addChild(self.animation);
            self.animation.play();


            return resolve(1);
        });
    }
}
