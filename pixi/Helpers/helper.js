function switchTitleTexture() {

    if (isMobile()) {
        // if (window.innerWidth > window.innerHeight) {
        //     if(settings.FS_STATUS){
        //         oAL.titleImg.texture = freeS.spritesheet.background.textures['freespins_title.png']
        //     } else {
        //         oAL.titleImg.texture = oAL.backgroundElements['title']
        //     }
        // } else {
        //     oAL.titleImg.texture = oAL.backgroundElements['portrait_title']
        // }

        /*setTimeout(() => {
            if (window.innerWidth > window.innerHeight) {
                titleContainer.x = GAME_WIDTH / 2 - titleContainer.width / 2
                oAL.titleImg.x = titleContainer.width / 2 - oAL.titleImg.width / 2
            } else {
                titleContainer.x = 0
                oAL.titleImg.x = GAME_WIDTH / 2 - oAL.titleImg.width / 2;
            }
        }, 100)*/
    }
}

function prepareLayoutForDevice() {

    oAL.backgroundImg.visible = (isMobile() && !isLandscape());
    oAL.backgroundImg.width = window.innerWidth;
    oAL.backgroundImg.height = window.innerHeight;

    titleContainer.visible = true//!(isMobile() && !isLandscape());

    freeS.dialogContainer.y = isMobile() && !isLandscape() ? -160 : 0;

    if (isMobile() && !isLandscape()) {
        game.reelBackground.y = freeS.dialogContainer.visible && settings.FS_STATUS ? -25 : 100;
    }

    if (isMobile()) {
        if (isLandscape()) {
            // oAL.reelDividerImg.visible = !settings.FS_STATUS;
            // oAL.gameBackgroundImg.visible = true;
            // game.reelBackground.y = 100;
            // oAL.reelDividerImg.y = 0
            game.reelBackground.visible = false;
        } else {
            // oAL.reelDividerImg.visible = !settings.FS_STATUS;
            // oAL.reelDividerImg.y = -120
            // oAL.gameBackgroundImg.visible = false;
            // game.reelBackground.y = -25;
            game.reelBackground.visible = false;
        }
    }
}

function repositionGameContainer() {
    game.fakeReelContainer.y = freeS.reelContainer.y = game.reelContainer.y = isMobile && !isLandscape() ? 0 : REEL_CONTAINER_MARGIN_TOP
    game.fakeReelContainer.x = freeS.reelContainer.x = game.reelContainer.x = (function () {
        let position = (GAME_WIDTH / 2) - ((SYMBOL_CONTAINER_WIDTH * settings.REEL_NUMBER) / 2)
        if (isMobile && !isLandscape()) {

            position = gameContainer.width / 2 - game.reelContainer.width * gameContainer.scale.x / 2
            position = 0
        }
        return position;
    }());
    // gameContainer.x = window.innerWidth / 2 - gameContainer.width / 2;
    gameContainer.x = (function () {
        let position = (GAME_WIDTH / 2) - ((SYMBOL_CONTAINER_WIDTH * settings.REEL_NUMBER) / 2)
        if (isMobile) {
            if (!isLandscape()) {
                position = (GAME_WIDTH / 2) - ((SYMBOL_CONTAINER_WIDTH * settings.REEL_NUMBER) / 3.1)
            } else {
                renderableFakeReel(false);
                position = window.innerWidth / 2 - gameContainer.width / 2;
                renderableFakeReel(true);
            }
        }
        return position;
    }());

    gameContainer.y = (function () {
        let position = 0
        if (isMobile) {
            if (!isLandscape()) {
                // position = //52//titleContainer.y + titleContainer.height + 20 //NOTE: that hardcoded value should be removed
                // position = titleContainer.y + titleContainer.height + 20;
                position = titleContainer.height + titleContainer.height/2.3
            } /*else {
                position = window.innerWidth / 2 - gameContainer.width / 2
            }*/
        }
        return position;
    }());

    return true;
    /*game.fakeReelContainer.x = freeS.reelContainer.x = game.reelContainer.x = (function () {
        let position = (GAME_WIDTH / 2) - ((SYMBOL_CONTAINER_WIDTH * settings.REEL_NUMBER) / 2)
        if (isMobile && !isLandscape()){
            console.log(game.reelContainer.width, 'game.reelContainer');
            position = window.innerWidth / 2 - gameContainer.width / 2//(GAME_WIDTH / 2) -  (game.reelContainer.width * gameContainer.scale.x) / 2;
        }


        return position;
    }());*/
}

function repositionRespinTitle() {
    if (isMobile() && !isLandscape()) {
        titleContainer.x = window.innerWidth / 2 - titleContainer.width / 2;

        if(oAL.respinTextTitle) {
            oAL.respinTextTitle.x = GAME_WIDTH - (oAL.respinTextTitle.width * 1.1)
            oAL.respinTextTitle.y = -GAME_HEIGHT_PORTRAIT / 7;
        }
    } else {
        titleContainer.x = window.innerWidth / 2 - titleContainer.width / 2;

        if(oAL.respinTextTitle) {
            oAL.respinTextTitle.x = GAME_WIDTH / 2 - oAL.respinTextTitle.width / 2;
            oAL.respinTextTitle.y = 0//GAME_HEIGHT / 6 + oAL.respinTextTitle.height / 6;
        }
    }
}

function resetSideIndicators() {

    if (typeof gameCompleted.containerLineIndicators !== "undefined" && typeof oAL.sideIndicatorsImg !== "undefined")
        gameCompleted.containerLineIndicators.y = oAL.sideIndicatorsImg.y = (isMobile() && !isLandscape()) ? -95 : 18

    if (typeof game.anticipation !== "undefined")
        game.anticipation.forEach(function (val, key) {
            if (isMobile())
                if (isLandscape())
                    val.y = 90;
                else
                    val.y = -20
        });

    if (typeof oAL.rightSideIndicatorNumbersImg !== 'undefined')
        oAL.rightSideIndicatorNumbersImg.x = isMobile() && !isLandscape() ? 965/*GAME_WIDTH_PORTRAIT - 100*/ : GAME_WIDTH - 100;
}

function addGraphic() {
    var appGraphics = new PIXI.Sprite(oAL.panel['app_graphic']);
    appGraphics.name = 'app-graphic';
    appGraphics.zIndex = 0;
    appGraphics.width = window.innerWidth;
    appGraphics.height = window.innerHeight;
    app.stage.addChild(appGraphics)
}

function renderableFakeReel(value) {
     let val = game.fakeReelContainer.children.length;

    for (let i = 0; i < val; i++) {
        if (game.fakeReelContainer.children[i].name !== "symbol-h") {
            game.fakeReelContainer.getChildAt(i).renderable = value;
        }
    }
}