function loadActionContainerTextures(texturePack) {
    oAL.panel['menu'] = {
        'menu_mask_1': {
            'mobile': {
                'portrait': texturePack.textures['mobile/portrait/realDemo/menu1Mask.png.png'],
                'landscape': texturePack.textures['mobile/landscape/realDemo/menu1Mask.png.png']
            },
            'desktop': '',
        },
        'menu_mask_2': {
            'mobile': {
                'portrait': texturePack.textures['mobile/portrait/realDemo/menu2Mask.png'],
                'landscape': texturePack.textures['mobile/landscape/realDemo/menu2Mask.png']
            },
            'desktop': '',
        },
        'normal': {
            'mobile': texturePack.textures['mobile/sharedButtons/menu_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/menu_n.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/sharedButtons/menu_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/menu_d.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/menu_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/menu_h.png'],
        },
        'close_normal': {
            'mobile': texturePack.textures['mobile/sharedButtons/menu_close_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/menu_close_n.png'],
        },
        'close_hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/menu_close_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/menu_close_h.png'],
        },
        'info_bar': {
            'mobile': {
                'portrait': texturePack.textures['mobile/portrait/baseGame/menu_mask_2.png'],
                'landscape': texturePack.textures['mobile/landscape/baseGame/menu_mask_2.png']
            },
            'desktop': texturePack.textures['desktop/baseGame/menu_close_h.png'],
        }
    };

    oAL.panel['coins_menu'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/sharedButtons/coins_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/coins_n.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/sharedButtons/coins_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/coins_d.png'],
        },
        'close_normal': {
            'mobile': texturePack.textures['mobile/sharedButtons/coins_close_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/coins_close_n.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/coins_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/coins_h.png'],
        }
    };

    oAL.panel['spin'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/sharedButtons/spin_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/spin_n.png'],
        },
        'stop': {
            'mobile': texturePack.textures['mobile/sharedButtons/stopSpin_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/stop_lg_n.png'],
        },
        'stop_hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/stop_lg_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/stop_lg_h.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/sharedButtons/spin_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/spin_d.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/spin_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/spin_h.png'],
        }
    };

    oAL.panel['autospin'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/sharedButtons/autoplay_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/autoplay_n.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/sharedButtons/autoplay_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/autoplay_d.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/autoplay_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/autoplay_h.png'],
        },
        'cancel': {
            'mobile': texturePack.textures['mobile/sharedButtons/cancelAutoplay_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_n.png'],
        },
        'cancel_hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/cancelAutoplay_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_h.png'],
        },
        'disabled_cancel': {
            'mobile': texturePack.textures['mobile/sharedButtons/cancelAutoplay_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_d.png'],
        },
    };

    oAL.panel['home'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/sharedButtons/home_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/home_n.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/sharedButtons/home_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/home_d.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/home_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/home_h.png'],
        },
        'cancel': {
            'mobile': texturePack.textures['mobile/sharedButtons/cancelAutoplay_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_n.png'],
        },
        'cancel_hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/cancelAutoplay_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_h.png'],
        },
        'disabled_cancel': {
            'mobile': texturePack.textures['mobile/sharedButtons/cancelAutoplay_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/cancelAutoplay_d.png'],
        },
    };

    oAL.panel['fastspin'] = {
        'normal': {
            'mobile': texturePack.textures['mobile/sharedButtons/normalSpin_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/normalSpin_n.png'],
        },
        'disabled': {
            'mobile': texturePack.textures['mobile/sharedButtons/normalSpin_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/normalSpin_d.png'],
        },
        'hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/normalSpin_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/normalSpin_h.png'],
        },
        'quick': {
            'mobile': texturePack.textures['mobile/sharedbuttons/quickspin_n.png'],
            'desktop': texturePack.textures['desktop/baseGame/quickspin_n.png'],
        },
        'quick_hover': {
            'mobile': texturePack.textures['mobile/sharedButtons/quickspin_h.png'],
            'desktop': texturePack.textures['desktop/baseGame/quickspin_h.png'],
        },
        'quick_disabled': {
            'mobile': texturePack.textures['mobile/sharedButtons/quickspin_d.png'],
            'desktop': texturePack.textures['desktop/baseGame/quickspin_d.png'],
        },
    };
}

function setActionContainerTextures() {
    if (!settings.GAME_START)
        return;

    let isMobile = !settings.DEVICE.includes('D');
    let key = 'desktop';
    if (isMobile)
        key = 'mobile';


    if (isMobile) {
        footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][key];
        footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][key];
        footer.mobileInfoBar.texture = oAL.panel['menu']['menu_mask_2'][key][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait']; /*TODO: on desktop we need regenerate json*/
        footer.sliderBar.texture = oAL.panel['menu']['menu_mask_2'][key][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait']; /*TODO: on desktop we need regenerate json*/
    }

    footer.spinButtonSprite.texture = oAL.panel['spin']['normal'][key];
    footer.autoSpinButton.texture = oAL.panel['autospin']['normal'][key];
    footer.homeBtn.texture = oAL.panel['home']['normal'][key];
    footer.darkSpinButtonSprite.texture = oAL.panel['spin']['disabled'][key];//new PIXI.Sprite(oAL.panel.spin_disabled);
    footer.stopSpinButtonSprite.texture = oAL.panel['spin']['stop'][key];//new PIXI.Sprite(oAL.panel.stop_spin);
    footer.fastSpinButton.texture = oAL.panel['fastspin']['normal'][key]
}

function setActionContainerPosition() {
    if (!settings.GAME_START)
        return;
    let isMobile = !settings.DEVICE.includes('D');
    /**SPIN*/
    footer.stopSpinButtonSprite.x = footer.darkSpinButtonSprite.x = footer.spinButtonSprite.x = (function () {
        let position = footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile) {
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = GAME_WIDTH / 2 - footer.spinButtonSprite.width / 2;
            }
        }

        return position;
    }());

    if (isMobile) {
        footer.mobileMenu.y = (function () {
            let position = window.innerWidth > window.innerHeight ? 100 : 0;
            return position;
        }());

        footer.mobileMenu.x = (function () {
            let position = 0;
            if (isLandscape())
                position = 0;
            else
                position = GAME_WIDTH - footer.mobileMenu.width - settings.FOOTER_CONTAINER_MARGIN - 65;

            return position;
        }());

        footer.coinMenuContainer.y = (function () {
            let position = 0;
            if (window.innerWidth > window.innerHeight) {
                position = footer.mobileMenu.y + footer.mobileMenu.height + settings.ACTION_CONTAINER_BUTTONS_MARGIN;
            }
            return position;
        }());

        footer.coinMenuContainer.x = (function () {
            let position = 0
            if (isMobile)
                if (!isLandscape()) {
                    /*position = footer.mobileMenu.x - footer.mobileMenu.width - settings.ACTION_CONTAINER_MARGIN*/
                    position = (footer.mobileMenu.x - footer.mobileMenu.width) - (footer.spinButtonSprite.x + footer.spinButtonSprite.width) + GAME_WIDTH / 2
                } else {
                    position = 0//
                }
            return position;
        }());
    }

    footer.stopSpinButtonSprite.y = footer.darkSpinButtonSprite.y = footer.spinButtonSprite.y = (function () {
        let position = footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y + 2;
        if (isMobile)
            position = window.innerWidth > window.innerHeight ? GAME_HEIGHT / 2 - footer.spinButtonSprite.height / 2 : -footer.spinButtonSprite.height;//footerCoordinates.spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());
    /**END SPIN*/

    footer.autoSpinButton.x = (function () {
        let position = footerCoordinates.auto_spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile) {
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.auto_spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = (footer.spinButtonSprite.x - footer.homeBtn.width) / 2 - footer.autoSpinButton.width / 2 + settings.ACTION_CONTAINER_MARGIN + 10
            }
        }

        return position;
    }());
    footer.autoSpinButton.y = (function () {
        let position = footerCoordinates.auto_spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y + 2;
        if (isMobile)
            position = window.innerWidth > window.innerHeight ? footer.spinButtonSprite.y + footer.spinButtonSprite.height + settings.ACTION_CONTAINER_BUTTONS_MARGIN : 0;

        return position;
    }());

    footer.homeBtn.x = (function () {
        let position = 0//footerCoordinates.home_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        if (isMobile) {
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.home_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = 70//settings.ACTION_CONTAINER_MARGIN;
            }
        }

        return position;
    }());
    footer.homeBtn.y = (function () {
        let position = 0//footerCoordinates.home_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            position = window.innerWidth > window.innerHeight ? footer.autoSpinButton.y + footer.autoSpinButton.height + settings.ACTION_CONTAINER_BUTTONS_MARGIN : 0;

        return position;
    }());

    footer.amountPerLine.x = (function () {
        let position = 0//footerCoordinates.amount_per_line[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        if (isMobile)
            if (isLandscape())
                position = footerCoordinates.amount_per_line[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            else{
                position = footer.amountPerLine.parent.width / 2 - footer.amountPerLine.width / 2;
            }


        return position;
    }());
    footer.amountPerLine.y = (function () {
        let position = 0//footerCoordinates.amount_per_line[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile) {
            position = isLandscape() ? footer.amountPerLine.parent.height / 2 - (footer.amountPerLine.height / 2) : 100;
        }
        return position;
    }());

    footer.infoBtn.x = (function () {
        let position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile) {
            if (isLandscape()) {
                position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['mobile']['landscape'].x;
            } else {
                position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['mobile']['portrait'].x;
            }
        }
        return position;
    }());
    footer.infoBtn.y = (function () {
        let position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y + 2;
        if (isMobile)
            if (isLandscape()) {
                position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['mobile']['landscape'].y;
            } else {
                position = footerCoordinates.info_btn[settings.GAME_PLAY_MODE + '_mode']['mobile']['portrait'].y;
            }

        return position;
    }());

    footer.soundButton.x = (function () {
        let position = footerCoordinates.sound_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            position = footerCoordinates.sound_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;

        return position;
    }());
    footer.soundButton.y = (function () {
        let position = footerCoordinates.sound_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y + 2;
        if (isMobile)
            position = footerCoordinates.sound_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    footer.fastSpinButton.x = (function () {
        let position = footerCoordinates.fast_spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile) {
            if (!isLandscape()) {
                position = footerCoordinates.fast_spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop']['portrait'].x;
            } else {
                position = footerCoordinates.fast_spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            }
        }


        return position;
    }());
    footer.fastSpinButton.y = (function () {
        let position = footerCoordinates.fast_spin_btn[settings.GAME_PLAY_MODE + '_mode']['desktop'].y;
        if (isMobile)
            position = footerCoordinates.fast_spin_btn[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    if (isMobile) {
        footer.mobileCoinPlus.x = (function () {
            return footerCoordinates.plus_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
        }());
        footer.mobileCoinPlus.y = (function () {
            return footerCoordinates.plus_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;
        }());

        footer.mobileCoinMinus.x = (function () {
            return footerCoordinates.minus_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x
        }());
        footer.mobileCoinMinus.y = (function () {
            return footerCoordinates.minus_btn[settings.GAME_PLAY_MODE + '_mode']['mobile'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;
        }());
    }

    resizeActionContainerText();
}

function repositionActionContainer() {
    if (isMobile()) {
        if (!isLandscape())
            actionContainer.y = window.innerHeight - 120
        /*actionContainer.y = footerContainer.y + footerContainer.height + 30*/
    }
}

function resizeActionContainerText() {
    footer.amountPerLine.x = (function () {
        let position = (footer.amountPerLine.parent.width / 2) - (footer.amountPerLine.width / 2);
        if (isMobile()) {
            if (isLandscape())
                position = (footer.mobileCoinMinus.x + footer.mobileCoinMinus.width - footer.mobileCoinPlus.x) / 2 - footer.amountPerLine.width / 2;
            else
                position = 0
        }

        return position;
    }());
    footer.amountPerLine.y = (function () {
        let position = (footer.amountPerLine.parent.height / 2) - (footer.amountPerLine.height / 2);
        if (isMobile()) {
            if (isLandscape())
                position = (footer.amountPerLine.parent.height / 2) - (footer.amountPerLine.height / 2);
            else
                position = (footer.mobileCoinMinus.y + footer.mobileCoinMinus.height - footer.mobileCoinPlus.y) / 2 - footer.amountPerLine.height / 2
        }

        return position;
    }());
}

/**
 * ACTIONS
 * */
var menuActions = {
    pointerover() {
        footer.mobileMenuBtn.texture = oAL.panel['menu']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
    },
    pointerout() {
        footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
    },
    pointerdown() {
        if(settings.FS_STATUS)
            return;
        coinMenuBtnActions.closeSliderBar()
        if (typeof footer.mobileMenu !== "undefined" && footer.mobileMenu.status) {
            footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.mobileMenu.status = false;
        } else {
            footer.mobileMenuBtn.texture = oAL.panel['menu']['close_normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.mobileMenu.status = true;
        }
        this.openCloseInfoBar();

    },
    openCloseInfoBar(changeView = false) {
        if (!settings.GAME_START)
            return;

        if (changeView) {
            footer.mobileInfoBar.visible = false;
            footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.mobileMenu.status = false;
        } else {
            footer.mobileInfoBar.visible = true;
        }
        if (isLandscape()) {
            footer.mobileInfoBar.y = 0;
            TweenMax.to(footer.mobileInfoBar, 0.5, {
                x: footer.mobileMenu.status && !changeView ? -footer.mobileInfoBar.width + footer.mobileMenuBtn.width - (footer.mobileMenuBtn.width / 2) : actionContainer.width
            });
        } else {
            footer.mobileInfoBar.x = 0;
            TweenMax.to(footer.mobileInfoBar, 0.5, {
                y: footer.mobileMenu.status && !changeView ? -footer.mobileInfoBar.height + footer.mobileMenuBtn.height : actionContainer.height
            });
        }
    },

    closeInfoBar() {
        if(typeof footer.mobileMenuBtn === "undefined")
            return;
        footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
        footer.mobileMenu.status = false;

        if (isLandscape()) {
            footer.mobileInfoBar.y = 0;
            TweenMax.to(footer.mobileInfoBar, 0.5, {
                x: actionContainer.width
            });
        } else {
            footer.mobileInfoBar.x = 0;
            TweenMax.to(footer.mobileInfoBar, 0.5, {
                y: actionContainer.height
            });
        }
    }
};
var coinMenuBtnActions = {
    pointerover() {
        footer.coinMenuBtn.texture = oAL.panel['coins_menu']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
    },
    pointerout() {
        footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
    },
    pointerdown() {
        if(settings.FS_STATUS)
            return;
        if (footer.coinMenuContainer.status) {
            footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.coinMenuContainer.status = false;
        } else {
            footer.coinMenuBtn.texture = oAL.panel['coins_menu']['close_normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.coinMenuContainer.status = true;
        }

        this.openCloseSliderBar();
    },
    openCloseSliderBar(changeView = false) {
        if (!settings.GAME_START)
            return;

        menuActions.closeInfoBar()
        if (changeView) {
            footer.sliderBar.visible = false;
            footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
            footer.coinMenuContainer.status = false;
        } else {
            footer.sliderBar.visible = true;
        }
        if (isLandscape()) {
            footer.sliderBar.y = 0;
            TweenMax.to(footer.sliderBar, 0.5, {
                x: footer.coinMenuContainer.status && !changeView ? -footer.sliderBar.width + footer.coinMenuBtn.width - (footer.coinMenuBtn.width / 2) : actionContainer.width,
            });
        } else {
            footer.amountPerLine.x = footer.amountPerLine.parent.width / 2 - footer.amountPerLine.width / 2 ;
            footer.sliderBar.x = 0//footer.coinMenuContainer.x;
            TweenMax.to(footer.sliderBar, 0.5, {
                y: footer.coinMenuContainer.status && !changeView ? -footer.sliderBar.height + footer.coinMenuBtn.height : actionContainer.height + 50,
            });
        }
    },
    closeSliderBar() {
        if(typeof footer.coinMenuBtn === "undefined")
            return;
        footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
        footer.coinMenuContainer.status = false;
        if (isLandscape()) {
            footer.sliderBar.y = 0;
            TweenMax.to(footer.sliderBar, 0.5, {
                x: actionContainer.width,
            });
        } else {
            footer.sliderBar.x = footer.coinMenuContainer.x;
            TweenMax.to(footer.sliderBar, 0.5, {
                y: actionContainer.height,
            });
        }
    }
};
var spinActions = {
    pointerover() {
        footer.spinButtonSprite.texture = oAL.panel['spin']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
    },
    pointerout() {
        footer.spinButtonSprite.texture = oAL.panel['spin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
    },
    pointerdown() {
        if (footer.disableFooter)
            return;

        if(settings.FS_RUNNING_STATUS && settings.FS_COUNT === 0){
            return;
        }


        /*if (sound)
            button.play();*/
        setSettings.ACTIVITY = 8;

        if (isMobile) {
            footer.switchMobileCoinSelector(true)
        }
        if (footer.spinCounter === 0)
            goToFullScreen();
        footer.spinCounter++;
        spinH.activeSpinButton();
    }
};
var fastSpinActions = {
    pointerover() {
        if (footer.isPressedQuick) {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['quick_hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }
    },
    pointerout() {
        if (footer.isPressedQuick) {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['quick'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }
    },
    pointerdown() {
        if (footer.disableFooter) {
            return;
        }

        closeMobileMenuSliderContainer()
        setSettings.ACTIVITY = 8;

        if (sound)
            button.play()

        footer.isPressedQuick = !footer.isPressedQuick;
        if (footer.isPressedQuick) {
            spinH.fastSpin = true;
            footer.fastSpinButton.texture = oAL.panel['fastspin']['quick'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            spinH.fastSpin = false;
            footer.fastSpinButton.texture = oAL.panel['fastspin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']

        }
    }
};
var autoSpinActions = {
    pointerover() {
        if (footer.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['autospin']['cancel_hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
            /*self.autoSpinButton.texture = oAL.panel['cancel_autoplay_hover'];*/
        } else {
            footer.autoSpinButton.texture = oAL.panel['autospin']['hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
            /*self.autoSpinButton.texture = oAL.panel['autoplay_hover'];*/
        }

    },
    pointerout() {
        if (self.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['autospin']['cancel'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            footer.autoSpinButton.texture = oAL.panel['autospin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }
    },
    pointerdown() {
        if (footer.disableFooter) {
            return;
        }
        setSettings.ACTIVITY = 8;
        if (sound)
            button.play()

        footer.isPressedAutospin = !footer.isPressedAutospin;
        if (footer.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['cancel_autoplay']; //TODO: need to load this texture
            spinH.activeSpinButton();
            spinH.autospin = true;
        } else {
            footer.autoSpinButton.texture = oAL.panel['autoplay']; ////TODO: need to load this texture
            spinH.autospin = false;
        }
    }
};
var stopSpinActions = {
    pointerover() {
        footer.stopSpinButtonSprite.texture = oAL.panel['spin']['stop_hover'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
    },
    pointerout() {
        footer.stopSpinButtonSprite.texture = oAL.panel['spin']['stop'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];

    },
    pointerdown() {
        setSettings.STOP_REEL = true;
    }
}

function closeMobileMenuSliderContainer() {
    /* if (footer.mobileMenu !== undefined && footer.mobileMenu.status) {
         footer.mobileMenu.status = false;
         menuActions.openCloseInfoBar();
     }

     if (footer.coinMenuContainer !== undefined && footer.coinMenuContainer.status) {
         footer.coinMenuContainer.status = false;
         coinMenuBtnActions.openCloseSliderBar();
     }*/
}

