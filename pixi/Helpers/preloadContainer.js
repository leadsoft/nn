let preloaderTextures;
let preloaderTexture;
let preloaderTextureTitle;
let WGS = '';
let preloaderImg = new PIXI.Sprite(/*preloaderTexture*/);
preloaderImg.name = 'preload-img';
preloaderImg.sortableChildren = true
let preloaderImages = [];
let featureButton = [];

async function loadPreloadContainerTextures() {
    return new Promise((resolve) => {
        PIXI.loader
            .add('loadingBarBg', "newAssets/nn/dist/common/preloader/desktop/loading_bar_bg.png?ver=" + version)
            .add('loadingBar', 'newAssets/nn/dist/common/preloader/desktop/loading_bar.png?ver=' + version)
            .add('mobilePreloadBg', 'newAssets/nn/dist/basegame/mobile/portrait/preload/preloadLayout.png?ver=' + version)
            .add('mobilePreload', 'newAssets/nn/dist/common/panels/mobile/portrait/preload/preload.png?ver=' + version)
            .add('preload', 'newAssets/nn/dist/common/preloader/desktop/preloader.png?ver=' + version)
            .load(async function (loader, resources) {
                preloaderTextures = resources;

                if (isMobile() && !isLandscape()) {
                    /*preloaderTexture = await PIXI.Texture.from('newAssets/_movie magic/dist/common/panels/mobile/portrait/preload/preload_bg.png?ver=' + version);
                    preloaderTextureTitle = await PIXI.Texture.from('newAssets/_movie magic/dist/common/panels/mobile/portrait/preload/preload.png?ver=' + version);*/
                    preloaderTexture = resources['mobilePreloadBg'].texture;
                    preloaderTextureTitle = resources['mobilePreload'].texture
                } else {
                    preloaderTexture = resources['preload'].texture;
                }

                preloaderImages = {
                    'loadingBarBg': await PIXI.Sprite.from(resources['loadingBarBg'].texture),
                    'loadingBar': await PIXI.Sprite.from(resources['loadingBar'].texture)
                };

                return resolve(1);
            });
    })
}

function rotateDevice() {
    if (settings.PRELOAD_STATUS) {
        if (isMobile() && !isLandscape()) {
            preloaderTexture = preloaderTextures['mobilePreloadBg'].texture;
            preloaderTextureTitle = preloaderTextures['mobilePreload'].texture
        } else {
            preloaderTexture = preloaderTextures['preload'].texture;
        }
        setPreloadContainerTextures();
        loadBrand();
        positionLoadingBar()
    } else {
        buildFeaturePage()
    }
}

function buildFeaturePage() {
    preloaderContainer.children = [];
    let resources = app.loader.resources
    let featureBg = function () {
        /*mobile/portrait/feature/feature_image.png*/
        if (isMobile() && !isLandscape()){
            // let featurePage = new PIXI.Container();
            // let featurePageBg = new PIXI.Sprite(resources.panels.spritesheet.textures['mobile/portrait/feature/feature_bg.png']);
            // let featurePageTexture = new PIXI.Sprite(resources.panels.spritesheet.textures['mobile/portrait/feature/feature_image.png']);
            // featurePage.addChild(featurePageBg, featurePageTexture);
            // return featurePage;
            let featurePageTexture = resources['nn_basegame'].spritesheet.textures['mobile/portrait/feature/feature.png'];

            return new PIXI.Sprite(featurePageTexture);
        } else{
            // let featurePageTexture = PIXI.Texture.from('newAssets/nn/dist/feature.png');
            let featurePageTexture = resources['featurePage'].spritesheet.textures['featureImage.png'];
            return new PIXI.Sprite(featurePageTexture);
        }

    }();
    let featurePageContainer = new PIXI.Container();
    featurePageContainer.name = 'feature-container';

    /*let featureBg = new PIXI.Sprite(featurePageTexture);*/

    featureBg.name = 'feature-bg';
    oAL.loadingBar.visible = false;
    oAL.loadingBarProgressText.visible = false;
    featureBg.width = GAME_WIDTH;
    featureBg.x = 0;
    featureBg.y = 0;
    featureBg.height = GAME_HEIGHT;

    /*if (!(isMobile() && !isLandscape()))*/
        featurePageContainer.addChild(featureBg);


    /*if (isMobile() && !isLandscape())
        setPreloadContainerTextures()*/

    preloaderContainer.addChild(featurePageContainer);
    setPreloadContainerPosition();
    // if (!isMobile())
    wildFeature.play();

    featurePageContainer.addChild(featureButton);
    /*if (isMobile() && !isLandscape()) {
        featureButton.position.x = featureButton.parent.width / 2 - featureButton.width / 2;
    } else*/
        featureButton.position.x = GAME_WIDTH / 2 - featureButton.width / 2;

}

function buildFeatureButton() {
    let resources = app.loader.resources
    let featurePage = resources['featurePage'].spritesheet;
    featureButton = new PIXI.Container();
    featureButton.name = 'play-button';
    featureButton.visible = true;
    if(!isMobile()) {
        featureButton.position.y = 500;
    } else if(isMobile() && isLandscape()) {
        featureButton.position.y = 510;
    } else {
        featureButton.position.y = 850;
    }


    var graphics = new PIXI.Sprite(featurePage.textures['fpContinue_d.png']);
    graphics.name = 'button-graphic'
    featureButton.addChild(graphics);
    featureButton.buttonMode = true;
    featureButton.interactive = true;
    graphics.texture = featurePage.textures['fpContinue_n.png']

    featureButton
        .on('pointerdown', function () {
            // oAL.startGame();
            oAL.continueBtn();
            goToFullScreen();
            graphics.texture = featurePage.textures['fpContinue_p.png']
        })
        .on('pointerover', function () {
            graphics.texture = featurePage.textures['fpContinue_h.png']
        })
        .on('pointerout', function () {
            graphics.texture = featurePage.textures['fpContinue_n.png']
        });
}

async function setPreloadContainerTextures(resize = false) {

    if (preloaderImg.getChildByName('preloader-title') !== null)
        preloaderImg.getChildByName('preloader-title').visible = false;

    preloaderImg.texture = preloaderTexture;
    /*if(isMobile()){*/
    preloaderImg.width = window.innerWidth;
    preloaderImg.height = window.innerHeight;
    /*}*/

    preloaderContainer.addChild(preloaderImg);
    if (isMobile() && !isLandscape()) {
        if (preloaderImg.getChildByName('preloader-title') !== null)
            preloaderImg.getChildByName('preloader-title').visible = true;
        else {
            let spriteTitle = new PIXI.Sprite(preloaderTextureTitle);
            spriteTitle.name = 'preloader-title';
            preloaderImg.addChild(spriteTitle);
        }
    }

    /*await sleep(resize ? 100 : 800);*/

    return true;
}

function addProgressBar() {

    app.stage.addChild(oAL.loadingBarProgressText);
    app.stage.addChild(oAL.loadingBar);
    var queryParams = getUrlParameters();

    oAL.progressAnimation.x = -oAL.progressAnimation.width;
    oAL.progressAnimation.visible = true;
    oAL.progressAnimation.name = 'progress-animation';
    oAL.loadingBar.visible = false;

    if (queryParams.clientType && !isMobile()) {
        /*oAL.loadingBar.position.x = GAME_WIDTH / 2 - oAL.progressAnimation.width / 2;*/
        preloaderContainer.x = 0
    } else if (queryParams.clientType && queryParams.clientType === 'M') {

        /*oAL.loadingBar.position.x = (window.innerWidth / 2) - (oAL.loadingBar.width / 2);*/

        /*oAL.loadingBar.x = (oAL.loadingBar.parent.width / 2) - ((oAL.loadingBar.width * oAL.loadingBar.parent.scale.x) / 2);*/


        /*positionLoadingBar();*/
        if (preloaderContainer.width !== window.innerWidth)
            setPreloadContainerPosition();
    }

    return true;
}

async function addProgressBarTextures() {
    /*console.log(oAL.loadingBarSprite.texture, preloaderImages['loadingBarBg'], 'preloaderImages');
    await sleep(500);*/
    oAL.loadingBarSprite.texture = preloaderImages['loadingBarBg'].texture;
    oAL.progressAnimation.texture = preloaderImages['loadingBar'].texture;

    return true;
}

function positionLoadingBar() {
    oAL.loadingBar.visible = true;
    if (isMobile()) {
        if (!isLandscape()) {
            oAL.loadingBar.position.y = oAL.loadingBarProgressText.y = window.innerHeight / 2 + oAL.loadingBarProgressText.height;
            oAL.loadingBarProgressText.y = window.innerHeight / 2 + oAL.loadingBarProgressText.height + 20;
            reScaleContainer(oAL.loadingBar)
            oAL.loadingBar.x = window.innerWidth / 2 - (528 * oAL.loadingBar.scale.x /*- oAL.progressAnimation.width*/) / 2;
            oAL.loadingBarProgressText.x = window.innerWidth / 2 - oAL.loadingBarProgressText.width / 2
        } else {
            oAL.loadingBarProgressText.y = 265;
            oAL.loadingBar.y = 265;
            oAL.loadingBar.x = window.innerWidth / 2 - (oAL.loadingBar.width /*- 528*//*oAL.progressAnimation.width*/) / 2;
            oAL.loadingBarProgressText.x = window.innerWidth / 2 - oAL.loadingBarProgressText.width / 2
        }
    } else {
        oAL.loadingBar.position.x = GAME_WIDTH / 2 - oAL.progressAnimation.width / 2;
    }
}

function positionLoadingBarText() {
    if (isMobile()) {
        oAL.loadingBarProgressText.x = (preloaderContainer.width / preloaderContainer.scale.x) / 2 - (oAL.loadingBarProgressText.width) / 2;
    } else {
        oAL.loadingBarProgressText.x = window.innerWidth / app.stage.scale.x / 2 - (oAL.loadingBarProgressText.width) / 2;
    }
}

function setPreloadContainerPosition() {
    if (isMobile()) {
        preloaderContainer.width = window.innerWidth;
        preloaderContainer.height = window.innerHeight;
    } else {
        preloaderContainer.width = GAME_WIDTH;
        preloaderContainer.height = GAME_HEIGHT;
    }
}

function loadBrandTexture() {
    if (PIXI.loader.resources["newAssets/preload/anim/preload_ani.json?ver=" + version] === undefined)
        PIXI.Loader.shared.add("newAssets/preload/anim/preload_ani.json?ver=" + version).load(loadBrand);
    else
        loadBrand(true)
}

function loadBrand(reposition = false) {

    const queryParams = getUrlParameters();
    if (WGS === '') {
        let sheet = PIXI.Loader.shared.resources["newAssets/preload/anim/preload_ani.json?ver=" + version].spritesheet;
        if (sheet !== undefined) {
            WGS = new PIXI.AnimatedSprite(sheet.animations["preloader_anim"]);
            app.stage.addChild(WGS);
            WGS.animationSpeed = 0.350;
            WGS.name = 'WGS';
        }
    }

    if (WGS !== '' && queryParams.clientType && !isMobile()) {
        WGS.x = (GAME_WIDTH / 2) - (WGS.width / 2);

        WGS.y = 455/*app.stage.height - WGS.height * 3 */;
    } else {
        /*if (isLandscape()) {
            WGS.transform.scale.x = 0.8
            WGS.transform.scale.y = 0.8
        } else {
            WGS.transform.scale.x = 0.4
            WGS.transform.scale.y = 0.4
        }*/

        WGS.x = window.innerWidth / 2 - WGS.width / 2;
        if (!isLandscape()) {
            WGS.y = window.innerHeight / 2 + oAL.loadingBarProgressText.height + 20 + WGS.height
        } else {
            WGS.y = window.innerHeight - WGS.height //window.innerHeight - WGS.height/*app.stage.height - WGS.height * 3 */;
        }

    }
    if (WGS !== '' && reposition) {
        WGS.play();
        logoSound.play();
        WGS.loop = false;
    }
}

function hidePreloadComponents() {
    preloaderImg.visible = false;
}

function initPreloadContainer(resize = false) {

    return new Promise(async (resolve) => {
        await loadPreloadContainerTextures();
        await setPreloadContainerTextures(resize);

        console.log(preloaderContainer.width, 'settings.PRELOAD_STATUS');
        //await sleep(200);
        console.log(preloaderContainer.width, 'settings.PRELOAD_STATUS');
        if (settings.PRELOAD_STATUS || settings.PRELOAD_STATUS === 0) {
            await addProgressBarTextures();
            await addProgressBar();
            positionLoadingBarText();
            positionLoadingBar();
            //reScaleContainer(preloaderContainer);
        } else {
            hidePreloadComponents();
        }

        console.log(2, 'settings.PRELOAD_STATUS');
        return resolve(1);
    })
    /*
    //preloaderContainer.visible = true;

    return true;*/
}
