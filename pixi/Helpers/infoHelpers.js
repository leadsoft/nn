function loadInfoTextures(texturePack) {
    //infoModal.infoSprites = resources['infoPages'].spritesheet;
    infoModal.infoFullSprites['control'] = {
        'mobile': {
            'portrait': {
                'close_disabled': texturePack.textures['buttons and page indicators/close_d.png'],
                'close_hover': texturePack.textures['buttons and page indicators/close_h.png'],
                'close_normal': texturePack.textures['buttons and page indicators/close_n.png'],
                'close_p': texturePack.textures['buttons and page indicators/close_p.png'],
                'dots_disabled': texturePack.textures['mobile portrait/buttons/dots_d.png'],
                'dots_normal': texturePack.textures['mobile portrait/buttons/dots_n.png'],
                'left_arrow_disabled': texturePack.textures['mobile portrait/buttons/L_arrow_d.png'],
                'left_arrow_hover': texturePack.textures['mobile portrait/buttons/L_arrow_h.png'],
                'left_arrow_p': texturePack.textures['mobile portrait/buttons/L_arrow_p.png'],
                'left_arrow_normal': texturePack.textures['mobile portrait/buttons/L_arrow_n.png'],
                'page1_h': texturePack.textures['mobile portrait/buttons/dots_h1.png'],
                'page2_h': texturePack.textures['mobile portrait/buttons/dots_h2.png'],
                'page3_h': texturePack.textures['mobile portrait/buttons/dots_h3.png'],
                'page4_h': texturePack.textures['mobile portrait/buttons/dots_h4.png'],
                'right_arrow_disabled': texturePack.textures['mobile portrait/buttons/R_arrow_d.png'],
                'right_arrow_hover': texturePack.textures['mobile portrait/buttons/R_arrow_h.png'],
                'right_arrow_normal': texturePack.textures['mobile portrait/buttons/R_arrow_n.png'],
                'right_arrow_p': texturePack.textures['mobile portrait/buttons/R_arrow_p.png'],
            },

            'landscape': {
                'close_disabled': texturePack.textures['buttons and page indicators/close_d.png'],
                'close_hover': texturePack.textures['buttons and page indicators/close_h.png'],
                'close_normal': texturePack.textures['buttons and page indicators/close_n.png'],
                'close_p': texturePack.textures['buttons and page indicators/close_p.png'],
                'dots_disabled': texturePack.textures['buttons and page indicators/dots_d.png'],
                'dots_normal': texturePack.textures['buttons and page indicators/dots_n.png'],
                'left_arrow_disabled': texturePack.textures['buttons and page indicators/L_arrow_d.png'],
                'left_arrow_hover': texturePack.textures['buttons and page indicators/L_arrow_h.png'],
                'left_arrow_p': texturePack.textures['buttons and page indicators/L_arrow_p.png'],
                'left_arrow_normal': texturePack.textures['buttons and page indicators/L_arrow_n.png'],
                'page1_h': texturePack.textures['buttons and page indicators/page1_h.png'],
                'page2_h': texturePack.textures['buttons and page indicators/page2_h.png'],
                'page3_h': texturePack.textures['buttons and page indicators/page3_h.png'],
                'page4_h': texturePack.textures['buttons and page indicators/page4_h.png'],
                'right_arrow_disabled': texturePack.textures['buttons and page indicators/R_arrow_d.png'],
                'right_arrow_hover': texturePack.textures['buttons and page indicators/R_arrow_h.png'],
                'right_arrow_normal': texturePack.textures['buttons and page indicators/R_arrow_n.png'],
                'right_arrow_p': texturePack.textures['buttons and page indicators/R_arrow_p.png'],
            }
        },
        'desktop': {
            'close_disabled': texturePack.textures['buttons and page indicators/close_d.png'],
            'close_hover': texturePack.textures['buttons and page indicators/close_h.png'],
            'close_normal': texturePack.textures['buttons and page indicators/close_n.png'],
            'close_p': texturePack.textures['buttons and page indicators/close_p.png'],
            'dots_disabled': texturePack.textures['buttons and page indicators/dots_d.png'],
            'dots_normal': texturePack.textures['buttons and page indicators/dots_n.png'],
            'left_arrow_disabled': texturePack.textures['buttons and page indicators/L_arrow_d.png'],
            'left_arrow_hover': texturePack.textures['buttons and page indicators/L_arrow_h.png'],
            'left_arrow_p': texturePack.textures['buttons and page indicators/L_arrow_p.png'],
            'left_arrow_normal': texturePack.textures['buttons and page indicators/L_arrow_n.png'],
            'page1_h': texturePack.textures['buttons and page indicators/page1_h.png'],
            'page2_h': texturePack.textures['buttons and page indicators/page2_h.png'],
            'page3_h': texturePack.textures['buttons and page indicators/page3_h.png'],
            'page4_h': texturePack.textures['buttons and page indicators/page4_h.png'],
            'right_arrow_disabled': texturePack.textures['buttons and page indicators/R_arrow_d.png'],
            'right_arrow_hover': texturePack.textures['buttons and page indicators/R_arrow_h.png'],
            'right_arrow_normal': texturePack.textures['buttons and page indicators/R_arrow_n.png'],
            'right_arrow_p': texturePack.textures['buttons and page indicators/R_arrow_p.png'],
        }
    };

    infoModal.infoFullSprites['pages'] = {
        'mobile': {
            'portrait': {
                'page1': texturePack.textures['page1.png'],
                'page2': texturePack.textures['page2.png'],
                'page3': texturePack.textures['page3.png'],
                'page4': texturePack.textures['page4.png']

            },
            'landscape': {
                'page1': texturePack.textures['page1.png'],
                'page2': texturePack.textures['page2.png'],
                'page3': texturePack.textures['page3.png'],
                'page4': texturePack.textures['page4.png']
            },
        },
        'desktop': {
            'page1': texturePack.textures['page1.png'],
            'page2': texturePack.textures['page2.png'],
            'page3': texturePack.textures['page3.png'],
            'page4': texturePack.textures['page4.png']
        }
    };
}

function setInfoMasterTextures() {
    infoModal.infoSprites = {
        'close_disabled': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['close_disabled'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['close_disabled'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['close_disabled'];

            return texture;
        }()),
        'close_normal': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['close_normal'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['close_normal'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['close_normal'];

            return texture;
        }()),
        'close_p': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['close_p'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['close_p'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['close_p'];

            return texture;
        }()),
        'dots_disabled': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['dots_disabled'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['dots_disabled'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['dots_disabled'];

            return texture;
        }()),
        'dots_normal': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['dots_normal'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['dots_normal'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['dots_normal'];

            return texture;
        }()),
        'left_arrow_disabled': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['left_arrow_disabled'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['left_arrow_disabled'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['left_arrow_disabled'];

            return texture;
        }()),
        'left_arrow_hover': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['left_arrow_hover'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['left_arrow_hover'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['left_arrow_hover'];

            return texture;
        }()),
        'left_arrow_p': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['left_arrow_p'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['left_arrow_p'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['left_arrow_p'];

            return texture;
        }()),
        'left_arrow_normal': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['left_arrow_normal'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['left_arrow_normal'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['left_arrow_normal'];

            return texture;
        }()),
        'page1_h': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['page1_h'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['page1_h'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['page1_h'];

            return texture;
        }()),
        'page2_h': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['page2_h'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['page2_h'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['page2_h'];

            return texture;
        }()),
        'page3_h': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['page3_h'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['page3_h'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['page3_h'];

            return texture;
        }()),
        'page4_h': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['page4_h'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['page4_h'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['page4_h'];

            return texture;
        }()),
        'right_arrow_disabled': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['right_arrow_disabled'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['right_arrow_disabled'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['right_arrow_disabled'];

            return texture;
        }()),
        'right_arrow_hover': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['right_arrow_hover'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['right_arrow_hover'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['right_arrow_hover'];

            return texture;
        }()),
        'right_arrow_normal': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['right_arrow_normal'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['right_arrow_normal'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['right_arrow_normal'];

            return texture;
        }()),
        'right_arrow_p': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['control']['mobile']['landscape']['right_arrow_p'];
                else
                    texture = infoModal.infoFullSprites['control']['mobile']['portrait']['right_arrow_p'];
            else
                texture = infoModal.infoFullSprites['control']['desktop']['right_arrow_p'];

            return texture;
        }()),
        'ipPage1': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['pages']['mobile']['landscape']['page1'];
                else
                    texture = infoModal.infoFullSprites['pages']['mobile']['portrait']['page1'];
            else
                texture = infoModal.infoFullSprites['pages']['desktop']['page1'];

            return texture;
        }()),
        'ipPage2': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['pages']['mobile']['landscape']['page2'];
                else
                    texture = infoModal.infoFullSprites['pages']['mobile']['portrait']['page2'];
            else
                texture = infoModal.infoFullSprites['pages']['desktop']['page2'];

            return texture;
        }()),
        'ipPage3': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['pages']['mobile']['landscape']['page3'];
                else
                    texture = infoModal.infoFullSprites['pages']['mobile']['portrait']['page3'];
            else
                texture = infoModal.infoFullSprites['pages']['desktop']['page3'];

            return texture;
        }()),
        'ipPage4': (function () {
            let texture;
            if (isMobile())
                if (isLandscape())
                    texture = infoModal.infoFullSprites['pages']['mobile']['landscape']['page4'];
                else
                    texture = infoModal.infoFullSprites['pages']['mobile']['portrait']['page4'];
            else
                texture = infoModal.infoFullSprites['pages']['desktop']['page4'];

            return texture;
        }())
    }
}

function setElementPosition() {
    infoModal.mainContainer.y = -app.stage.height;
    infoModal.closeBtn.x = GAME_WIDTH - infoModal.closeBtn.width - 20;
    infoModal.closeBtn.y = 20;
    infoModal.panel.x = 0//(GAME_WIDTH / 2) - (infoModal.panel.width / 2);
    infoModal.panel.y = 0//(GAME_HEIGHT / 2) - (infoModal.panel.height / 2);

    /*console.log(infoModal.controlContainer.scale, infoModal.controlContainer.height, infoModal.controlContainer.width, 'infoModal.controlContainer');*/
    if (isMobile() && !isLandscape()) {
        infoModal.bulletPage.scale.set(0.75, 0.75);
    } else {
        infoModal.bulletPage.scale.set(1, 1);
    }

    infoModal.controlContainer.y = infoModal.panel.height - infoModal.controlContainer.height * 2;
    infoModal.bulletPage.x = (infoModal.controlContainer.width / 2) - (infoModal.bulletPage.width / 2);
    infoModal.bulletPage.y = infoModal.bulletPage.parent.height / 2 - infoModal.bulletPage.height / 2;
    infoModal.rightArrow.x = (infoModal.controlContainer.width / 2) + (infoModal.bulletPage.width / 2);
    infoModal.leftArrow.x = (infoModal.controlContainer.width / 2) - (infoModal.bulletPage.width / 2) - (infoModal.leftArrow.width) - 10;
    infoModal.controlContainer.x = (GAME_WIDTH / 2) - (infoModal.controlContainer.width / 2);
    infoModal.bg.width = GAME_WIDTH;
    infoModal.bg.height = app.stage.height + 100;
}

function setInfoTextures() {
    infoModal.closeBtn.texture = infoModal.infoSprites['close_normal'];
    infoModal.panel.texture = infoModal.infoSprites['ipPage1'];
    infoModal.rightArrow.texture = infoModal.infoSprites['right_arrow_normal'];
    infoModal.leftArrow.texture = infoModal.infoSprites['left_arrow_normal'];
    infoModal.bulletPage.texture = infoModal.infoSprites['page1_h'];
}

async function infoOnResize() {
    setInfoMasterTextures();
    setInfoTextures();
    setElementPosition();
}
