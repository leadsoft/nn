function disableEnableButtons(enable) {
    let isMobile = !settings.DEVICE.includes('D');
    let key = 'desktop';
    if (isMobile)
        key = 'mobile';

    if (enable) {
        footer.infoBtn.texture = oAL.panel['info'];
        /*footer.cImage.texture = oAL.panel['coins_enable_n']*/
        if (isMobile) {
            footer.coinMenuBtn.texture = oAL.panel['coins_menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
            footer.mobileMenuBtn.texture = oAL.panel['menu']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }

        if (settings.MIN_BET === 10) {
            footer.plusButton.texture = oAL.panel['disable_plus_coin'];
        } else {
            footer.plusButton.texture = oAL.panel['plus_coin'];
        }
        if (settings.MIN_BET === 0.01) {
            footer.minusButton.texture = oAL.panel['coins_minus_d'];
        } else {
            footer.minusButton.texture = oAL.panel['minus_coin'];
        }
        /*Sound*/
        if (footer.isMuted) {
            footer.soundButton.texture = oAL.panel['mute_sound'];
        } else {
            footer.soundButton.texture = oAL.panel['sound'];
        }
        /*QuickSpin*/
        if (footer.isPressedQuick) {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['quick'][key];
        } else {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['normal'][key];
        }
        /*AutoSpin*/
        if (footer.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['autospin']['cancel'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop']
        } else {
            footer.autoSpinButton.texture = oAL.panel['autospin']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }
        footer.homeBtn.texture = oAL.panel['home']['normal'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        if (!settings.DEVICE.includes('D')) {
            /*footer.mobileCoinsEnable.texture = oAL.panel['coins_enable_n'];*/
            /*footer.mobileCoinsClose.texture = oAL.panel['mobile_coins_close_n'];*/
            footer.mobileCoinMinus.texture = oAL.panel['coins_minus_n'];
            footer.mobileCoinPlus.texture = oAL.panel['coins_plus_n'];
            /*footer.mobileFastSpin.texture = oAL.panel['mobile_quickspin_n'];*/
            /*footer.mobileNormalSpin.texture = oAL.panel['mobile_normalspin_n'];*/
            footer.mobileMenu.texture = oAL.panel['menu_n'];
        }
    } else {

        footer.infoBtn.texture = oAL.panel['disable_info'];
        footer.plusButton.texture = oAL.panel['disable_plus_coin'];
        footer.minusButton.texture = oAL.panel['coins_minus_d'];
        //footer.cImage.texture = oAL.panel['coins_enable_d']
        if (isMobile) {
            footer.coinMenuBtn.texture = oAL.panel['coins_menu']['disabled'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
            footer.mobileMenuBtn.texture = oAL.panel['menu']['disabled'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }

        /*Sound*/
        if (footer.isMuted) {
            footer.soundButton.texture = oAL.panel['disable_mute_sound'];
        } else {
            footer.soundButton.texture = oAL.panel['disable_sound'];
        }
        /*QuickSpin*/
        if (footer.isPressedQuick) {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['quick_disabled'][key];
        } else {
            footer.fastSpinButton.texture = oAL.panel['fastspin']['disabled'][key];
        }
        /*AutoSpin*/
        if (footer.isPressedAutospin) {
            footer.autoSpinButton.texture = oAL.panel['autospin']['disabled_cancel'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        } else {
            footer.autoSpinButton.texture = oAL.panel['autospin']['disabled'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];
        }

        footer.homeBtn.texture = oAL.panel['home']['disabled'][!settings.DEVICE.includes('D') ? 'mobile' : 'desktop'];

        if (!settings.DEVICE.includes('D')) {
            /*footer.mobileCoinsEnable.texture = oAL.panel['coins_enable_d'];*/
            /*footer.mobileCoinsClose.texture = oAL.panel['mobile_coins_close_d'];*/

            footer.mobileCoinMinus.texture = oAL.panel['coins_minus_d'];
            footer.mobileCoinPlus.texture = oAL.panel['coins_plus_d'];
            //footer.mobileFastSpin.texture = oAL.panel['mobile_quickspin_d'];
            //footer.mobileNormalSpin.texture = oAL.panel['mobile_normalspin_d'];
            //footer.coinMenuContainer.getChildByName('coin-btn').texture = oAL.panel['menu_close_d'];
            footer.mobileMenu.texture = oAL.panel['menu_d'];
        }

    }

    footer.infoBtn.interactive = enable;
    footer.soundButton.interactive = enable;
    footer.autoSpinButton.interactive = enable;
    footer.fastSpinButton.interactive = enable;
    footer.plusButton.interactive = enable;
    footer.minusButton.interactive = enable;
    footer.homeBtn.interactive = enable;
    if (!settings.DEVICE.includes('D')) {
        footer.mobileCoinsEnable.interactive = enable;
        /*footer.mobileCoinsClose.interactive = enable;*/
        footer.mobileCoinMinus.interactive = enable;
        footer.mobileCoinPlus.interactive = enable;
        /*footer.mobileFastSpin.interactive = enable;*/
        /*footer.mobileNormalSpin.interactive = enable;*/
        footer.coinMenuContainer.getChildByName('coin-btn').interactive = enable;
        footer.mobileMenu.interactive = enable;
    }
}

function addText(isMobile) {
    let style = {
        font: /*isMobile ? "Arial" : */"PT Sans",
        fill: isMobile ? "0xffffff" : "0x000000",
        //fontWeight: "bold"
    };

    let linesBet = new PIXI.Text('LINES', style);
    footer.betLinesContainer.addChild(linesBet);
    linesBet.name = 'lines-bet-text';

    let bet = new PIXI.Text("BET", style);
    if (!isMobile)
        bet.style.fill = 0xffffff
    footer.betContainer.addChild(bet);
    /*footer.betImage.addChild(bet);*/
    bet.name = 'bet-text';

    let win = new PIXI.Text("WIN", style);
    win.name = 'win-text';
    /*footer.winImage.addChild(win);*/
    footer.winContainer.addChild(win)
    footer.winImage.zIndex = 1;

    let balance = new PIXI.Text("BALANCE ", style);
    balance.name = 'balance-text';
    /*footer.balanceImage.addChild(balance);*/
    footer.balanceContainer.addChild(balance);
    footer.balanceImage.zIndex = 1;

    let totalBalance = new PIXI.Text("TOTAL WIN", style);
    totalBalance.name = 'total-balance-text';
    /*footer.totalBalanceImage.addChild(totalBalance);*/
    footer.totalBalanceContainer.addChild(totalBalance);

    footer.totalBalanceImage.zIndex = 1;
    console.log(settings.GAME_PLAY_MODE, 'GAME_PLAY_MODE');
    let bonus = new PIXI.Text('', style);
    switch (settings.GAME_PLAY_MODE) {
        case 'demo':
            bonus.text = "DEMO MODE"
            break;
        case 'real':
            bonus.text = "BONUS: "
            break;
        case 'tournament':
            bonus.text = "TOURNAMENT"
            break;
    }

    bonus.name = 'bonus-text';
    /*footer.bonusImage.addChild(bonus);*/
    footer.bonusContainer.addChild(bonus);
    footer.bonusImage.zIndex = 1;

    let betsToQualify = new PIXI.Text("BETS TO QUALIFY", style);
    betsToQualify.style.fontSize = 20;
    betsToQualify.x = (footer.betsToQualifyImage.width / 2) - (betsToQualify.width / 2);
    betsToQualify.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.betsToQualifyImage.addChild(betsToQualify);

    let betsMade = new PIXI.Text("BETS MADE", style);
    betsMade.style.fontSize = 20;
    betsMade.x = (footer.betsMadeImage.width / 2) - (betsMade.width / 2);
    betsMade.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.betsMadeImage.addChild(betsMade);

    let timeLeft = new PIXI.Text("TIME LEFT", style);
    timeLeft.style.fontSize = 20;
    timeLeft.x = (footer.timeLeftImage.width / 2) - (timeLeft.width / 2);
    timeLeft.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.timeLeftImage.addChild(timeLeft);

    let rankBalance = new PIXI.Text("RANK BALANCE", style);
    rankBalance.style.fontSize = 20;
    rankBalance.x = (footer.rankBalanceImage.width / 2) - (rankBalance.width / 2);
    rankBalance.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.rankBalanceImage.addChild(rankBalance);

    let rank = new PIXI.Text("RANK", style);
    rank.style.fontSize = 20;
    rank.x = (footer.rankImage.width / 2) - (rank.width / 2);
    rank.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
    footer.rankImage.addChild(rank);

    /*footer.balanceImage.addChild(footer.userBalance);*/
    footer.balanceImage.zIndex = 1;

    if (isMobile) {
        /*footer.balanceImage.x = 780;*/
        /*footer.bonusImage.x = 610;*/
        /*footer.winImage.x = 210;*/
        /*footer.betImage.x = 70;*/
    }
}

function textAdjustment(isMobile) {
    if (!settings.GAME_START)
        return;
    let winText = footerContainer.getChildByName('win-container').getChildByName('win-text');
    let linesBetText = footerContainer.getChildByName('bet-lines-container').getChildByName('lines-bet-text');
    let balanceText = footerContainer.getChildByName('balance-container').getChildByName('balance-text');
    let totalBalanceText = footerContainer.getChildByName('total-balance-container').getChildByName('total-balance-text');
    let bonusText = footerContainer.getChildByName('bonus-container').getChildByName('bonus-text');
    let betText = footerContainer.getChildByName('bet-container').getChildByName('bet-text');

    if (isMobile) {
        if (isLandscape()) {
            if (settings.FS_STATUS) {
                linesBetText.text = /*settings.GAME_PLAY_MODE === 'demo' ? 'LINES' : */'FREE SPINS\n REMAINING';

                linesBetText.style.fontSize = 12;
            } else
                linesBetText.style.fontSize = 16;

            winText.style.fontSize = 17;
            balanceText.style.fontSize = 17;
            totalBalanceText.style.fontSize = 17;
            bonusText.style.fontSize = 17;
            betText.style.fontSize = 17;
            footer.amountPerLine.style.fontSize = 30;
            footer.winAmount.style.fontSize = 24
        } else {
            if (settings.FS_STATUS)
                linesBetText.text = /*settings.GAME_PLAY_MODE === 'demo' ? 'LINES' : */'FREE SPINS REMAINING';

            footer.amountPerLine.style.fontSize = 20
            winText.style.fontSize = 20;
            linesBetText.style.fontSize = 20;
            balanceText.style.fontSize = 20;
            totalBalanceText.style.fontSize = 20;
            bonusText.style.fontSize = 20;
            betText.style.fontSize = 20;
            footer.winAmount.style.fontSize = 30
        }

        function resetPosition() {
            linesBetText.x = footer.linesBetImage.width / 2 - linesBetText.width / 2;
            linesBetText.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;

            winText.x = footer.winImage.width / 2 - winText.width / 2;
            if (isLandscape())
                winText.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            else
                winText.y = -28;

            totalBalanceText.x = (footer.totalBalanceImage.width / 2) - (totalBalanceText.width / 2);
            totalBalanceText.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;

            balanceText.x = (footer.balanceImage.width / 2) - (balanceText.width / 2);
            balanceText.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;

            bonusText.x = (footer.bonusImage.width / 2) - (bonusText.width / 2);
            bonusText.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;

            betText.x = footer.betImage.width / 2 - betText.width / 2;
            betText.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;

            footer.userBalance.y = footer.bonusImage.height - footer.userBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.userBalance.x = footer.bonusImage.width / 2 - footer.userBalance.width / 2;

            footer.linesBetAmount.y = footer.linesBetImage.height - footer.linesBetAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.linesBetAmount.x = footer.linesBetImage.width / 2 - footer.linesBetAmount.width / 2;

            footer.userBonusBalance.y = footer.bonusImage.height - footer.userBonusBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.userBonusBalance.x = footer.bonusImage.width / 2 - footer.userBonusBalance.width / 2;

            footer.betAmount.y = footer.betImage.height - footer.betAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.betAmount.x = footer.betImage.width / 2 - footer.betAmount.width / 2;

            footer.winAmount.y = footer.winImage.height - footer.winAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN - 5;
            footer.winAmount.x = footer.winAmount.parent.width / 2 - footer.winAmount.width / 1.3;

            resizeActionContainerText()
            if(isMobile && isLandscape() && settings.FS_STATUS) {
            /**
             * #250(4) - Fix "Free Spins Remaining" font size
             *
             * */
                linesBetText.style.fontSize = 17;
                linesBetText.y = 2
            }
        }

        resetPosition()
        setTimeout(function () {
            resetPosition();
        }, 200)
    } else {
        linesBetText.style.fontSize = 14;
        winText.style.fontSize = 17;
        balanceText.style.fontSize = 17;
        totalBalanceText.style.fontSize = 17;
        bonusText.style.fontSize = 17;
        betText.style.fontSize = 20;

        setTimeout(function () {
            resetDesktopPosition();
        }, 400);

        function resetDesktopPosition() {
            linesBetText.text = 'LINES'
            linesBetText.x = linesBetText.parent.width / 2 - linesBetText.width / 2;
            linesBetText.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.linesBetAmount.y = footer.linesBetAmount.parent.parent.getChildByName('bg-panel').height - footer.linesBetAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.linesBetAmount.x = footer.linesBetAmount.parent.width / 2 - footer.linesBetAmount.width / 2;

            balanceText.x = 5//balanceText.parent.width / 2 - balanceText.width / 2;
            balanceText.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.userBalance.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN//footer.userBalance.parent.parent.getChildByName('bg-panel').height - footer.userBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            /*footer.userBalance.x = footer.userBalance.parent.width - footer.userBalance.width - 5/!*footer.userBalance.parent.width / 2 - footer.userBalance.width / 2*!/;*/
            footer.userBalance.x = balanceText.x + balanceText.width

            /*bonusText.x = (function () {
                let position = footerCoordinates.lines_bet_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
                switch (settings.GAME)
                return position;
            }());*/
            bonusText.x = settings.GAME_PLAY_MODE !== 'real' ? bonusText.parent.width / 2 - bonusText.width / 2 : 5//balanceText.parent.width / 2 - balanceText.width / 2;
            bonusText.y = footer.userBonusBalance.parent.parent.getChildByName('bg-panel').height - footer.userBonusBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.userBonusBalance.y = footer.userBonusBalance.parent.parent.getChildByName('bg-panel').height - footer.userBonusBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            /*footer.userBonusBalance.x = footer.userBalance.parent.width - footer.userBalance.width - 5;*/
            footer.userBonusBalance.x = bonusText.x + bonusText.width;


            betText.x = betText.parent.width / 2 - betText.width * 1.2 ;
            betText.y = 5//settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.betAmount.y = footer.betAmount.parent.parent.getChildByName('bg-panel').height - footer.betAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.betAmount.x = footer.betAmount.parent.width / 2 - footer.betAmount.width * 1.1;

            winText.x = winText.parent.width / 2 - winText.width ;
            winText.y = settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN //7;
            totalBalanceText.x = 5;
            totalBalanceText.y = footer.userBonusBalance.parent.parent.getChildByName('bg-panel').height - footer.userBonusBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            freeS.totalWinAmount.x = footer.totalBalanceImage.width - freeS.totalWinAmount.width - 5; /*freeS.totalWinAmount.parent.width*/
            freeS.totalWinAmount.y = footer.userBonusBalance.parent.parent.getChildByName('bg-panel').height - footer.userBonusBalance.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.winAmount.y = footer.winAmount.parent.parent.getChildByName('bg-panel').height - footer.winAmount.height - settings.FOOTER_CONTAINER_TEXT_TOP_AND_BOTTOM_MARGIN;
            footer.winAmount.x = footer.winAmount.parent.width / 2 - footer.winAmount.width / 1.3;
        }
    }
}

function setCurrency(){
    let currencies = [];

    for (let i = 0; i < Object.keys(WGS_GAME_CONFIG.currencies).length; i++) {
        if (WGS_GAME_CONFIG.currencies[Object.keys(WGS_GAME_CONFIG.currencies)[i]].default)
            defaultCurrency = Object.keys(WGS_GAME_CONFIG.currencies)[i]
    }

    currencies = WGS_GAME_CONFIG.currencies;

    let currencyParam = new URLSearchParams(window.location.search).get('currency');
    let curr = 'USD';
    if (currencyParam !== undefined && currencyParam !== null && currencyParam.trim() !== '') {
        curr = currencyParam;
    }

    if(Object.keys(currencies).includes(curr)) {
        user.currencySymbol = currencies[curr].symbol
    } else {
        user.currencySymbol = currencies[defaultCurrency].symbol
    }

    footer.betAmount.text = currencies[curr].symbol + footer.betAmount.text.substring(1);
    footer.amountPerLine.text = currencies[curr].symbol + footer.amountPerLine.text.substring(1);
    footer.userBalance.text = currencies[curr].symbol + footer.userBalance.text.substring(1);
    if(settings.GAME_PLAY_MODE === 'real')
        footer.userBonusBalance.text = currencies[curr].symbol + footer.userBonusBalance.text.substring(1);
}

function setAndCenterPaylineText(paylineText){
    footer.paylineWin.text = paylineText;
    footer.paylineWin.visible = true;

    if(isMobile()){
        footer.paylineWin.y = footer.winImage.height - footer.paylineWin.height - 2;
    } else {
        footer.paylineWin.y = footerContainer.height - footer.paylineWin.height - 5;
        // footer.paylineWin.x = footer.winAmount.parent.width / 2 - footer.winAmount.width;
    }
    if ((typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1)) {
        footer.paylineWin.x = footer.paylineWin.parent.width / 2 - footer.paylineWin.width / 1.7;
        /*if (settings.SWITCH_LINES_BET && settings.DEVICE.includes('D')) {
        footer.paylineWin.x = footer.paylineWin.parent.width / 2 - footer.paylineWin.width / 2;
        } else if (settings.SWITCH_LINES_BET && !settings.DEVICE.includes('D')) {
            footer.paylineWin.x = footer.paylineWin.parent.width / 2 - footer.paylineWin.width / 2;
        } else {
            footer.paylineWin.x = footer.paylineWin.parent.width / 2 - footer.paylineWin.width / 2;
        }*/
    } else {
        if (settings.SWITCH_LINES_BET && settings.DEVICE.includes('D')) {
            footer.paylineWin.y = footer.paylineWin.parent.parent.getChildByName('bg-panel').height / footer.paylineWin.parent.scale.y - footer.paylineWin.height - 5;
        } else {
            footer.paylineWin.x = footer.paylineWin.parent.width / 2 - footer.paylineWin.width / 2
        }
    }
}


