class spinHelper {

    constructor() {
        this.fastSpin = false;
        this.autospin = false;
        this.reel1ScatterActive = false;
        this.reel2ScatterActive = false;
        this.reel3ScatterActive = false;
        this.gameType = game;
        this.paylines = [];
        this.slotPosition = [];
        this.freeSlotPosition = [];
        this.firstReel = [];
        this.bigSymbol = [];
    }

    async activeSpinButton() {
        if(!game.startSpin && game.respin) {
            return;
        }

            if (settings.GAME_RUNNING) return;
            if (!settings.CAN_BET) return;
            let self = this;
            setSettings.GAME_RUNNING = true;
            setSettings.RESET_SPIN_VALUE = false;
            setSettings.CURRENT_SPIN_INDEX++;
            if (footer.mobileMenu !== undefined && footer.mobileMenu.status) {
                footer.mobileMenu.status = false;
                menuActions.openCloseInfoBar();
            }

            if (footer.coinMenuContainer !== undefined && footer.coinMenuContainer.status) {
                footer.coinMenuContainer.status = false;
                coinMenuBtnActions.openCloseSliderBar();
            }
            if (parseFloat(settings.LAST_WIN_AMOUNT).toFixed(2) > 0) {

                if (!settings.SWITCH_LINES_BET && settings.DEVICE.includes('D') && !settings.FS_STATUS) {
                    footer.winAmount.style.fill = 0x000000;
                } else if (!settings.SWITCH_LINES_BET && !settings.DEVICE.includes('D')) {
                    footer.winAmount.style.fill = 0xffffff;
                }

                footer.winAmount.text = user.currencySymbol + formatMoney(parseFloat(settings.LAST_WIN_AMOUNT).toFixed(2));
                //gameCompleted.winText.text = '';
                gameCompleted.winText.children = [];
                oAL.winLine.visible = false;
                footer.winAmount.x = footer.winAmount.parent.width / 2 - footer.winAmount.width / 1.3;
                // footer.winAmount.y = (footer.winAmount.parent.height / 2) - (footer.winAmount.height / 2);
            }

            if (!settings.FS_STATUS) {
                let userBalance = await user.debitUserBalance();
                if (!userBalance.status) {
                    if (typeof userBalance.type !== "undefined" && userBalance.type === 'tournament_rebuy') {
                        modalDialog.show(messages.rebuy_insufficient_funds, 'tournament')
                    } else {
                        if (typeof userBalance.type !== "undefined")
                            modalDialog.show(userBalance.msg, userBalance.type)
                        else
                            modalDialog.show(userBalance.msg)
                    }
                    return;
                }
            }

            setSettings.SPIN_STATUS = true;

            disableEnableButtons(false);
            footer.showHideDarkSpinButton(true, 'activeSpinButton');

            if (!settings.TICK_STATUS)
                footer.winAmount.text = '';
            if (isMobile() && settings.FS_ENABLE_STATUS) {
                footer.betAmount.text = '';
            }
            setSettings.TICK_STATUS = false;
            game.randomElement = {0: true, 1: true, 2: true, 3: true, 4: true};
            freeS.randomElement = {0: true, 1: true, 2: true};

            self.startPlay();
            setSettings.LAST_WIN_AMOUNT = 0;

    }
    async startPlay() {
        setSettings.INITIAL_WIN = false;
        setSettings.FS_START = false;
        setSettings.FS_END = false;
        setSettings.STOP_PAYLINE_ANIMATION = false;
        gameCompleted.firstLoop = true;
        setSettings.WINNING_ROTATION_STATUS = false;

        gameCompleted.hideAllLinesIndicators();
        // game.fakeReelContainer.removeChildren();
        console.log("oAL.respin " + game.respin)
        footer.paylineWin.text = '';
        gameCompleted.hideActivePayline();
        /*gameCompleted.winText.text = '';*/
        gameCompleted.winText.children = [];
        oAL.winLine.visible = false;
        /*scatter.stop();*/
        tickerStart.stop();
        tickerMid.stop();
        reelStop.stop();
        startSpin.stop();
        startSpin.play();


        if(game.nrFreeSpin === 2) {
            game.nrFreeSpin = 0;
        }

        if(game.respin) {
            let val = game.fakeReelContainer.children.length;
            game.nrFreeSpin += 1;
            game.respin = false;
            for (let i = 0; i < val; i++) {
                if (game.fakeReelContainer.children[i].name !== "symbol-h") {
                    game.fakeReelContainer.getChildAt(i).removeChildren();
                }
            }
        } else if (!game.respin)  {
            setTimeout(() => {
                for(let i=1; i<=3; i++) {
                    game.reelContainer.getChildAt(2).getChildAt(i).visible = true;
                }
                // game.reelContainer.getChildAt(2).getChildAt(1).visible = true;
                // game.reelContainer.getChildAt(2).getChildAt(2).visible = true;
                // game.reelContainer.getChildAt(2).getChildAt(3).visible = true;
                game.fakeReelContainer.removeChildren();
            },200)

            if(oAL.respinTextTitle.visible) {
                oAL.respinTextTitle.animationSpeed = -0.500;
                oAL.respinTextTitle.play();
                setTimeout(() => {
                    oAL.respinTextTitle.gotoAndStop(0);
                    oAL.respinTextTitle.visible = false;
                }, 700)


            }
        }

        if(gameCompleted.winText.visible === false) {
            gameCompleted.winText.visible = true;
        }

        this.removeAllAnimatedSprite();
        gameCompleted.addRemoveBlurAllSymbols(false);
        gameCompleted.hideAllLines();
        setSettings.START_GAME = true;
        /*let fsStatus = settings.FS_STATUS;*/
        this.gameType = /*settings.FS_STATUS ? freeS :*/ game;
        if(settings.FS_STATUS){
            footer.linesBetAmount.text = parseInt(settings.FS_COUNT) - 1;
        }
        if(settings.FSV_STATUS){
            setSettings.FSV_REMAINING = parseInt(settings.FSV_REMAINING) - 1;
        }


        await slotOperation();

        setTimeout(function () {
            footer.showHideStopSpinButton(true);
        }, 500)

        if (responseVariable == null) {
            setSettings.GAME_RUNNING = false;
        } else {
            setSettings.GAME_RUNNING = true;

            for (let i = 0; i < this.gameType.reels.length; i++) {
                const r = this.gameType.reels[i];
                const extra = Math.floor(Math.random() * 3);
                const target = r.position + 50 + i * 5 + extra;
                let time = (i + 1) * 400 + 500/* * 10000*///(i + 1) * 1000 + extra * 600;
                let backout = 0;

                if (game.nrFreeSpin === 2) {
                    time = (i + 1) * 700 + 500;
                }
                //     if (this.fastSpin) {
                //     time = 100 + i * 200 + extra;
                //     backout = 0;
                // }

                this.tweenTo(r, 'position', target, time, this.backout(backout), this.update.bind(this), i === this.gameType.reels.length - 1 ? this.reelsComplete.bind(this) : null, i);
            }
        }
    }

    update() {
    }

    async reelsComplete() {
        let self = this;
        if (sound)
            startSpin.stop();
        game.restScatterCounter()
        setSettings.ANTICIPATION = false;
        game.scatterCount = 0;
        game.reel1ScatterActive = false;
        game.reel2ScatterActive = false;
        game.reel3ScatterActive = false;

        setSettings.GAME_RUNNING = false;
        setSettings.START_GAME = false;
        setSettings.STOP_REEL = false;
        setSettings.SPIN_STATUS = false;
        let win = parseFloat($(responseVariable).find('slotstatus').attr('win'))
        setSettings.LAST_WIN_AMOUNT = win;
        setSettings.TICK_STATUS = win > 0;
        setSettings.USER_BALANCE = parseFloat($(responseVariable).find('slotstatus').attr('chips'));
        setSettings.PROMO_CHIPS = $(responseVariable).find('slotstatus').attr('promochips');
        setSettings.RANK_USER_BALANCE = parseFloat($(responseVariable).find('slotstatus').attr('rankbalance') || 0);
        if (!settings.FS_STATUS)
            disableEnableButtons(true);

        if (this.paylines.length > 0) {
            this.winningRound = true;
            await this.winPaylines();
        } else {
            if (!settings.FS_STATUS && settings.FS_COUNT === 0)
                setSettings.FS_STOP = true;
            else if (settings.FS_STATUS && !settings.FS_START)
                setSettings.FS_START = true;

            this.winningRound = false;
            if (!settings.FS_ENABLE_STATUS)
                footer.showHideSpinButton(true);
        }

        if(!this.winningRound && settings.FS_STATUS && parseInt(settings.FS_COUNT || 0) > 0 && settings.FS_RUNNING_STATUS){
            await sleep(1000)
            console.log(1);
            self.activeSpinButton()
        } else if(!this.winningRound && (settings.FSV_STATUS && (settings.FSV_REMAINING || 0) > 0 && !settings.FS_RUNNING_STATUS)){
            console.log(2);
            await sleep(1000)
            self.activeSpinButton()
        } else if (this.autospin && !this.winningRound && !settings.FS_RUNNING_STATUS) {
            await sleep(1000)
            self.activeSpinButton()
        }

        /*if (this.autospin || settings.FS_STATUS || (settings.FSV_STATUS && (settings.FSV_REMAINING || 0) > 0)) {
            if (!this.winningRound || (!settings.FS_STATUS && parseInt(settings.FS_COUNT || 0) > 0))
                setTimeout(function () {
                    if(parseInt(settings.FS_COUNT || 0) > 0)
                        self.activeSpinButton()
                    else {
                        console.log('mi-l sugi')
                    }
                }, 1000)
        }*/
    }

    async winPaylines() {
        return new Promise(async (resolve, reject) => {
            gameCompleted.winnLineIndex = 0;
            await gameCompleted.index();
            resolve(1);
        })
    }

    tweenTo(object, property, target, time, easing, onchange, oncomplete, reelPosition) {
        const tween = {
            object,
            property,
            propertyBeginValue: object[property],
            target,
            easing,
            time,
            change: onchange,
            complete: oncomplete,
            scatterCount: 0,
            start: Date.now(),
        };

       /* if (this.gameType.name === 'free-spin' && reelPosition === 0)
            tween.time += 1000;
        else if (this.gameType.name === 'free-spin' && reelPosition === 2)
            tween.time += 1150;*/

        this.gameType.tweening.push(tween);
        /*game.tweening.push(tween);*/

        return tween;
    }

    lerp(a1, a2, t) {

        return a1 * (1 - t) + a2 * t;
    }

    backout(amount) {
        return (t) => (--t * t * ((amount + 1) * t + amount) + 1);
    }

    removeAllAnimatedSprite() {
        for (let i = 0; i < spinH.gameType.reels.length; i++) {
            for (let j = 0; j < 4; j++) {
                spinH.gameType.reels[i].symbols[j].children = []
            }
        }
    }
}
