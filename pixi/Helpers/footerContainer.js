function loadFooterContainerTextures(texturePack) {
    oAL.panel['balance_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/realDemo/balanceMask.png'],
                'freeGame': texturePack.textures['mobile/portrait/realDemo/balanceMask.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/realDemo/balanceMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/freeSpinVoucher/balanceMask.png']
            }
        },
        'desktop': ''/*(function () {
                    let key = mSource.toLowerCase() + (isMobile ? (window.innerHeight > window.innerWidth ? '/portrait' : '/landscape') : '') + '/baseGame/balanceMask.png';
                    return panelsElements.textures[key];
            }())*/
    };
    oAL.panel['bet_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/realDemo/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/portrait/realDemo/bonusMask.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/freeSpinVoucher/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/freeSpinVoucher/bonusMask.png']
            },
        },
        'desktop': ''
    };
    oAL.panel['bonus_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/realDemo/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/portrait/realDemo/bonusMask.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/freeSpinVoucher/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/freeSpinVoucher/bonusMask.png']
            },
        },
        'desktop': ''
    };

    oAL.panel['lines_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/realDemo/bonusMask.png'],
                'freeGame': texturePack.textures['mobile/portrait/realDemo/middleMask.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/realDemo/linesMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/realDemo/linesMask.png']
            }
        },
        'desktop': ''
    };
    oAL.panel['win_mask'] = {
        'mobile': {
            'portrait': {
                'baseGame': texturePack.textures['mobile/portrait/realDemo/winMask_long.png'],
                'freeGame': texturePack.textures['mobile/portrait/freeSpinVoucher/winMask_long.png']
            },
            'landscape': {
                'baseGame': texturePack.textures['mobile/landscape/realDemo/winMask.png'],
                'freeGame': texturePack.textures['mobile/landscape/freeSpinVoucher/winMask.png']
            }
        },
        'desktop': ''
    };
}

function setFooterContainerTextures() {

    let isMobile = !settings.DEVICE.includes('D');
    let device = isMobile ? 'mobile' : 'desktop';
    let key = 'desktop';
    let gameType =/* settings.FS_STATUS ? 'freeGame' : */'baseGame';
    if (isMobile) {
        if (window.innerWidth > window.innerHeight) {
            key = 'landscape';
        } else {
            key = 'portrait';
        }
    }
    if (isMobile) {
        footer.balanceImage.texture = oAL.panel['balance_mask'][device][key][gameType]; //TODO: need version for desktop
        footer.totalBalanceImage.texture = oAL.panel['balance_mask'][device][key][gameType];
        footer.betImage.texture = oAL.panel['bet_mask'][device][key][gameType];
        footer.winImage.texture = oAL.panel['win_mask'][device][key][gameType];
        footer.bonusImage.texture = oAL.panel['bonus_mask'][device][key][gameType];
        footer.linesBetImage.texture = oAL.panel['lines_mask'][device][key][gameType];
    } else {
        footer.balanceImage.width = footer.bonusImage.width = 220;
        /*footer.bonusImage.width = 220;*/
        footer.betImage.width = 135;
        footer.totalBalanceImage.width = footer.winImage.width = 170;
        footer.linesBetImage.width = 80;
    }
}

function setFooterContainerPosition() {
    let isMobile = !settings.DEVICE.includes('D');
    footer.betLinesContainer.x = (function () {
        let position = footerCoordinates.lines_bet_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.lines_bet_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop']['landscape'].x;
            } else {
                footerContainer.x = 0;
                position = settings.FOOTER_CONTAINER_MARGIN;
            }
        return position;
    }());

    footer.betLinesContainer.y = (function () {
        let position = 0//footerCoordinates.lines_bet_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            position = footerCoordinates.lines_bet_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());

    footer.balanceContainer.x = (function () {
        let position = footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            if (isLandscape()) {
                position = footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['portrait'].x;
            }
        return position;
    }());
    footer.balanceContainer.y = (function () {
        let position = 0//footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            if (isLandscape()) {
                position = footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['landscape'].y;
            } else {
                position = footer.winContainer.height + footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['portrait'].y + settings.FOOTER_CONTAINER_MARGIN//footerCoordinates.balance_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['portrait'].y;
            }

        return position;
    }());

    footer.bonusContainer.x = (function () {
        let position = footerCoordinates.bonus_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        if (isMobile)
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.bonus_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = footer.balanceContainer.x + footer.bonusImage.width + settings.FOOTER_CONTAINER_MARGIN;
            }

        return position;
    }());
    footer.bonusContainer.y = (function () {
        let position = 0//footerCoordinates.bonus_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            if (isLandscape()) {
                position = footerCoordinates.bonus_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['landscape'].y;
            } else {
                position = footer.winContainer.height + footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['portrait'].y + settings.FOOTER_CONTAINER_MARGIN;
            }


        return position;
    }());

    footer.betContainer.x = (function () {
        let position = footerCoordinates.bet_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;
        if (isMobile)
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.bet_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                position = GAME_WIDTH - footer.betContainer.width - settings.FOOTER_CONTAINER_MARGIN;
            }

        return position;
    }());
    footer.betContainer.y = (function () {
        let position = footerCoordinates.bet_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].y;
        if (isMobile)
            if (isLandscape()) {
                position = footerCoordinates.bet_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['landscape'].y;
            } else {
                position = footer.winContainer.height + footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['portrait'].y + settings.FOOTER_CONTAINER_MARGIN;
            }
        return position;
    }());

    footer.winContainer.x = (function () {
        let position = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode']['desktop'].x;

        if (isMobile) {
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].x;
            } else {
                footerContainer.x = 0;
                position = GAME_WIDTH / 2 - footer.winContainer.width / 2;
            }
        }

        return position;
    }());

    /*var bg = new PIXI.Sprite(PIXI.Texture.WHITE);
    bg.tint = 0xffaa00;
    bg.width = footer.winContainer.width;
    bg.height = footer.winContainer.height;
    bg.alpha = 0.4
    bg.name = 'bg'
    footer.winContainer.addChild(bg);*/

    footer.winContainer.y = (function () {
        //let position = -5//footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        let position = 0
        if (isMobile) {
            if (isLandscape()) {
                position = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['landscape'].y;
            } else {
                position = footerCoordinates.win_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['portrait'].y;
            }
        } else {
            position -= 7;
        }

        return position;
    }());

    footer.totalBalanceContainer.x = (function () {
        let position = footerCoordinates.total_balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].x;
        if (isMobile)
            if (window.innerWidth > window.innerHeight) {
                position = footerCoordinates.total_balance_container[settings.GAME_PLAY_MODE + '_mode']['mobile']['landscape'].x;
            } else {
                position = footer.balanceContainer.x + footer.bonusImage.width + settings.FOOTER_CONTAINER_MARGIN;
            }

        return position;
    }());
    footer.totalBalanceContainer.y = (function () {
        let position = 0//footerCoordinates.total_balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'].y;
        if (isMobile)
            position = footerCoordinates.total_balance_container[settings.GAME_PLAY_MODE + '_mode'][isMobile ? 'mobile' : 'desktop'][window.innerWidth > window.innerHeight ? 'landscape' : 'portrait'].y;

        return position;
    }());
}

function repositionFooterContainer() {
    if (isMobile()) {
        if (!isLandscape()){
            console.log(gameContainer.y,  gameContainer.height, 'repo')

            setTimeout( () => {
                // footerContainer.y = (SYMBOL_SIZE * 3) / gameContainer.scale.y;
                footerContainer.y = gameContainer.y + gameContainer.height + 50
            }, 200)
        }
    }

    return true;
}

function adjustFooterStyle() {
    footerContainer.getChildByName('win-image').getChildByName('win-text').style.fontSize = 30
    footerContainer.getChildByName('lines-bet-image').getChildByName('lines-bet-text').style.fontSize = 30
    footerContainer.getChildByName('balance-image').getChildByName('balance-text').style.fontSize = 30
    footerContainer.getChildByName('bonus-image').getChildByName('bonus-text').style.fontSize = 30
    footerContainer.getChildByName('bet-image').getChildByName('bet-text').style.fontSize = 30
}

