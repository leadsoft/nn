class freeSpinVoucher {
    constructor() {

    }

    init() {
        this.freeCouponContainer = new PIXI.Container();
        var graphics = new PIXI.Graphics();
        graphics.width = 300;
        graphics.name = 'graphic';
        graphics.height = 75;

        graphics.beginFill(0x000000);

        graphics.lineStyle(1, 0xFFFFFF);

        if (isMobile()) {
            graphics.drawRoundedRect(0, 0, 230, 80, 5);
        } else {
            graphics.drawRect(0, 0, 300, 95);
        }
        graphics.name = 'graphic';
        graphics.alpha = 0.6;
        /*this.awardedText = new PIXI.Text("FREE SPIN AWARDED \nStarting " + settings.FSV_REMAINING + " of " + settings.FSV_AWARDED + " Plays\n\nGood Luck", {*/
        /**
         * Done #956 - Changed awarded text wording
         *
         * */
        this.awardedText = new PIXI.Text("FREE SPINS VOUCHER REDEEMED \nStarting " + settings.FSV_REMAINING + " Complimentary Spins\n\nGood Luck", {
        /*this.awardedText = new PIXI.Text("FREE SPINS VOUCHER REDEEMED \nContinuing Your Free Spins\n\nGood Luck", {*/
            font: "Arial",
            fill: isMobile() ? 0xd1d119 : "0xffffff",
            fontSize: isMobile() ? 11 : 15,
            fontWeight: isMobile() ? 900 : 'normal',
            align: "center"
        });
        console.log('fsv remaining', settings.FSV_REMAINING, this.awardedText)

        this.freeCouponContainer.addChild(graphics);
        this.freeCouponContainer.addChild(this.awardedText);
        this.awardedText.x = (this.freeCouponContainer.width / 2) - this.awardedText.width / 2;
        this.awardedText.y = 5;

        this.freeCouponContainer.zIndex = 10;

        app.stage.addChild(this.freeCouponContainer);
        this.freeCouponContainer.x = 20//GAME_WIDTH / 2 - graphics.width / 2;
        this.freeCouponContainer.y = -this.freeCouponContainer.height
    }

    repositionFreeCouponContainer(){
        let self = this;

        if (isMobile()) {
            if (!isLandscape()) {
                self.freeCouponContainer.getChildByName('graphic').width = 250
                self.freeCouponContainer.getChildByName('graphic').height = 50
                self.freeCouponContainer.x = window.innerWidth / 2 - self.freeCouponContainer.width / 2;
                self.freeCouponContainer.y = footerContainer.y + footerContainer.height//window.innerHeight - self.freeCouponContainer.height
                if(!settings.FS_STATUS)
                    self.awardedText.text = "FREE SPIN VOUCHER \n\nTOTAL WIN: " + user.currencySymbol + parseFloat(settings.FSV_TOTAL_WIN).toFixed(2) + "   " + settings.FSV_REMAINING + " PLAYS REMAINING";
            } else {
                if(!settings.FS_STATUS)
                    self.awardedText.text = "FREE SPIN VOUCHER \n\nTOTAL WIN: " + user.currencySymbol + parseFloat(settings.FSV_TOTAL_WIN).toFixed(2) + "\n\n" + settings.FSV_REMAINING + " PLAYS REMAINING";
                self.freeCouponContainer.getChildByName('graphic').height = 77
                self.freeCouponContainer.getChildByName('graphic').width = 140
                self.freeCouponContainer.y = 10;
                self.freeCouponContainer.x = 20;
            }
            self.freeCouponContainer.scale.set(1, 1);
        } else {
            if(!settings.FS_STATUS)
                self.awardedText.text = "FREE SPIN VOUCHER \n\nTOTAL WIN: " + user.currencySymbol + parseFloat(settings.FSV_TOTAL_WIN).toFixed(2) + "\n\n" + settings.FSV_REMAINING + " PLAYS REMAINING";
        }
    }

    async showHideAwardedModal() {
        await sleep(1000);
        let self = this;
        this.freeCouponContainer.scale.set(1, 1);
        if (isMobile()) {
            this.freeCouponContainer.x = window.innerWidth / 2 - this.freeCouponContainer.width / 2;
        } else {
            this.freeCouponContainer.x = 20//GAME_WIDTH / 2 - this.freeCouponContainer.width / 2;
        }

//        this.freeCouponContainer.width = 300;
        TweenMax.to(this.freeCouponContainer, 0.6, {
            y: 0,
            ease: Bounce.easeOut,
        });
        await sleep(2000);

        TweenMax.to(this.freeCouponContainer, 0.6, {
            y: isMobile() && !isLandscape() ? window.innerHeight : -this.freeCouponContainer.height,
            ease: Power2.easeIn,
            onComplete: function (e) {
                if (isMobile()) {
                    if (!isLandscape()) {
                        self.freeCouponContainer.getChildByName('graphic').width = 250
                        self.freeCouponContainer.getChildByName('graphic').height = 50
                        self.freeCouponContainer.x = window.innerWidth / 2 - self.freeCouponContainer.width / 2;
                    } else {
                        self.freeCouponContainer.getChildByName('graphic').height = 77
                        self.freeCouponContainer.getChildByName('graphic').width = 140
                    }
                    self.freeCouponContainer.scale.set(1, 1);
                }
            },
        });

        await sleep(1000);

        if (isMobile() && !isLandscape()) {
            this.awardedText.text = "FREE SPIN VOUCHER \n\nTOTAL WIN: " + user.currencySymbol + parseFloat(settings.FSV_TOTAL_WIN).toFixed(2) + "   " + settings.FSV_REMAINING + " PLAYS REMAINING";
        } else {
            this.freeCouponContainer.x = 20;
            this.awardedText.text = "FREE SPIN VOUCHER \n\nTOTAL WIN: " + user.currencySymbol + parseFloat(settings.FSV_TOTAL_WIN).toFixed(2) + "\n\n" + settings.FSV_REMAINING + " PLAYS REMAINING";
        }
        this.awardedText.x = (this.freeCouponContainer.width / 2) - this.awardedText.width / 2;
        TweenMax.to(this.freeCouponContainer, 0.6, {
            y: isMobile() && !isLandscape() ? footerContainer.y + footerContainer.height/*window.innerHeight - this.freeCouponContainer.height*/ : 10,
            ease: Bounce.easeOut
        });
        if (!settings.FS_STATUS) {
            console.log('active spin 4')
            spinH.activeSpinButton()
        }
    }

    async closeFreeCouponSession() {
        setSettings.REQUEST_END = false;
        disableEnableButtons(false);
        footer.showHideDarkSpinButton(true);
        await sleep(2000);
        startGame();
        await waitUntilIsDone('REQUEST_END');
        await sleep(1000);
        TweenMax.to(this.freeCouponContainer, 0.6, {
            y: -this.freeCouponContainer.height,
            ease: Power2.easeIn,
            onComplete: function (e) {
                modalDialog.show('We hope that you have enjoyed your complementary game play!<br>You have won <span style="color: #FFFF00">' + user.currencySymbol + parseFloat(settings.FSV_TOTAL_WIN).toFixed(2) + '</span> which has been added to your bonus balance.', 'reward')
                disableEnableButtons(true);
                /*setSettings.PROMO_CHIPS += parseFloat(settings.FSV_TOTAL_WIN).toFixed(2)*/
                setSettings.CAN_BET = true;
                footer.showHideDarkSpinButton(false);
                footer.spinButtonSprite.visible = true;
                startGame();
            },
        });

        await sleep(1000);
        setSettings.FSV_TOTAL_WIN = 0;
        setSettings.FSV_COUPON_ID = "";
    }

    start() {
        disableEnableButtons(false);
    }

    end() {
        disableEnableButtons(true);
    }

    awardedTextControl(startGame = false) {
        if (settings.FSV_STATUS)
            this.repositionFreeCouponContainer()
            //this.awardedText.text = "FREE SPIN VOUCHER \nTOTAL WIN: " + user.currencySymbol + (parseFloat(settings.FSV_TOTAL_WIN) + parseFloat(settings.TOTAL_BONUS_WIN || 0)).toFixed(2) + "\n\n" + (settings.FSV_REMAINING - startGame) + " PLAYS REMAINING";

    }
}

