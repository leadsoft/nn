class ModalDialog {
    constructor(gameContainer) {

        this.gameContainer = gameContainer;
        this.modalContainer = new PIXI.Container();
        this.modalContainer.width = 483;
        this.modalContainer.height = 218;
        this.modalContainer.x = (GAME_WIDTH / 2) - (this.modalContainer.width / 2);
        this.modalContainer.pivot.x = 500 / 2; //this.modalContainer.width
        this.modalContainer.y = 60;
        this.modalContainer.zIndex = 20;
        this.defaultRebuyModal;

        var bg = new PIXI.Sprite(PIXI.Texture.WHITE);
        bg.width = 483;
        bg.height = 218;
        bg.tint = '#07000e';
        this.modalContainer.addChild(bg);

        this.modalText = new PIXI.Text("TEST123", {
            fill: "#f2f2f2",
            align: 'center',
            font: 'pt_sans',
            fontSize: 18,
            color: "#f2f2f2",
            multiline: true,
            leading: 5,
            textWidth: 400
        });

        this.modalText.addChild(this.modalText);
        this.modalContainer.addChild(this.modalText);
        /*this.modalContainer.addChild(cancel);*/

        this.modalContainer.visible = false;
    }

    show(txt, type = 'normal') {
        setSettings.MODAL_DIALOG_STATUS = true;
        let modal = $(".modal");
        modal.find('.main-container').empty();
        modal.find('.main-container').html('<p>' + txt + '</p>');
        if (type === 'normal') {
            modal.find('.lobby-button').addClass('hidden');
            modal.find('.rebuy-button').addClass('hidden');
            modal.find('.close-button').addClass('hidden');
            modal.find('.refresh-button').removeClass('hidden');
        } else if (type === 'refresh') {
            modal.find('.rebuy-button').addClass('hidden');
            modal.find('.close-button').addClass('hidden');
            modal.find('.cancel-button').addClass('hidden');
            modal.find('.refresh-button').removeClass('hidden');
            /*modal.find('.refresh-button').addClass('center').css({display: 'block', margin: '0 auto'});*/
            modal.find('.lobby-button').removeClass('hidden');
        } else if (type === 'insufficient_funds') {
            modal.find('.rebuy-button').addClass('hidden');
            modal.find('.close-button').removeClass('hidden');
            modal.find('.cancel-button').addClass('hidden');
            modal.find('.refresh-button').addClass('hidden');
            modal.find('.lobby-button').addClass('hidden');
        } else if (type === 'real_funds') {
            modal.find('.lobby-button').addClass('hidden');
            modal.find('.rebuy-button').addClass('hidden');
            modal.find('.cancel-button').addClass('hidden');
            modal.find('.close-button').removeClass('hidden');
            modal.find('.refresh-button').addClass('hidden');
        } else if (type === 'reward') {
            setSettings.MODAL_DIALOG_STATUS = true;
            modal.find('.lobby-button').addClass('hidden');
            modal.find('.rebuy-button').addClass('hidden');
            modal.find('.cancel-button').addClass('hidden');
            modal.find('.close-button').removeClass('hidden');
            modal.find('.refresh-button').addClass('hidden');
        } else if (type === 'tournament') {
            modal.find('.lobby-button').addClass('hidden');
            modal.find('.rebuy-button')
                .attr('show-rebuy-default-modal', true)
                .removeClass('hidden');
            modal.find('.refresh-button').addClass('hidden');
            modal.find('.close-button').addClass('hidden');
        }

        modal.modal({
            fadeDuration: 100,
            escapeClose: false,
            clickClose: false,
            showClose: false
        });
    }

    hide() {
        this.modalText.text = '';
        this.modalContainer.visible = false;
        this.gameContainer.alpha = 1;
        gameCompleted.containerLineIndicators.visible = true;
        footer.disableFooter = false;
    }

    showRebuyDefaultModal(txt, rebuy = false) {
        let modal = $(".modal");
        modal.find('.main-container').empty();
        modal.find('.main-container').html('<p>' + txt + '</p>');
        /*if (rebuy)
            modal.find('.main-container').append('<p class="center">Good Luck</p>');*/
        modal.find('.refresh-button').addClass('hidden');
        modal.find('.close-button').addClass('hidden');
        modal.find('.lobby-button').addClass('hidden');
        modal.find('.rebuy-button').removeClass('hidden');
        modal.find('.cancel-button').removeClass('hidden');
        modal.find('.rebuy-button').attr('show-rebuy-default-modal', false);
        modal.modal({
            fadeDuration: 100,
            escapeClose: false,
            clickClose: false,
            showClose: false
        });
    }

    showAwardedModal(txt) {
        let modal = $(".modal-coupon");
        modal.find('.main-container').empty();
        modal.find('.main-container').html('<p>' + txt + '</p>');

        modal.find('.main-container').append('<p class="center">Good Luck</p>');

        modal.modal({
            fadeDuration: 100,
            escapeClose: false,
            clickClose: false,
            showClose: false
        });
    }
}

$(document).ready(function () {
    $('.close-button, .cancel-button').on('click', function () {
        setSettings.MODAL_DIALOG_STATUS = false;
        $.modal.close();
        setSettings.GAME_RUNNING = false;
        setSettings.SWITCH_LINES_BET = false;
    });

    $('.lobby-button').on('click', function () {
        window.parent.location;
        if (window.self!==window.top)
            window.parent.location.replace(settings.HOME_LINK)
        else
            window.location.href = settings.HOME_LINK;
    });

    $('.refresh-button').on('click', function () {
        window.location.reload()
    });

    $('.rebuy-button').on('click', function () {
        if ($(this).attr('show-rebuy-default-modal') === 'true') {

            $.modal.close();
            setTimeout(function () {
                modalDialog.showRebuyDefaultModal(messages.rebuy_default, 'rebuy');
            }, 300)

        } else {
            requestForRebuy(function (data, err) {
                if (err)
                    console.log('show error');


                if($(responseVariable).find('tournamenterror').length > 0){
                    $.modal.close()
                    setTimeout(function(){
                        modalDialog.show($(responseVariable).find('tournamenterror').attr('debug'), 'insufficient_funds');
                    }, 300)

                } else {

                    requestStartGameAfterRebuy(function (data, err) {
                        rebuy.hideRebuyPopup();
                        $.modal.close()
                    });
                }
            })
        }
    })
});


